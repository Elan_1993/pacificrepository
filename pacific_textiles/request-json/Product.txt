[
     {
        "id": {
            "prodCode": "1111",
            "prodCmpCode": "2"
        },
        "prodName": "xxxx",
        "prodPrice": "900",
        "prodDiscount": "10",
        "prodGender": "F",
        "prodCategory": "1",
        "prodMaterial": "Silk",
        "prodBrand": "new",
        "prodDescription": "Description",
        "prodIsActive": true,
        "prodSize": "null",
        "prodColor": null,
        "prodCatalogId": null,
        "createdby": null,
        "modifiedby": null,
        "createdDatetime": null,
        "modifiedDatetime": null
    },
     {
        "id": {
            "prodCode": "1113",
            "prodCmpCode": "2"
        },
        "prodName": "zzz",
        "prodPrice": "1000",
        "prodDiscount": "10",
        "prodGender": "M",
        "prodCategory": "1",
        "prodMaterial": "Silk",
        "prodBrand": "new",
        "prodDescription": "Description",
        "prodIsActive": true,
        "prodSize": "null",
        "prodColor": null,
        "prodCatalogId": null,
        "createdby": null,
        "modifiedby": null,
        "createdDatetime": null,
        "modifiedDatetime": null
    },
    {
        "id": {
            "prodCode": "1114",
            "prodCmpCode": "2"
        },
        "prodName": "kkkk",
        "prodPrice": "800",
        "prodDiscount": "15",
        "prodGender": "M",
        "prodCategory": "1",
        "prodMaterial": "Silk",
        "prodBrand": "new",
        "prodDescription": "Description",
        "prodIsActive": true,
        "prodSize": "null",
        "prodColor": null,
        "prodCatalogId": null,
        "createdby": null,
        "modifiedby": null,
        "createdDatetime": null,
        "modifiedDatetime": null
    }
]