import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ShopComponent} from './frontendportal/view/shop/shop.component';
import {ElementsComponent} from './frontendportal/view/elements/elements.component';
import {NewLoginComponent} from './frontendportal/view/new-login/new-login.component';
import {MyOrderComponent} from './frontendportal/view/myorder/myorder.component';
import {FashionOneComponent} from './frontendportal/view/home/fashion/fashion-one/fashion-one.component';
import {TitleComponent} from './frontendportal/view/elements/theme/title/title.component';
import {CollectionBannerComponent} from './frontendportal/view/elements/theme/collection-banner/collection-banner.component';
import {HomeSliderComponent} from './frontendportal/view/elements/theme/home-slider/home-slider.component';
import {CategoryComponent} from './frontendportal/view/elements/theme/category/category.component';
import {ThreeColumnComponent} from './frontendportal/view/shop/product/three-column/three-column.component';
import {CollectionNoSidebarComponent} from './frontendportal/view/shop/collection/collection-no-sidebar/collection-no-sidebar.component';
import {CompareComponent} from './frontendportal/view/shop/compare/compare.component';
import {SuccessComponent} from './frontendportal/view/shop/checkout/success/success.component';
import {CartComponent} from './frontendportal/view/shop/cart/cart.component';
import {CheckoutComponent} from './frontendportal/view/shop/checkout/checkout.component';

export const content: Routes = [
  {
    path: '',
    component: FashionOneComponent,
  },
  {
    path: 'home/fashion',
    component: FashionOneComponent,
  },
  {
    path: 'login',
    component: NewLoginComponent,
  },
  {
    path: 'myorder',
    component: MyOrderComponent
  },
  {
    path: 'shop',
    component: ShopComponent,
    children: [
      {
        path: 'product/three/column/:prodCode/:cmpCode',
        component: ThreeColumnComponent,
      },
      {
        path: 'collection/no/sidebar',
        component: CollectionNoSidebarComponent
      },

      {
        path: 'compare',
        component: CompareComponent
      },
      {
        path: 'checkout/success/:id',
        component: SuccessComponent
      },
      {
        path: 'cart',
        component: CartComponent
      },
      {
        path: 'checkout',
        component: CheckoutComponent
      }
    ]
  },
  {
    path: 'elements',
    component: ElementsComponent,
    children: [{
      path: 'theme/title',
      component: TitleComponent
    },
      {
        path: 'theme/collection-banner',
        component: CollectionBannerComponent
      },
      {
        path: 'theme/home-slider',
        component: HomeSliderComponent
      },
      {
        path: 'theme/category',
        component: CategoryComponent
      }]
  },
  {
    path: '**', // Navigate to Home Page if not found any page
    redirectTo: 'home/fashion',
  },
];
