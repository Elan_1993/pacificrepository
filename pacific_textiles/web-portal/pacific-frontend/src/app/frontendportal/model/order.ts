import { Product } from './product';

export interface OrderH {
    orderNo?: any;
    product?: Product;
    orderQty?: any;
    orderAmount?: any;
    orderState?: any;
    odrerStatus?: any;
    orderAddress?: any;
    orderShipAmount?: any;
    orderGrossAmount?:any;
    orderDiscountAmount?:any;
    orderExpectedCourier?:any;
    orderActualCourier?:any;
    orderTrackingId?:any;
    orderStateId?:any;
    orderStateName?:any;
    orderRpayNo?:any;
    orderRpayStatus?:any;
    orderRpayPaymentNo?:any;
    orderRpayPaymentStatus?:any;
    orderRpaySignature?:any;
    orderDetails?: OrderDetails[];
    createdDatetime?: any;
}

export interface OrderDetails {
    id?: OrderDId;
    orderRate?: number;
    orderQty?: number;
    orderGrossAmt?: number;
    ordertaxAmt?: number;
    orderDisc?: number;
    orderNetAmt?: number;
    imgSrc?: any;
}



export interface OrderDId {
    orderNo: any;
    orderCmpCode?: any;
    orderProdCode?: any;
}
// Order
export interface Order {
    shippingDetails?: any;
    product?: Product;
    orderId?: any;
    totalAmount?: any;
}
