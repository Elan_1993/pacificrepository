// Products
export interface Product {
    id?: string;
    title?: string;
    description?: string;
    type?: string;
    brand?: string;
    collection?: any[];
    category?: string;
    prodWeightType?: string;
    prodWeight?: number;
    price?: number;
    sale?: boolean;
    discount?: number;
    discountPrice?:number;
    stock?: number;
    newProduct?: boolean;
    quantity?: number;
    tags?: any[];
    variants?: Variants[];
    images?: Images[];
    alt?: string;
    src?: string;
    size?: string;
    color?: string;
    cmpCode?: string;
    prodCatalogId?: number;
    discountTotalPrice?:number;

}

export interface Variants {
    variant_id?: number;
    id?: string;
    sku?: string;
    size?: string;
    color?: string;
    image_id?: number;
}

export interface Images {
    image_id?: number;
    id?: string;
    alt?: string;
    src?: string;
    variant_id?: any[];
}
