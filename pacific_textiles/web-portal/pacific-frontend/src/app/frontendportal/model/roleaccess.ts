import {Injectable} from '@angular/core';

@Injectable()
export class RoleAccess {
  screenID: number;
  moduleID: number;
  create: boolean;
  edit: boolean;
  view: boolean;
  delete: boolean;
}