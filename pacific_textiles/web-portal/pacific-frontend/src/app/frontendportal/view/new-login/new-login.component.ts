import { Component, OnInit, ViewChild, TemplateRef, PLATFORM_ID, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MasterService } from '../../service/master.service';
import { User } from '../../model/user';
import { DataService } from '../../service/data.service';
import { first } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-new-login',
  templateUrl: './new-login.component.html',
  styleUrls: ['./new-login.component.scss']
})

export class NewLoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMsg: string;
  user: FormGroup;
  loading = false;
  submitted = false;
  fullName = '';
  show: boolean = false;
  confirmShow: boolean = false;

  @ViewChild('loginView', { static: false }) LoginView: TemplateRef<any>;
  public closeResult: string;
  public modalOpen = false;
  public validRegistration: boolean;
  constructor(private formBuilder: FormBuilder, @Inject(PLATFORM_ID) private platformId: Object,
              private router: Router, private modalService: NgbModal, private http: HttpClient,
              private route: ActivatedRoute,
              private dataService: DataService,
              private masterService: MasterService) {
    this.user = this.formBuilder.group({
      userName: new FormControl('', [Validators.required]),
      userPhone: new FormControl('', [Validators.required]),
      userMail: new FormControl('', [Validators.required]),
      userPassword: new FormControl('', [Validators.required]),
      userConfirmPassword: new FormControl('', [Validators.required])
    });
  }

  password() {
    this.show = !this.show;
  }

  confirmPassword(){
    this.confirmShow = !this.confirmShow;
  }

  get f() {
    return this.user.controls;
  }

  loginMessage() {
    return this.errorMsg;
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      loginEmail: [''],
      loginPassWord: [''],
    });    
  }

  onSubmit() {
    this.errorMsg = '';
    const oauthHttpOptions = {};
    this.http.post(`${environment.apiUrl}` + '/oauth/token?grant_type=password&scope=user_info&username='
        + this.loginForm.get('loginEmail').value + '&password=' + this.loginForm.get('loginPassWord').value, oauthHttpOptions)
        .subscribe((resToken: any) => {
          sessionStorage.setItem('frontend_access_token', resToken.access_token);
          sessionStorage.setItem('LoginUserPwd' , this.loginForm.get('loginPassWord').value);
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + sessionStorage.getItem('frontend_access_token')
            })
          };

          this.http.post(`${environment.externalApiUrl}/get-user-info`, JSON.stringify({
            userCode: this.loginForm.get('loginEmail').value,
            password: this.loginForm.get('loginPassWord').value,
            companyUserMenu: false
          }), httpOptions).subscribe(
              (res: User) => {
                if (res.loginStatus) {
                  this.dataService.setUser(res);
                  this.errorMsg = res.message;
                  this.router.navigate(['./frontendlanding/home/fashion']);
                } else {
                  this.errorMsg = res.message;
                }
              }, (err: HttpErrorResponse) => {
                console.error(err.message);
                this.errorMsg = 'Login Failed';
              }
          );
        }, (err: HttpErrorResponse) => {
          console.error(err.message);
          this.errorMsg = 'Login Failed';
        });
  }

  registerValidation(): boolean {
    if (this.user.value.userName == '' || this.user.value.userName === undefined) {
      alert('Enter name');
      return false;
    }
    if (this.user.value.userPhone === '' || this.user.value.userPhone === undefined) {
      alert('Enter mobileNo');
      return false;
    }
    if (this.user.value.userMail === '' || this.user.value.userMail === undefined) {
      alert('Enter email');
      return false;
    }
    if (this.user.value.userPassword === '' || this.user.value.userPassword === undefined) {
      alert('Enter password');
      return false;
    }
    if (this.user.value.userConfirmPassword === '' || this.user.value.userConfirmPassword === undefined) {
      alert('Enter confirmPassword');
      return false;
    }
    if (this.user.value.userPassword !== this.user.value.userConfirmPassword) {
      alert('Password not matched');
      return false;
    }
    return true;
  }

  onRegister(): void {
    this.submitted = true;
    this.loading = true;
    if (!this.registerValidation()) {
      return;
    }

    this.masterService.registerUser(this.user.value).pipe(first())
        .subscribe(
            data => {                            
              if (!data.status){
               alert(data.message);
               return;
              }              
              this.loading = false;
              this.submitted = false;
              this.redirectLogin(this.user.value);
            },
            error => {
              this.loading = false;
              this.submitted = false;
              console.log('=failed====');
            });
  }

  redirectLogin(userValue) {    
    this.errorMsg = '';
    const oauthHttpOptions = {};
    this.http.post(`${environment.apiUrl}` + '/oauth/token?grant_type=password&scope=user_info&username='
        + userValue.userMail + '&password=' + userValue.userPassword, oauthHttpOptions)
        .subscribe((resToken: any) => {         
          sessionStorage.setItem('frontend_access_token', resToken.access_token);
          sessionStorage.setItem('LoginUserPwd', userValue.userPassword);
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + sessionStorage.getItem('frontend_access_token')
            })
          };

          this.http.post(`${environment.externalApiUrl}/get-user-info`, JSON.stringify({
            userCode: userValue.userMail,
            password: userValue.userPassword,
            companyUserMenu: false
          }), httpOptions).subscribe(
              (res: User) => {
                if (res.loginStatus) {
                  this.dataService.setUser(res);
                  this.errorMsg = res.message;
                  this.router.navigate(['./frontendlanding/home/fashion']);
                } else {
                  this.errorMsg = res.message;
                }
              }, (err: HttpErrorResponse) => {
                console.error(err.message);
                this.errorMsg = 'Login Failed';
              }
          );
        }, (err: HttpErrorResponse) => {
          console.error(err.message);
          this.errorMsg = 'Login Failed';
        });
  }
  
  openModal() {
    this.modalOpen = true;
    if (isPlatformBrowser(this.platformId)) { // For SSR
      this.modalService.open(this.LoginView, {
        size: 'lg',
        ariaLabelledBy: 'modal-basic-title',
        centered: true,
        windowClass: 'Login'
      }).result.then((result) => {
        `Result ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
