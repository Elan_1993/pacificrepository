import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';


@Injectable()
export class AuthGuard {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkLoggedIn(state.url);
  }

  checkLoggedIn(url: string): boolean {
    const status = sessionStorage.getItem('screenEnabled');

    if (status !== 'success' && (url.includes('/frontendlanding') || url.includes('/landing'))) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
