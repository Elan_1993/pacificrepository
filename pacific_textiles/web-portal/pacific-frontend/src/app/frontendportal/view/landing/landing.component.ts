import {Component} from '@angular/core';
import {trigger, transition, useAnimation} from '@angular/animations';
import {fadeIn} from 'ng-animate';


@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss'],
    animations: [
        trigger('animateRoute', [transition('* => *', useAnimation(fadeIn, {
            // Set the duration to 5seconds and delay to 2 seconds
            // params: { timing: 3}
        }))])
    ]
})
export class LandingComponent {


    constructor() {


    }

    public getRouterOutletState(outlet) {
        return outlet.isActivated ? outlet.activatedRoute : '';
    }


}
