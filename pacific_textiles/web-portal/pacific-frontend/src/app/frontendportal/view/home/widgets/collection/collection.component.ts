import { Component, OnInit, Input } from '@angular/core';
import { CollectionSlider } from '../../../../model/slider';

@Component({
  selector: 'app-widget-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionWidgetComponent implements OnInit {

  @Input() categories: any[];
  @Input() category: string;
  @Input() class: string;

  constructor() { }

  ngOnInit(): void {
  }

   public CollectionSliderConfig: any = CollectionSlider;

}
