import { Component, OnInit } from '@angular/core';
import { ProductSlider } from '../../../../model/slider';
import { Product } from '../../../../model/product';
import { ProductService } from '../../../../service/product.service';

@Component({
  selector: 'app-fashion-one',
  templateUrl: './fashion-one.component.html',
  styleUrls: ['./fashion-one.component.scss']
})
export class FashionOneComponent implements OnInit {

  public products: Product[] = [];
  public newCollectionProducts: Product[] = [];
  public trendingCollectionProducts: Product[] = [];
  public specialOfferProducts: Product[] = [];
  public productCollections: any[] = [];
  public specialOfferAvailable: boolean = false;

  constructor(public productService: ProductService) {
    this.specialOfferAvailable = false;
    // tslint:disable-next-line:triple-equals
    this.productService.getDashBoardProducts({
      cusType: Number( sessionStorage.getItem('paidUser'))
    }).subscribe(data => {
      Object.entries(data).map(key => {
        if (key[0] == "New Collections") {
          this.newCollectionProducts = key[1];
        } else if (key[0] == "Special Offers") {
          this.specialOfferAvailable = true;
          this.specialOfferProducts = key[1];
        } else if ((key[0] == "Trending Collections")) {
          this.trendingCollectionProducts = key[1];
        }
      });
    });
  }

  public ProductSliderConfig: any = ProductSlider;

  public sliders = [{
    title: 'welcome to fashion',
    image: 'assets/images/saree-1.jpg'
  }, {
    title: 'welcome to fashion',
    image: 'assets/images/saree-2.jpg'
  }]

  // Collection banner
  public collections = [{
    image: 'assets/images/saree-1.jpg',
    save: 'save 50%',
    title: 'men'
  }, {
    image: 'assets/images/saree-2.jpg',
    save: 'save 50%',
    title: 'women'
  }];

  // Blog
  public blog = [{
    image: 'assets/images/blog/1.jpg',
    date: '25 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/images/blog/2.jpg',
    date: '26 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/images/blog/3.jpg',
    date: '27 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/images/blog/4.jpg',
    date: '28 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }];

  // Logo
  public logo = [{
    image: 'assets/images/logos/1.png',
  }, {
    image: 'assets/images/logos/2.png',
  }, {
    image: 'assets/images/logos/3.png',
  }, {
    image: 'assets/images/logos/4.png',
  }, {
    image: 'assets/images/logos/5.png',
  }, {
    image: 'assets/images/logos/6.png',
  }, {
    image: 'assets/images/logos/7.png',
  }, {
    image: 'assets/images/logos/8.png',
  }];

  ngOnInit(): void {

  }

  // Product Tab collection
  getCollectionProducts(collection) {
    return this.products.filter((item) => {
      if (item.collection.find(i => i === collection)) {
        return item
      }
    })
  }

}
