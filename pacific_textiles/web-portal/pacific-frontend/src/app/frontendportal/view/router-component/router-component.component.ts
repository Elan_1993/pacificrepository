import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-router-component',
    templateUrl: './router-component.component.html',
    styleUrls: ['./router-component.component.scss']
})
export class RouterComponentComponent implements OnInit {

    constructor(private router: Router) {
        sessionStorage.setItem('screenEnabled', 'success');
        this.router.navigate(['./frontendlanding/login']);
    }

    ngOnInit(): void {
    }

}
