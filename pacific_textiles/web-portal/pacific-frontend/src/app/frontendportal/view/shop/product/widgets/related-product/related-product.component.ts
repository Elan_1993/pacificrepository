import {Component, Input, OnChanges} from '@angular/core';
import { Product } from '../../../../../model/product';
import { ProductService } from '../../../../../service/product.service';

@Component({
  selector: 'app-related-product',
  templateUrl: './related-product.component.html',
  styleUrls: ['./related-product.component.scss']
})
export class RelatedProductComponent  implements OnChanges{

  @Input() type: string;
  @Input() productCode: string;
  @Input() catalogId: number;

  public products: Product[] = [];

  constructor(public productService: ProductService) {

  }

  ngOnChanges(){
        this.productService.getAllProducts({ first: 0, rows : 100, conditions : {
                category: this.type,
                prodCatalogId: this.catalogId,
                prodExcludeCode: this.productCode,
                stkQtyNonZeroEnable: true
            },
            cusType: Number( sessionStorage.getItem('paidUser')) }).subscribe(response =>
            this.products = response
        );
    }


}
