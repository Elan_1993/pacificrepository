import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ProductDetailsMainSlider, ProductDetailsThumbSlider } from '../../../../model/slider';
import { Product } from '../../../../model/product';
import { ProductService } from '../../../../service/product.service';
import { SizeModalComponent } from '../../../modal/size-modal/size-modal.component';
import {Observable} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-three-column',
  templateUrl: './three-column.component.html',
  styleUrls: ['./three-column.component.scss']

})
export class ThreeColumnComponent implements OnInit {

  state: Observable<object>;

  public product: Product = {};
  public counter = 1;
  public activeSlide: any = 0;
  public selectedSize: any;



  @ViewChild('sizeChart') SizeChart: SizeModalComponent;

  public ProductDetailsMainSliderConfig: any = ProductDetailsMainSlider;

  constructor(private route: ActivatedRoute, private router: Router,
              public productService: ProductService) {
    route.params.subscribe(params => {
      this.productService.getAllProductsByID(params.prodCode, params.cmpCode).pipe(first()).subscribe(data => {
         this.product = data;
         this.counter = 1;
       });
      });
   }

  ngOnInit(): void {    
  }
  // Get Product Color
  Color(products) {
    const uniqColor = [];
    // for (let i = 0; i < Object.keys(variants).length; i++) {
    if (uniqColor.indexOf(products.color) === -1 && products.color) {
        uniqColor.push(products.color);
      }
    // }
    return uniqColor;
  }

  // Get Product Size
  Size(products) {
    const uniqSize = [];
    // for (let i = 0; i < Object.keys(variants).length; i++) {
    if (uniqSize.indexOf(products.size) === -1 && products.size) {
        uniqSize.push(products.size);
      }
    // }
    return uniqSize;
  }

  selectSize(size) {
    this.selectedSize = size;
  }

  // Increament
  increment() {
    this.counter++ ;
  }

  // Decrement
  decrement() {
    if (this.counter > 1) { this.counter-- ; }
  }

  // Add to cart
  async addToCart(product: any) {
    product.quantity = this.counter || 1;
    const status = await this.productService.addToCart(product);
    if (status) {
      this.router.navigate(['./frontendlanding/shop/cart']);
    }
  }

  // Buy Now
  async buyNow(product: any) {
    product.quantity = this.counter || 1;
    const status = await this.productService.addToCartFromBuyNow(product);
    if (status) {
      this.router.navigate(['./frontendlanding/shop/checkout']);
    }
  }

  // Add to Wishlist
  addToWishlist(product: any) {
    this.productService.addToWishlist(product);
  }

}
