import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Order, OrderH} from '../../../../model/order';
import {OrderService} from '../../../../service/order.service';
import {ProductService} from '../../../../service/product.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-success',
    templateUrl: './success.component.html',
    styleUrls: ['./success.component.scss']
})
export class SuccessComponent {

    public orderH: any;   

    constructor(private route: ActivatedRoute, private router: Router,
                private orderService: OrderService,
                public productService: ProductService) {
        route.params.subscribe(params => {           
            this.orderService.fetchOrderByOrderNUmber(params.id).subscribe((data: any) =>{               
                this.orderH = data;
            });
        });
    }

}
