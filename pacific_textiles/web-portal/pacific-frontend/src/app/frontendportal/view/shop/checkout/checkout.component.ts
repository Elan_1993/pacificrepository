import {Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {IPayPalConfig, ICreateOrderRequest} from 'ngx-paypal';
import {environment} from '../../../../../environments/environment';
import {Product} from '../../../model/product';
import {ProductService} from '../../../service/product.service';
import {OrderService} from '../../../service/order.service';
import {Order, OrderH} from 'src/app/frontendportal/model/order';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

    // @Input() themeLogo = 'assets/images/Indra-cart-logo.png'; // Default Logo
    public checkoutForm: FormGroup;
    public products: Product[] = [];
    public payPalConfig?: IPayPalConfig;
    public payment: string = 'Stripe';
    public amount: any = 0;
    public discAmount: any = 0;
    public weight: any;
    public shippingAmount: any = 0;
    public shippingAmountPerKg: any = 0;
    public totalAmout: any = 0;
    public orderH: OrderH[] = [];
    stateId = 0;
    courierId = 0;
    courierName = '';
    stateName = '';
    orderPlaced = false;
    key: String;
    stateList: any[] = [];
    courierList: any[] = [];
    stateCourierMap: any;
    paymentResponse: any;
    private orderResponse: OrderH;
    errorMsg: string;
    loginCheck =1;

    constructor(private fb: FormBuilder,
                public productService: ProductService,
                private orderService: OrderService,
                private router: Router,
                private http: HttpClient,) {
        this.checkoutForm = this.fb.group({
            address: ['', [Validators.required, Validators.maxLength(200)]],
        });
    }

    loginMessage() {
        return this.errorMsg;
    }

    ngOnInit(): void {
        this.orderService.fetchStateCourierList(101).subscribe(
            (res: any) => {
                this.stateList = res.stateList;
                this.courierList = res.courierList;
                this.stateCourierMap = res.courierPriceList;
            });        
        this.allAmountCalculation();
    }
    allAmountCalculation(): void{
        this.productService.cartItems.subscribe(response => this.products = response);        
        this.getTotal.subscribe(amount => this.amount = Math.floor(amount));
        this.getDiscAmount.subscribe(discAmount => this.discAmount = discAmount);
        this.getShipping.subscribe(weight => this.weight = weight);
        this.amount = this.amount - this.discAmount;
        this.getShippingTotal();
    }


    getShippingTotal(): void {
        let weightInkg = this.weight / 1000;
        weightInkg = Math.ceil(weightInkg);
        this.shippingAmount = weightInkg * this.shippingAmountPerKg;
        this.totalAmout = this.amount + this.shippingAmount;
        this.totalAmout=Math.floor(this.totalAmout);    

    }

    public get getDiscAmount(): Observable<number> {
        return this.productService.cartDiscAmount();
    }

    public get getTotal(): Observable<number> {
        return this.productService.cartTotalAmount();
    }

    public get getShipping(): Observable<number> {
        return this.productService.cartTotalshipping();
    }

    stateChange(e) {
        this.stateId = e.currentTarget.options.selectedIndex;
        this.stateName = e.target.value;
        this.calculateShippingCharge();
    }

    courierChange(e) {
        this.courierId = e.currentTarget.options.selectedIndex;
        this.courierName = e.target.value;
        this.calculateShippingCharge();
    }

    calculateShippingCharge() {
        if (this.courierId == 0 || this.stateId == 0) {
            this.shippingAmountPerKg = 0.00;
            this.getShippingTotal();
            return;
        }
        this.key = this.courierId + '-' + this.stateId;
        Object.entries(this.stateCourierMap).map(key => {
            if (key[0] === this.key) {
                this.shippingAmountPerKg = key[1];
            }
        });
        this.getShippingTotal();
    }

    placeOrder() {        
        if (sessionStorage.getItem('loginUserCode') == null) {
            alert('Please login to proceed');
            this.router.navigate(['/login']);
            return;
        }
        if (this.checkoutForm.value.address == '') {
            alert('Please provide the address');
            return;
        }
        if (this.stateId == 0) {
            alert('Please Select State');
            return;
        }
        if (this.courierId == 0) {
            alert('Please Select Courier');
            return;
        }
        this.orderPlaced=true;
        const orderDList = [];
        var orderH = {};
        var totalQty = 0;
        var grossAmount = 0;
        for (const product of this.products) {
            totalQty += product.quantity;
            grossAmount += product.quantity * product.price;
            orderDList.push({
                id: {orderNo: null, orderCmpCode: product.cmpCode, orderProdCode: product.id},
                orderRate: product.price,
                orderQty: product.quantity,
                orderGrossAmt: product.quantity * product.price,
                orderDCusCode: sessionStorage.getItem('loginUserCode'),
                ordertaxAmt: 0,               
                orderDisc: (product.quantity * product.price) * (product.discount / 100),
                orderNetAmt: (product.quantity * product.price) - (product.quantity * product.price) * (product.discount / 100)
            });
        }
        orderH = {
            orderQty: totalQty,
            orderGrossAmount: grossAmount,
            orderShipAmount: this.shippingAmount,
            orderDiscountAmount: this.discAmount,
            orderAmount: this.totalAmout,
            orderState: 1,
            orderStatus: 'Created',
            orderCusCode: sessionStorage.getItem('loginUserCode'),
            orderAddress: this.checkoutForm.value.address,
            orderExpectedCourier: this.courierName,            
            orderStateId: this.stateId,
            orderStateName: this.stateName,
            orderDs: orderDList
        };       
        this.orderService.saveOrder(orderH).pipe(first())
            .subscribe(
                (data: any) => {                   
                    if (data.responseCode === 91) {
                        alert(data.message);
                        this.orderPlaced = false;
                        this.errorMsg = data.message;                                              
                        // this.router.navigate(['./frontendlanding/shop/cart']);
                    }else if(data.responseCode === 0){                        
                          this.orderResponse = {};
                          this.orderResponse  = data.object;
                            var self = this;
                            const opt = {
                                "key": environment.razorpay_secret_key,
                                "amount": (this.orderResponse.orderAmount * 100).toString,
                                "currency": "INR",
                                "name": "Indra Cart",
                                "description": "Fashion for life",
                                "image": "http://localhost:4200/assets/images/Indra-cart-logo.png",
                                "order_id": this.orderResponse.orderRpayNo,
                                "handler": function (response){
                                  self.paymentResponse = response;                                 
                                  self.checkMethod(self.paymentResponse);
                              },
                                "prefill": {
                                    "name": sessionStorage.getItem('loginUserCode'),
                                    "email":sessionStorage.getItem('loginUserMail'),
                                    "contact": ""
                                },
                                "notes": {
                                    "address": "IndraCart Corporate Office"
                                },
                                "theme": {
                                    "color": "#3399cc"
                                },
                                "modal": {
                                    "ondismiss": function(){   
                                       rzp1.close();          
                                       self.rpayModalClose();                          
                                    //    self.orderPlaced=false;
                                    }
                                }
                              };
                              var rzp1 = new this.orderService.nativeWindow.Razorpay(opt);                              
                              rzp1.on('payment.failed', function (response){
                            });
                            rzp1.open();
                    }
                },
                error => {
                    // alert(this.loginCheck);                    
                    if(this.loginCheck == 1){
                        this.loginCheck +=1;
                        this.refreshLogin();
                       
                    }else{                        
                        alert('Order Save failed');
                        this.orderPlaced = false;            
                    }                    
                });
    }

    private refreshLogin(): void{        
        const oauthHttpOptions = {};
        this.http.post(`${environment.apiUrl}` + '/oauth/token?grant_type=password&scope=user_info&username='
            + sessionStorage.getItem('loginUserPhone') + '&password=' + sessionStorage.getItem('LoginUserPwd'), oauthHttpOptions)
            .subscribe((resToken: any) => {              
              sessionStorage.setItem('frontend_access_token', resToken.access_token);
              this.placeOrder();              
           });
    }

    private rpayModalClose():void{                       
        this.orderService.stockReversal(this.orderResponse).subscribe((data: any) => {   
            this.orderPlaced = false;
        });        
    }

    private checkMethod(response): void {                  
            this.orderResponse.orderRpayPaymentNo = response.razorpay_payment_id,
            this.orderResponse.orderRpaySignature = response.razorpay_signature;    
        this.orderService.checkPaymentSiganture(this.orderResponse).subscribe((data: any) => {                                                     
                    if (data != null) {                            
                        this.shippingAmount = 0;
                        this.amount = 0;
                        this.totalAmout = 0;
                        location.href = environment.successScreen+data.orderNo;                       
                        this.productService.removeCartAfterOrderPlaced(this.products);
                    } else {
                        // this.router.navigate(['./frontendlanding/shop/checkout/failure']);
                    }
                });
    }
     // Increament
  increment(product, qty = 1) {
    this.productService.updateCartQuantity(product, qty);
    this.allAmountCalculation();
  }

  // Decrement
  decrement(product, qty = -1) {
    this.productService.updateCartQuantity(product, qty);
    this.allAmountCalculation();
  }

  public removeItem(product: any) {
    if (window.confirm('Are you want to delete?')) {
    this.productService.removeCartItem(product);
    this.allAmountCalculation();
    }
  }
  productDetail(product: any){  
    this.router.navigate(['/shop/product/three/column/'+product.id+'/'+product.cmpCode]);
  }
}
