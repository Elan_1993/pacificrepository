import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductService } from "../../../service/product.service";
import { Product } from "../../../model/product";
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public products: Product[] = [];

  constructor(public productService: ProductService,private router: Router) {
    this.productService.cartItems.subscribe(response => this.products = response);
  }

  ngOnInit(): void {
  }

  public get getTotal(): Observable<number> {    
    return this.productService.cartTotalAmountWithDisc();
  }

  // Increament
  increment(product, qty = 1) {
    this.productService.updateCartQuantity(product, qty);
  }

  // Decrement
  decrement(product, qty = -1) {
    this.productService.updateCartQuantity(product, qty);
  }

  public removeItem(product: any) {
    if (window.confirm('Are you want to delete?')) {
    this.productService.removeCartItem(product);
    }
  }

  productDetail(product: any){  
    this.router.navigate(['/shop/product/three/column/'+product.id+'/'+product.cmpCode]);
  }
}
