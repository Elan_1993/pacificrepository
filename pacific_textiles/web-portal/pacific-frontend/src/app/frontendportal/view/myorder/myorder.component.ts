import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { OrderService } from '../../service/order.service';
import { OrderH } from 'src/app/frontendportal/model/order';
import {ProductService} from '../../service/product.service';
import {shareTextToWhatsApp} from 'share-text-to-whatsapp';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {environment} from '../../../../environments/environment';


@Component({
  selector: 'app-myorder',
  templateUrl: './myorder.component.html',
  styleUrls: ['./myorder.component.scss']
})
export class MyOrderComponent implements OnInit {

  public orders: OrderH[] = [];
  public singleOrder: any;
  loginCheck =1;

  constructor(
    private orderService: OrderService,public productService: ProductService, private http: HttpClient) {
  }

  toDate = new Date();
  fromDate = new Date(this.toDate.getFullYear(), this.toDate.getMonth(), 1, 1);

  ngOnInit() {
    this.fetchOrdersByDate(0);
  }

  fetchOrdersByDate(value: any): void {
    this.toDate = new Date();
    if (value != 0) {
      this.fromDate = new Date(this.toDate.getFullYear(), this.toDate.getMonth() - value, 1);
      this.toDate = new Date(this.toDate.getFullYear(), this.toDate.getMonth() + 0, 0);
    } else {
      this.toDate = new Date();
      this.fromDate = new Date(this.toDate.getFullYear(), this.toDate.getMonth(), 1, 1);
    }
    this.fetchOrder();
  }

  fetchOrder(): void {   
      this.orderService.fetchOrderByUser({
      userCode: sessionStorage.getItem('loginUserCode'),
      fromDate: this.fromDate,
      toDate: this.toDate
    }).pipe(first()).subscribe((data: any) => {         
      this.orders = data;
    },
    error => {     
      if(this.loginCheck == 1){
          this.loginCheck +=1;
          this.refreshLogin();         
      }else{
          alert('Network seems to be low');
      }                    
  });
  }

  private refreshLogin(): void{
    const oauthHttpOptions = {};
    const pwd= null;    
    this.http.post(`${environment.apiUrl}` + '/oauth/token?grant_type=password&scope=user_info&username='
        + sessionStorage.getItem('loginUserPhone') + '&password=' + sessionStorage.getItem('LoginUserPwd')  , oauthHttpOptions)
        .subscribe((resToken: any) => {                      
          sessionStorage.setItem('frontend_access_token', resToken.access_token);
          this.fetchOrder();             
       });
}

  fetchParticularOrder(orderNo: string) {   
    this.orderService.fetchOrderByOrderNUmber(orderNo).pipe(first()).subscribe((data: any) => {    
      this.singleOrder = data;
    });
  }
  shareTracking(orderh: OrderH){    
    const msg = orderh.orderActualCourier +"--"+orderh.orderTrackingId;
    shareTextToWhatsApp(msg);
  }
}


