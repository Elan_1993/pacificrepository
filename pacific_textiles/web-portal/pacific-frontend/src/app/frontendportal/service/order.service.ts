import { Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OrderH } from '../model/order';
import {environment} from '../../../environments/environment';

const state = {
  checkoutItems: JSON.parse(localStorage['checkoutItems'] || '[]')
}

function _window() : any {  
  return window;
}

@Injectable({
  providedIn: 'root'
})
export class OrderService{

  get nativeWindow() : any {   
    return _window();
  }
 
 httpOptions = {}; 

constructor(private router: Router, private http: HttpClient) { }
  
  private httpOptionsProvider(): void {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + sessionStorage.getItem('frontend_access_token')
      })
    };
  }
  // Get Checkout Items
  public get checkoutItems(): Observable<any> {
    const itemsStream = new Observable(observer => {
      observer.next(state.checkoutItems);
      observer.complete();
    });
    return <Observable<any>> itemsStream;
  }

  saveOrder(order: OrderH) {        
    this.httpOptionsProvider();    
    return this.http.post(`${environment.orderApiUrl}/save-order`, JSON.stringify(order), this.httpOptions);
  }
  
  fetchOrderByUser(customInputObject: any) {
    this.httpOptionsProvider();
    return this.http.post(`${environment.orderApiUrl}/fetch-orders`, JSON.stringify(customInputObject), this.httpOptions);
  }

  fetchStateCourierList(countryCode: number): any {
    this.httpOptionsProvider();
    return this.http.get(`${environment.externalApiUrl}/controller/load-state-courier-dropdowns/` + countryCode, this.httpOptions);
  }

  fetchOrderByOrderNUmber(orderNumber: any) {
    this.httpOptionsProvider();
    return this.http.get(`${environment.orderApiUrl}/get-order-by-id/` + orderNumber, this.httpOptions);
  }

  checkPaymentSiganture(order: OrderH) {
    this.httpOptionsProvider();
    return this.http.post(`${environment.orderApiUrl}/check-signature`, JSON.stringify(order), this.httpOptions);
  }
  stockReversal(order: OrderH){
    this.httpOptionsProvider();   
   return this.http.post(`${environment.orderApiUrl}/stock-reversal`, JSON.stringify(order), this.httpOptions);
  }
}



