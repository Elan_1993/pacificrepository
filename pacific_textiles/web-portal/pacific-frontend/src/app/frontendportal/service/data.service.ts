import { Injectable } from '@angular/core';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() {
  }

  setUser(userIn: User) {   
    sessionStorage.setItem('user', JSON.stringify(userIn));
    sessionStorage.setItem('loginstatus', 'success');
    sessionStorage.setItem('loginUserCode', userIn.userCode);
    sessionStorage.setItem('loginUserCmpCode', userIn.userCmpCode);
    sessionStorage.setItem('loginUserPhone', userIn.userPhone);
    sessionStorage.setItem('loginUserMail', userIn.userMail);
    sessionStorage.setItem('loginUserName', userIn.userName);
    if (userIn.userType === 3 || userIn.userType === 2 || userIn.userType === 1){
      sessionStorage.setItem('paidUser', String(userIn.userType));    
    } else {
      sessionStorage.setItem('paidUser', String(3));
    }
  }
}
