import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MasterService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + sessionStorage.getItem('frontend_access_token')
        })
    };

    constructor(private http: HttpClient) {
    }

    registerUser(user: any): Observable<any> {
        return this.http.post(`${environment.externalApiUrl}/controller/register-user`, JSON.stringify(user), this.httpOptions);
    }

    // logOutUser():any{       
    //     alert(sessionStorage.getItem('frontend_access_token'));
    // return this.http.post(`${environment.externalApiUrl}/logout`, this.httpOptions);
    // }
}
