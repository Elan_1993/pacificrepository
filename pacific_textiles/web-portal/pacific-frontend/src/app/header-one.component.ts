import { Component, OnInit, Input, HostListener , NgModule} from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from './frontendportal/service/product.service';
import { MasterService } from './frontendportal/service/master.service';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';



@Component({
  selector: 'app-header-one',
  templateUrl: './header-one.component.html',
  styleUrls: ['./header-one.component.scss']
})
export class HeaderOneComponent implements OnInit {
  allProducts = [];
  // allData = [];
  selectedItem = {};
  errorProdCode = false;
  isLoginEnable = false;
  keyword = 'prodCode';
  @Input() class: string;
  @Input() themeLogo = 'assets/images/Indra-cart-logo.png'; // Default Logo
  @Input() topbar = true; // Default True
  @Input() sticky = false; // Default false

  public stick = false; 
  constructor(private productService: ProductService, private http: HttpClient, private router: Router,private masterService : MasterService) { }

  ngOnInit(): void {
    const loginUser = sessionStorage.getItem('loginUserCode');
    this.isLoginEnable = (loginUser == null) ? false : true;  
  }
  
  getMyOrders(){
    this.router.navigate(['./frontendlanding/your/order']);
  }

  logOut(){    
    this.isLoginEnable = false;   
    this.http.post(environment.externalApiUrl + '/logout', {}, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + sessionStorage.getItem('frontend_access_token')
      })
    }).subscribe((res: any) => {
      sessionStorage.clear();      
      this.router.navigate(['./frontendlanding/login']);     
    });
  }
  selectEvent(item) {
    this.selectedItem = item;
  }
  productDetail(prodCode, prodCmpCode){
    const prodCode1 = prodCode.replace(/<[^>]*>/g, '');
    this.router.navigate(['./frontendlanding/shop/product/three/column/' + prodCode1 + '/' + prodCmpCode]);
  }

  onChangeSearch(val: any) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    this.productService.getProductsByAutocomplete(val).subscribe((data: any[]) => {
      this.allProducts = data;
    });
  }

  onFocused(e) {
    setTimeout(() => { // this will make the execution after the above boolean has changed
      this.errorProdCode = false;
    }, 0);
  }

  // @HostListener Decorator
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  	 if (number >= 150 && window.innerWidth > 400) {
  	  this.stick = true;
  	} else {
  	  this.stick = false;
  	}
  }

}
