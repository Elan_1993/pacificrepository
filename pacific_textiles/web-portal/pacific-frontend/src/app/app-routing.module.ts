import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {content} from './app-routes';
import {AuthGuard} from './frontendportal/view/new-login/auth-guard.service';
import {LandingComponent} from './frontendportal/view/landing/landing.component';
import {RouterComponentComponent} from './frontendportal/view/router-component/router-component.component';

const routes: Routes = [
    {
        path: '',
        component: RouterComponentComponent,
    },
    {
        path: 'frontendlanding',
        component: LandingComponent,
        canActivate: [AuthGuard],
        children: content
    }, {
        path: '**',
        redirectTo: '/login'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled',
        useHash: false,
        anchorScrolling: 'enabled',
        scrollPositionRestoration: 'enabled',
        relativeLinkResolution: 'legacy'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}