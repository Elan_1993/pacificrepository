import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule} from '@ngx-translate/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ShopComponent } from './frontendportal/view/shop/shop.component';
import { ElementsComponent } from './frontendportal/view/elements/elements.component';
import { NewLoginComponent } from './frontendportal/view/new-login/new-login.component';
import {FieldErrorDisplayComponent} from './frontendportal/view/field-error-display/field-error-display.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

// Header and Footer Components

import {HeaderOneComponent} from './header-one.component';
import {FooterOneComponent} from './footer-one.component';
import {FashionOneComponent} from './frontendportal/view/home/fashion/fashion-one/fashion-one.component';
import {ToolsComponent} from './frontendportal/view/home/tools/tools.component';
import {SliderComponent} from './frontendportal/view/home/widgets/slider/slider.component';
import {LogoComponent} from './frontendportal/view/home/widgets/logo/logo.component';
import {InstagramComponent} from './frontendportal/view/home/widgets/instagram/instagram.component';
import {ServicesComponent} from './frontendportal/view/home/widgets/services/services.component';
import {CollectionWidgetComponent} from './frontendportal/view/home/widgets/collection/collection.component';
import {LeftMenuComponent} from './frontendportal/view/left-menu/left-menu.component';
import {MenuComponent} from './frontendportal/view/menu/menu.component';
import {SettingsComponent} from './frontendportal/view/settings/settings.component';
import {BreadcrumbComponent} from './frontendportal/view/breadcrumb/breadcrumb.component';
import {CategoriesComponent} from './frontendportal/view/categories/categories.component';
import {ProductBoxOneComponent} from './frontendportal/view/product/product-box-one/product-box-one.component';
import {ProductBoxThreeComponent} from './frontendportal/view/product/product-box-three/product-box-three.component';
import {ProductBoxVerticalSliderComponent} from './frontendportal/view/product/product-box-vertical-slider/product-box-vertical-slider.component';
import {NewsletterComponent} from './frontendportal/view/modal/newsletter/newsletter.component';
import {QuickViewComponent} from './frontendportal/view/modal/quick-view/quick-view.component';
import {CartModalComponent} from './frontendportal/view/modal/cart-modal/cart-modal.component';
import {CartVariationComponent} from './frontendportal/view/modal/cart-variation/cart-variation.component';
import {VideoModalComponent} from './frontendportal/view/modal/video-modal/video-modal.component';
import {SizeModalComponent} from './frontendportal/view/modal/size-modal/size-modal.component';
import {AgeVerificationComponent} from './frontendportal/view/modal/age-verification/age-verification.component';
import {SkeletonProductBoxComponent} from './frontendportal/view/skeleton/skeleton-product-box/skeleton-product-box.component';
import {TapToTopComponent} from './frontendportal/view/tap-to-top/tap-to-top.component';
import {DiscountPipe} from './frontendportal/model/pipes/discount.pipe';
import {CommonModule} from '@angular/common';
import {CarouselModule} from 'ngx-owl-carousel-o';
import {BarRatingModule} from 'ngx-bar-rating';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {NgxSkeletonLoaderModule} from 'ngx-skeleton-loader';
import {RouterModule} from '@angular/router';
import {TitleComponent} from './frontendportal/view/elements/theme/title/title.component';
import {CollectionBannerComponent} from './frontendportal/view/elements/theme/collection-banner/collection-banner.component';
import {HomeSliderComponent} from './frontendportal/view/elements/theme/home-slider/home-slider.component';
import {CategoryComponent} from './frontendportal/view/elements/theme/category/category.component';
import {MyOrderComponent} from './frontendportal/view/myorder/myorder.component';
import {GalleryModule} from '@ks89/angular-modal-gallery';
import {ThreeColumnComponent} from './frontendportal/view/shop/product/three-column/three-column.component';
import {SocialComponent} from './frontendportal/view/shop/product/widgets/social/social.component';
import {RelatedProductComponent} from './frontendportal/view/shop/product/widgets/related-product/related-product.component';
import {CollectionNoSidebarComponent} from './frontendportal/view/shop/collection/collection-no-sidebar/collection-no-sidebar.component';
import {GridComponent} from './frontendportal/view/shop/collection/widgets/grid/grid.component';
import {PaginationComponent} from './frontendportal/view/shop/collection/widgets/pagination/pagination.component';
import {BrandsComponent} from './frontendportal/view/shop/collection/widgets/brands/brands.component';
import {ColorsComponent} from './frontendportal/view/shop/collection/widgets/colors/colors.component';
import {SizeComponent} from './frontendportal/view/shop/collection/widgets/size/size.component';
import {PriceComponent} from './frontendportal/view/shop/collection/widgets/price/price.component';
import {CompareComponent} from './frontendportal/view/shop/compare/compare.component';
import {SuccessComponent} from './frontendportal/view/shop/checkout/success/success.component';
import {NgxPayPalModule} from 'ngx-paypal';
import {Ng5SliderModule} from 'ng5-slider';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {CheckoutComponent} from './frontendportal/view/shop/checkout/checkout.component';
import {CartComponent} from './frontendportal/view/shop/cart/cart.component';
import {LandingComponent} from './frontendportal/view/landing/landing.component';
import {AuthGuard} from './frontendportal/view/new-login/auth-guard.service';
import { RouterComponentComponent } from './frontendportal/view/router-component/router-component.component';

// AoT requires an exported function for factories
// export function HttpLoaderFactory(http: HttpClient) {
//    return new TranslateHttpLoader(http, './assets/i18n/', ".json");
// }

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
    ElementsComponent,
    NewLoginComponent,
    FieldErrorDisplayComponent,
    HeaderOneComponent,
    FooterOneComponent,
    FashionOneComponent,
    ToolsComponent,
    SliderComponent,
    LogoComponent,
    InstagramComponent,
    ServicesComponent,
    LeftMenuComponent,
    MenuComponent,
    SettingsComponent,
    BreadcrumbComponent,
    CategoriesComponent,
    ProductBoxOneComponent,
    ProductBoxThreeComponent,
    ProductBoxVerticalSliderComponent,
    CartModalComponent,
    NewsletterComponent,
    QuickViewComponent,
    CartVariationComponent,
    VideoModalComponent,
    SizeModalComponent,
    AgeVerificationComponent,
    SkeletonProductBoxComponent,
    TapToTopComponent,
    DiscountPipe,
    TitleComponent,
    CollectionBannerComponent,
    HomeSliderComponent,
    CategoryComponent,
    ServicesComponent,
    MyOrderComponent,
    // Shop
    ThreeColumnComponent,
    ServicesComponent,
    SocialComponent,
    RelatedProductComponent,
    CollectionNoSidebarComponent,
    GridComponent,
    PaginationComponent,
    BrandsComponent,
    ColorsComponent,
    SizeComponent,
    PriceComponent,
    CartComponent,
    CompareComponent,
    CheckoutComponent,
    SuccessComponent,
    ServicesComponent,
    CollectionWidgetComponent,
    LandingComponent,
    RouterComponentComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,
    CarouselModule,
    NgxSkeletonLoaderModule,
    LazyLoadImageModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    BarRatingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgxPayPalModule,
    Ng5SliderModule,
    InfiniteScrollModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      progressBar: false,
      enableHtml: true,
    }),
    TranslateModule.forRoot(),
    AppRoutingModule,
    AutocompleteLibModule,
    CommonModule,
    GalleryModule.forRoot(),
    NgbModule,
    LazyLoadImageModule.forRoot({
      // preset: scrollPreset // <-- tell LazyLoadImage that you want to use scrollPreset
    }),
  ],
  providers: [AuthGuard],
  exports: [
    FieldErrorDisplayComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule,
    BarRatingModule,
    LazyLoadImageModule,
    NgxSkeletonLoaderModule,
    TranslateModule,
    BreadcrumbComponent,
    CategoriesComponent,
    ProductBoxOneComponent,
    ProductBoxThreeComponent,
    ProductBoxVerticalSliderComponent,
    NewsletterComponent,
    QuickViewComponent,
    CartModalComponent,
    CartVariationComponent,
    VideoModalComponent,
    SizeModalComponent,
    AgeVerificationComponent,
    SkeletonProductBoxComponent,
    TapToTopComponent,
    DiscountPipe,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CarouselModule,
    AutocompleteLibModule,
    BarRatingModule,
    NgxSkeletonLoaderModule,
    TranslateModule,
    CollectionWidgetComponent,
    NgbModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
