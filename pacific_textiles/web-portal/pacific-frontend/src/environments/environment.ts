// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  instagram_token: 'INSTA_TOKEN',
  stripe_token: 'STRIPE_TOKEN',
  paypal_token: 'PAYPAL_TOKEN',
  razorpay_secret_key: 'rzp_live_pEFo2RLK6pFize',
  
  logoUri : 'http://localhost:4200/assets/images/Indra-cart-logo.png',
  successScreen:"http://localhost:4200/frontendlanding/shop/checkout/success/",

  apiUrl: 'http://localhost:7001',
  externalApiUrl: 'http://localhost:8001/external.api',
  masterApiUrl: 'http://localhost:8001/master/controller',
  orderApiUrl: 'http://localhost:8001/billing/controller'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
