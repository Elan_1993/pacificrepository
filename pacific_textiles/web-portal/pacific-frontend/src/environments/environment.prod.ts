export const environment = {
    production: true,
    instagram_token: 'INSTA_TOKEN',
    stripe_token: 'STRIPE_TOKEN',
    paypal_token: 'PAYPAL_TOKEN',
    razorpay_secret_key: 'rzp_live_pEFo2RLK6pFize',

    logoUri : 'https://indracart.com/assets/images/Indra-cart-logo.png',
   
    successScreen:"https://indracart.com/frontendlanding/shop/checkout/success/",

    apiUrl: 'https://indracart.com:7001',
    externalApiUrl: 'https://indracart.com:8001/external.api',
    masterApiUrl: 'https://indracart.com:8001/master/controller',
    orderApiUrl: 'https://indracart.com:8001/billing/controller'
};
