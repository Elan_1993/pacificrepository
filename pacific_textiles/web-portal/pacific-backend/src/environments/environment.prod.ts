export const environment = {
  production: true,

  apiUrl : 'https://indracart.com:7001',
  externalApiUrl : 'https://indracart.com:8001/external.api',
  masterApiUrl : 'https://indracart.com:8001/master/controller',
  reportApiUrl : 'https://indracart.com:8001/external.api/report',
  inventoryApiUrl : 'https://indracart.com:8001/inventory/controller',
  billingApiUrl : 'https://indracart.com:8001/billing/controller'
};
