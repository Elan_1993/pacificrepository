import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {content} from './app-routes';
import {AuthGuard} from './portal/view/login/auth-guard.service';
import {LoginComponent} from './portal/view/login/login.component';
import {LandingComponent} from './portal/view/landing/landing.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'landing',
    component: LandingComponent,
    canActivate: [AuthGuard],
    children: content
  }, {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'legacy'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
