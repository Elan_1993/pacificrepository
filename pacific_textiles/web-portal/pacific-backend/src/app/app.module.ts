import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {CKEditorModule} from 'ngx-ckeditor';
import {GalleryModule} from '@ks89/angular-modal-gallery';
import 'hammerjs';
import 'mousetrap';
import {CountToModule} from 'angular-count-to';
import {ChartsModule} from 'ng2-charts';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ChartistModule} from 'ng-chartist';

import {DropzoneModule} from 'ngx-dropzone-wrapper';
import {DROPZONE_CONFIG} from 'ngx-dropzone-wrapper';
import {DropzoneConfigInterface} from 'ngx-dropzone-wrapper';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProductService} from './portal/service/product.service';
import {ProductComponent} from './portal/view/product/product.component';
import {AddProductComponent} from './portal/view/product/addproduct/addproduct.component';
import {LandingComponent} from './portal/view/landing/landing.component';
import {UserComponent} from './portal/view/user/user.component';
import {AdduserComponent} from './portal/view/user/adduser/adduser.component';
import {StockAdjustmentComponent} from './portal/view/stockadjustment/stock-adjustment.component';
import {OpeningStockComponent} from './portal/view/openingstock/openingstock.component';
import {FieldErrorDisplayComponent} from './portal/view/field-error-display/field-error-display.component';
import {CarouselModule} from 'ngx-owl-carousel-o';
import {LoginComponent} from './portal/view/login/login.component';
import {AuthGuard} from './portal/view/login/auth-guard.service';
import {NavService} from './portal/service/nav.service';
import {WINDOW_PROVIDERS} from './portal/service/windows.service';
import {ToggleFullscreenDirective} from './directives/fullscreen.directive';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {FeatherIconsComponent} from './portal/view/feather-icons/feather-icons.component';
import {FooterComponent} from './footer.component';
import {HeaderComponent} from './header.component';
import {SidebarComponent, AppSubMenuComponent} from './sidebar.component';
import {BreadcrumbComponent} from './breadcrumb.component';
import {RightSidebarComponent} from './right-sidebar.component';
import {DashboardComponent} from './portal/view/dashboard/dashboard.component';
import {StockReportComponent} from './portal/view/stockreport/stockreport.component';
import {ImageUploadComponent} from './portal/view/image-upload/image-upload.component';
import {NoStockSearchCountReportComponent} from './portal/view/nostocksearchcountreport/nostocksearchcountreport.component';
import {OrdersComponent} from './portal/view/orders/orders.component';
import {OrderViewComponent} from './portal/view/orders/order-view/order-view.component';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AuthInterceptor} from './portal/service/auth.interceptor';


const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  maxFilesize: 50,
  url: 'https://httpbin.org/post',
};

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    CarouselModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CKEditorModule,
    Ng2SmartTableModule,
    NgbModule,
    DropzoneModule,
    GalleryModule.forRoot(),
    CountToModule,
    ChartsModule,
    Ng2GoogleChartsModule,
    NgxChartsModule,
    ChartistModule,
    AutocompleteLibModule,
    BsDatepickerModule.forRoot(),
    NgxDatatableModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      closeButton: true
    }),
  ],
  declarations: [
    AppComponent,
    AddProductComponent,
    ProductComponent,
    LandingComponent,
    UserComponent,
    AdduserComponent,
    StockAdjustmentComponent,
    OpeningStockComponent,
    FieldErrorDisplayComponent,
    LoginComponent,
    FeatherIconsComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    AppSubMenuComponent,
    BreadcrumbComponent,
    RightSidebarComponent,
    ToggleFullscreenDirective,
    DashboardComponent,
    StockReportComponent,
    ImageUploadComponent,
    NoStockSearchCountReportComponent,
    OrdersComponent,
    OrderViewComponent
  ],
  providers: [{
    provide: DROPZONE_CONFIG,
    useValue: DEFAULT_DROPZONE_CONFIG
  },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    NgbActiveModal,
    ProductService,
    AuthGuard,
    NavService,
    WINDOW_PROVIDERS],
  exports: [
    FieldErrorDisplayComponent, FeatherIconsComponent, ToggleFullscreenDirective
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
