import {Routes} from '@angular/router';
import {ProductComponent} from './portal/view/product/product.component';
import {AddProductComponent} from './portal/view/product/addproduct/addproduct.component';
import {UserComponent} from './portal/view/user/user.component';
import {AdduserComponent} from './portal/view/user/adduser/adduser.component';
import {StockAdjustmentComponent} from './portal/view/stockadjustment/stock-adjustment.component';
import {OpeningStockComponent} from './portal/view/openingstock/openingstock.component';
import {DashboardComponent} from './portal/view/dashboard/dashboard.component';
import {StockReportComponent} from './portal/view/stockreport/stockreport.component';
import {ImageUploadComponent} from './portal/view/image-upload/image-upload.component';
import {NoStockSearchCountReportComponent} from './portal/view/nostocksearchcountreport/nostocksearchcountreport.component';
import {OrdersComponent} from './portal/view/orders/orders.component';
import {OrderViewComponent} from './portal/view/orders/order-view/order-view.component';

export const content: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      breadcrumb: 'Dashboard'
    }
  },
  {
    path: 'products',
    component: ProductComponent,
    data: {
      breadcrumb: 'Products'
    }
  },
  {
    path: 'add-products',
    component: AddProductComponent,
    data: {
      breadcrumb: 'Add Products'
    }
  },
  {
    path: 'user',
    component: UserComponent,
    data: {
      breadcrumb: 'User'
    }
  },
  {
    path: 'add-user',
    component: AdduserComponent,
    data: {
      breadcrumb: 'Add User'
    }
  },
  {
    path: 'add-user/:userCode',
    component: AdduserComponent,
    data: {
      breadcrumb: 'Add User'
    }
  },
  {
    path: 'stock-adjustment',
    component: StockAdjustmentComponent,
    data: {
      breadcrumb: 'Stock Adjustment'
    }
  },
  {
    path: 'opening-stock',
    component: OpeningStockComponent,
    data: {
      breadcrumb: 'Opening Stock'
    }
  },
  {
    path: 'stock-report',
    component: StockReportComponent,
    data: {
      breadcrumb: 'Stock Report'
    }
  },
  {
    path: 'image-upload',
    component: ImageUploadComponent,
    data: {
      breadcrumb: 'Image Upload'
    }
  },
  {
    path: 'no-stock-search-count-report',
    component: NoStockSearchCountReportComponent,
    data: {
      breadcrumb: 'Stock Report'
    }
  },
  {
    path: 'orders',
    component: OrdersComponent,
    data: {
      breadcrumb: 'Orders'
  }
},
{
  path: 'order-view',
  component: OrderViewComponent,
  data: {
    breadcrumb: 'Order-view'
}
},
{
    path: 'order-view/:orderNumber',
    component: OrderViewComponent,
    data: {
      breadcrumb: 'Order-view'
    }
  }
];
