import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavService } from './portal/service/nav.service';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from  '../environments/environment';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  public right_sidebar: boolean = false;
  public openNav: boolean = false;
  public isOpenMobile : boolean;

  @Output() rightSidebarEvent = new EventEmitter<boolean>();

  constructor(public navServices: NavService, private http: HttpClient, private router: Router) {
  }

  collapseSidebar() {
    this.navServices.open = !this.navServices.open;
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
  }
  right_side_bar() {
    this.right_sidebar = !this.right_sidebar
    this.rightSidebarEvent.emit(this.right_sidebar)
  }

  openMobileNav() {
    this.openNav = !this.openNav;
  }

  logout() {
    this.http.post(environment.externalApiUrl + '/logout', {}, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
      })
    }).subscribe((res: any) => {
      sessionStorage.clear();
      this.router.navigate(['/']);
    });
  }


  ngOnInit() { this.collapseSidebar();  }

}
