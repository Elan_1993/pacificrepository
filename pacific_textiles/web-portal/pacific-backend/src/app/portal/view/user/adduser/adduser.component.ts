import { Component, Injectable, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from '../../../service/user.service';
import { MasterService } from '../../../service/master.service';
import { Customer } from '../../../model/customer';
import {ActivatedRoute, Router} from '@angular/router';
import {DropDownOptions} from '../../../model/dropdownoptions';
import {CodeTable} from '../../../model/codetable';
import {UserGroup} from '../../../model/user.group';
import {ToastrService} from 'ngx-toastr';



@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss']
})

export class AdduserComponent implements OnInit {

  user: FormGroup;
  loading = false;
  searching = false;
  submitted = false;
  view = false;
  customerSave = false;
  customerDb: Customer;
  customerSearch: string;
  gender: DropDownOptions[] = [];
  groupCode: DropDownOptions[] = [];
  cusType : number;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private masterService: MasterService,
    private toasterService: ToastrService,
    private route: ActivatedRoute
  ) {
    this.user = this.formBuilder.group({
      userCode: [''],
      userGender: new FormControl(''),
      userPhone: new FormControl('', [Validators.required,
      Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]),
      userName: new FormControl('', [Validators.required, Validators.minLength(3) , Validators.maxLength(50)]),
      userMail: new FormControl('', [Validators.required, Validators.email]),
      userAddress: new FormControl(''),
      userCity: new FormControl(''),
      userPincode: new FormControl(''),
      userState: new FormControl(''),
      userCountry: new FormControl(''),
      userPassword: new FormControl('', [Validators.minLength(8) ]),
      groupCode: new FormControl(''),
      isUserActive: new FormControl(true),
      isPaidUser: new FormControl(false),
      cusType:new FormControl('')
    });
    route.params.subscribe(params => {
      this.userService.fetchUserByUserCode(params.userCode).pipe(first()).subscribe(data => {
       this.viewUser(data);
      });
    });
  }

  ngOnInit() {
    JSON.parse( sessionStorage.getItem('codeTable')).forEach((res: CodeTable) => {
      if (res.id.tableName === 'gender'){
        this.gender.push({label: res.description, value: res.id.code});
      }
    });
    this.masterService.getUserGroup().pipe(first()).subscribe((res: UserGroup[]) => {
      res.forEach(group => this.groupCode.push({label: group.groupName, value: group.groupCode}));
    });
  }

  get f(){
    return this.user.controls;
  }

  onSubmit() {
    if (this.customerSave) {
      this.saveCustomer();
      return;
    }
    const {value, valid} = this.user;
      this.submitted = true;
      this.loading = true;
      this.user.value.isCompanyUser = true;
      this.user.value.userCmpCode = sessionStorage.getItem('loginUserCmpCode');
      this.user.value.createdBy = sessionStorage.getItem('loginUserCode');
      this.user.value.modifiedBy = sessionStorage.getItem('loginUserCode');
      this.userService.saveUser(value).pipe(first())
        .subscribe(
          data => {
            this.loading = false;
            this.submitted = false;
            if (!data.status){
              this.toasterService.error(data.message);
            }else{
              this.toasterService.success(data.message);
              this.router.navigate(['./landing/user']);
            }
          }, (err) => {
            this.loading = false;
            this.submitted = false;
            console.error(err);
            this.toasterService.error('Contact Administrator');
          });
  }

  cusTypeChange(e){
    this.cusType = e.currentTarget.options.selectedIndex;
  }

  saveCustomer() {
    if(this.cusType == 0){
      alert("Select customer type");
      return;
    }
    this.customerDb.cusType = this.cusType;
    this.customerDb.cusIsPaid = this.user.get('isPaidUser')?.value;
    this.userService.saveCustomer(this.customerDb).pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          if (!data.status){
            this.toasterService.error(data.message);
          }else{
            this.toasterService.success(data.message);
            this.router.navigate(['./landing/user']);
          }
        },
        error => {
          this.loading = false;
          console.log('=failed====');
        });
  }

  viewCustomer(cusMobileOrMail: string): void {
    if (this.customerSearch === undefined || this.customerSearch === '' || this.customerSearch === '0') {
      alert('Enter valid phone Number or mail');
      return;
    }
    this.searching = true;
    this.customerSave = true;
    this.userService.fetchCustomerByPhoneOrMail(cusMobileOrMail).pipe(first()).subscribe((data: any) => {
      if(data === null){
        this.customerSave = false;
        this.toasterService.error("Enter valid phone number");
        return;
      }
      this.customerDb = data;
      this.searching = false;
      this.user.setValue({
        userCode: data.cusCode, userPhone: data.cusMobile,
        userGender: data.cusGender, userName: data.cusName, userMail: data.cusEmailId,
        userAddress: '', userCity: '', userPincode: '',
        userState: '', userCountry: '', userPassword: '*******',
        groupCode: '', isUserActive: true, isPaidUser:data.cusIsPaid, cusType:data.cusType
      });
    });
  }


  viewUser(data: any): void {
    this.view = true;
    this.customerSave = false;
    this.user.setValue({
        userCode: data.userCode, userPhone: data.userPhone,
        userGender: data.userGender, userName: data.userName, userMail: data.userMail,
        userAddress: data?.userAddress, userCity: data?.userCity, userPincode: data.userPincode,
        userState: data?.userState, userCountry: data?.userCountry, userPassword: '********',
        groupCode: data.groupCode, isUserActive: data.isUserActive, isPaidUser: false
      });
  }
  resetForm(): void {
    this.user.reset();
    this.customerSearch = '';
    this.customerSave = false;
    this.customerDb = null;
  }
}
