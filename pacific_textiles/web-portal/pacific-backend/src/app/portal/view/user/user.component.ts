import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {User} from '../../model/user';
import {LocalDataSource} from 'ng2-smart-table';
import { UserService } from '../../service/user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  source: LocalDataSource = new LocalDataSource();
  constructor(
    private router: Router,
    private userService: UserService) {
  }
  public settings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'view-product',
          title: '<i class="fa fa-pencil" title="view product"></i>'
        }
      ],
      position: 'right',
    },
    columns: {
      userName: {
        title: 'User Name',
      },
      userCode: {
        title: 'User Code'
      }
    },
  };

  addUser(): void{
    this.router.navigate(['./landing/add-user']);
  }

  route(event): void{
    this.router.navigate(['./landing/add-user/' + event.data.userCode + '']);
  }

  ngOnInit(): void {
    this.userService.getAllUser(sessionStorage.getItem('loginUserCmpCode')).subscribe((data: User[]) => {
      this.source.load(data);
    });
  }
}
