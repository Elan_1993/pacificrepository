import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeningStockComponent } from './openingstock.component';

describe('OpeningstockComponent', () => {
  let component: OpeningStockComponent;
  let fixture: ComponentFixture<OpeningStockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpeningStockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeningStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
