import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../service/product.service';
import { Product } from '../../model/product';
import { LocalDataSource } from 'ng2-smart-table';
import { first } from 'rxjs/operators';
import {InventoryService} from '../../service/inventory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-openingstock',
  templateUrl: './openingstock.component.html',
  styleUrls: ['./openingstock.component.scss']
})
export class OpeningStockComponent implements OnInit {
  allProducts = [];
  dataList = [];
  productQty: number;
  prodCode: number;
  selectedItem = {};
  errorQty: boolean = false;
  errorProdCode: boolean = false;
  loading = false;
  source: LocalDataSource = new LocalDataSource();
  constructor(private productService: ProductService,
              private inventoryService: InventoryService,
              private router: Router) { }
  public settings = {
    actions: {
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: false,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      prodCode: {
        title: 'Product Code',
        filter: true,
      },
      prodQty: {
        title: 'Product Quantity',
      },
      prodPrice: {
        title: 'Product Price',
      },
      prodMrp: {
        title: 'Total Price',
      },
    },
  };
  ngOnInit() {
  }
  keyword = 'prodCode';


  selectEvent(item) {
    this.selectedItem = item;
  }

  onChangeSearch(val: any) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    this.productService.getProductsByProdCode(val, false).subscribe((data: Product[]) => {      
      this.allProducts = data;
    });

  }

  onFocused(e) {
    setTimeout(() => { // this will make the execution after the above boolean has changed
      this.errorProdCode = false;
    }, 0);
  }

  addQty() {
    this.errorProdCode = false;
    if (this.productQty === 0 || this.productQty === undefined) {
      this.errorQty = true;
      return false;
    }
    this.dataList.forEach(a => {
        if (this.selectedItem['prodCode'] === a.prodCode) {
          this.errorProdCode = true;
          return false;
        }
      });
    if (!this.errorProdCode && !this.errorQty) {
      let productTable: any;
      productTable = {
        prodName: this.selectedItem['prodName'],
        prodCode: this.selectedItem['prodCode'],
        prodPrice: this.selectedItem['prodPrice'],
        prodCmpCode: this.selectedItem['prodCmpCode'],
        prodQty: this.productQty,
        prodMrp: this.selectedItem['prodPrice'] * this.productQty

      };
      this.dataList.push(productTable);
      this.source.load(this.dataList);
      this.reset();
    }
  }
  reset() {
    this.productQty = 0;
    this.prodCode = 0;
  }
  saveOpeningStock(){
    const productList = [];
    this.loading = true;
    this.source.getAll().then(res => {
      for (const dynItem of res) {
        productList.push({
            id: {osdProdCode: dynItem.prodCode, osdCmpCode: dynItem.prodCmpCode},
            osdQty: dynItem.prodQty,
            osdRate: dynItem.prodPrice,
            osdMrp: dynItem.prodMrp
          }
        );
      }
      this.inventoryService.saveOpeningStock({
        id: {osCmpCode: sessionStorage.getItem('loginUserCmpCode')},
        createdBy: sessionStorage.getItem('loginUserCode'),
        modifiedBy: sessionStorage.getItem('loginUserCode'),
        openingStockDs: productList
      })
        .pipe(first())
        .subscribe(
          data => {
            this.loading = false;
            this.router.navigate(['./landing/stock-report']);
          },
          error => {
            this.loading = false;
            console.log('save failed');
          });
    });

  }
  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.dataList.forEach(a => {
        if (event.data.prodCode === a.prodCode) {
          this.dataList.splice(a, 1);
        }
      });
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event) {
    if (window.confirm('Are you sure you want to save?')) {
      event.confirm.resolve(event.newData);
    } else {
      event.confirm.reject();
    }
  }
}
