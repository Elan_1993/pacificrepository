import { Component, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { first } from 'rxjs/operators';
import { ProductService } from '../../../service/product.service';
import { ProductTable } from '../../../model/tables/ProductTable';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { DropDownOptions } from '../../../model/dropdownoptions';
import { CodeTable } from '../../../model/codetable';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
@Injectable()
export class AddProductComponent implements OnInit {

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private toastr: ToastrService
  ) {
    this.product = this.formBuilder.group({
      productName: new FormControl('', [Validators.required]),
      productCategories: new FormControl('', [Validators.required]),
      productPrice: new FormControl('', [Validators.required]),
      productGender: new FormControl('', [Validators.required]),
      productBrand: new FormControl('', [Validators.required]),
      productMaterial: new FormControl('', [Validators.required]),
      productDescription: new FormControl('', [Validators.required]),
      productWeightType: new FormControl('', [Validators.required]),
      productWeight: new FormControl('', [Validators.required]),
      prodCatalogId: new FormControl('', [Validators.required]),
      productDiscount: new FormControl('0'),
      prodCode: new FormControl(''),
      prodColor: new FormControl('')
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.product.controls;
  }
  product: FormGroup;
  loading = false;
  submitted = false;
  chk: boolean = false;
  source: LocalDataSource = new LocalDataSource();
  private dataList: ProductTable[] = [];
  gender: DropDownOptions[] = [];
  categories: DropDownOptions[] = [];
  weightTypeList: DropDownOptions[] = [];
  brandList: DropDownOptions[] = [];
  materialList: DropDownOptions[] = [];
  categoryList: DropDownOptions[] = [];
  errorMsg: string = "";
  errorColor: boolean = false;
  errorCode: boolean = false;
  errorProdCode: boolean = false;
  existingProdCode :boolean = false;
  prodCode: String = "";
  prodColor: String = "";



  public config1: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  public settings = {
    actions: {
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      prodCode: {
        title: 'Product Code',
        filter: true,
      },
      prodColor: {
        title: 'Product Color'
      },
      updateCheck: {
        title: 'updateCheck',
        hide: true,
        value: false
      }
    },
  };

  ngOnInit() {
    if (history.state.isDataModel) {
      this.viewProduct(history.state.prodCatalogId);
    }
    JSON.parse(sessionStorage.getItem('codeTable')).forEach((res: CodeTable) => {
      if (res.id.tableName === 'gender') {
        this.gender.push({ label: res.description, value: res.id.code });
      }
      if (res.id.tableName === 'product_weight') {
        this.weightTypeList.push({ label: res.description, value: res.id.code });
      }
    });
    this.productService.getAllDropDowns().subscribe(
      (res: any) => {
        this.brandList = res.brandList;
        this.materialList = res.materialList;
      });

    this.productService.getProductCategory().subscribe((data: any) => {
      data.forEach(category => this.categoryList.push({ label: category.catName, value: category.catId }));
    });
  }
  numberValidation(): boolean {
    if (isNaN(this.product.value.productPrice) || this.product.value.productPrice == 0 || this.product.value.productPrice == "") {
      this.errorMsg = "Please give correct value for product price";
      this.showValidationError();
      return false;
    }
    if (isNaN(this.product.value.productWeight) || this.product.value.productWeight == 0 || this.product.value.productWeight == "") {
      this.errorMsg = "Please give correct value for product weight";
      this.showValidationError();
      return false;
    }
    if (isNaN(this.product.value.productDiscount) || this.product.value.productDiscount == "") {
      if (this.product.value.productDiscount != 0) {
        this.errorMsg = "Please give correct value for discount";
        this.showValidationError();
        return false;
      }
    }
    return true;
  }

  saveProducts(): void {
    if (!this.numberValidation()) {
      return;
    }
    const productList = [];
    this.submitted = true;
    this.loading = true;
    this.source.getAll().then(res => {
      res.forEach(dynItem => {
        if (dynItem.updateCheck === "") {
          dynItem.updateCheck = true;
        }
        productList.push({
          id: { prodCode: dynItem.prodCode, prodCmpCode: sessionStorage.getItem('loginUserCmpCode') },
          prodColor: dynItem.prodColor,
          prodSize: dynItem.prodSize,
          prodName: this.product.value.productName,
          prodDescription: this.product.value.productDescription,
          prodCategory: this.product.value.productCategories,
          prodPrice: this.product.value.productPrice,
          prodGender: this.product.value.productGender,
          prodMaterial: this.product.value.productMaterial,        
          prodBrand: this.product.value.productBrand,
          prodWeightType: this.product.value.productWeightType,
          prodWeight: this.product.value.productWeight,
          updateCheck: dynItem.updateCheck,
          prodCatalogId: this.product.value.prodCatalogId,
          prodDiscount: this.product.value.productDiscount,
          prodIsActive: true,
          createdBy: sessionStorage.getItem('loginUserCode'),
          modifiedBy: sessionStorage.getItem('loginUserCode')
        });
      });
      if (productList.length == 0) {
        this.toastr.error("Please add atleast one product");
      };      
      this.productService.saveProducts(productList)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.loading = false;
            if (!data.status) {              
              this.toastr.error(data.message + "-" + data.id);
            } else {
              this.toastr.success(data.message);
              this.router.navigate(['./landing/products']);
            }
          },
          error => {
            this.loading = false;
            this.toastr.error("Please provide all mandatory fields");
          });
    });
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you want to delete?')) {
      if (event.data.updateCheck === false) {
        this.productService.deleteProduct(event.data.prodCode, sessionStorage.getItem('loginUserCmpCode')).pipe(first()).subscribe((data: any) => {
          event.confirm.resolve();         
        });
      } else {       
        event.confirm.resolve();        
      } 
      this.dataList = this.dataList.filter(({prodCode}) =>prodCode!=event.data.prodCode);
      } else {      
      event.confirm.reject();      
      }
  }

  onSaveConfirm(event) {
    if (event.newData.prodCode === '' && event.newData.prodColor === '' && event.newData.prodSize === '') {
      alert('Fields should not be empty');
      return;
    }
    {
      event.confirm.resolve(event.newData);
    }
  }

  showSuccess() {
    this.toastr.success('Data successfully saved', 'Success', {
      timeOut: 3000,
      closeButton: true
    });
  }

  showValidationError() {
    this.toastr.error(this.errorMsg, 'Error', {
      timeOut: 3000,
      closeButton: true
    });
  }

  viewProduct(prodCatalogId: number): void {
    this.productService.fetchDataByCatalogId(prodCatalogId).pipe(first()).subscribe((data: any) => {
      this.product.setValue({
        productName: data[0].prodName, productCategories: data[0].prodCategory,
        productPrice: data[0].prodPrice, productGender: data[0].prodGender,
        productMaterial: data[0].prodMaterial, productBrand: data[0].prodBrand,
        productWeightType: data[0].prodWeightType, productWeight: data[0].prodWeight,
        productDiscount: data[0].prodDiscount,productDescription: data[0].prodDescription,
        prodCatalogId: data[0].prodCatalogId,
        prodCode: '',
        prodColor: ''
      });
      data.forEach(a => {
        let productTable: any;
        productTable = {
          prodColor: a.prodColor,
          prodCode: a.id.prodCode,
          updateCheck: false,
        };
        this.dataList.push(productTable);
      });
      this.source.load(this.dataList);
    });
  }

  prodColorChange(e) {
    if (e.target.value === null || e.target.value === undefined
      || e.target.value == '0' || e.target.value === "") {
      this.errorColor = true;
      return false;
    } else {
      this.errorColor = false;
    }
  }

  prodCodeChange(e) {
    if (e.target.value === null || e.target.value === undefined
      || e.target.value == '0' || e.target.value === "") {
      this.errorCode = true;
      this.errorProdCode = false;
      this.existingProdCode =false;
      return false;
    } else {
      this.productService.getProductByProdCode(this.product.get('prodCode')?.value).subscribe((data: any) => {      
        if(data!=null){
          this.errorCode = false;
          this.errorProdCode = false;
          this.existingProdCode = true;        
          return false;
        }else{
          this.errorCode = false;
          this.errorProdCode = false;
          this.existingProdCode = false;     
        }       
      });     
    }
  }

  addQty() {    
    this.errorProdCode = false;
    if (this.product.get('prodCode')?.value === undefined || this.product.get('prodCode')?.value == '0'
      || this.product.get('prodCode')?.value === "") {
      this.errorCode = true;
      return false;
    }
    this.errorColor = false;
    if (this.product.get('prodColor')?.value === undefined || this.product.get('prodColor')?.value == '0'
      || this.product.get('prodColor')?.value === "") {
      this.errorColor = true;
      return false;
    }
    this.dataList.forEach(a => {
      if (this.product.get('prodCode')?.value === a.prodCode) {
        this.errorProdCode = true;
        return false;
      }
    });    
    
    if (!this.errorProdCode && !this.errorColor && !this.errorCode && !this.existingProdCode) {
      let productTable: any;
      productTable = {
        prodCode: this.product.get('prodCode')?.value,
        prodColor: this.product.get('prodColor')?.value,
        updateCheck: true
      };    
      this.dataList.push(productTable);
      this.source.load(this.dataList);
      this.reset();
    }
  }

  reset() {    
    this.existingProdCode = false;
    this.errorProdCode = false;
    this.errorColor = false;
    this.errorCode = false;
  }

  onFocused(e) {
    setTimeout(() => { // this will make the execution after the above boolean has changed
      this.errorProdCode = false;
    }, 0);
  }
}
