import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Product} from '../../model/product';
import {ProductService} from '../../service/product.service';
import {LocalDataSource} from 'ng2-smart-table';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  source: LocalDataSource = new LocalDataSource();
  constructor(
    private router: Router,
    private productService: ProductService) {
  }

  public settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'view-product',
          title: '<i class="fa fa-pencil" title="view product"></i>'
        }
      ],
      position: 'right',
    },
    columns: {
      prodName: {
        title: 'Product Name',
      },
      prodCatalogId: {
        title: 'Catalog Id'
      }
    },
  };
  onCustomAction(event) {
    this.router.navigate(['./landing/add-products'], { state: { isDataModel: true, prodCatalogId: event.data.prodCatalogId}});    
  }
  addProduct(): void{
    this.router.navigate(['./landing/add-products']);
  }
  route(event): void{    
    this.router.navigate(['./landing/add-products'], { state: { isDataModel: true, prodCatalogId: event.data.prodCatalogId}});
  }


  ngOnInit() {
    this.productService.getAllCatalogData().subscribe((data: Product[]) => {
      this.source.load(data);
    });
  }

}
