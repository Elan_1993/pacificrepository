import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NostocksearchcountreportComponent } from './nostocksearchcountreport.component';

describe('NostocksearchcountreportComponent', () => {
  let component: NostocksearchcountreportComponent;
  let fixture: ComponentFixture<NostocksearchcountreportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NostocksearchcountreportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NostocksearchcountreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
