import { Component, OnInit } from '@angular/core';
import {ReportService} from '../../service/report.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-nostocksearchcountreport',
  templateUrl: './nostocksearchcountreport.component.html',
  styleUrls: ['./nostocksearchcountreport.component.scss']
})
export class NoStockSearchCountReportComponent implements OnInit {

  constructor(private reportService: ReportService ) { }

  public noStockReportData: any[];

  public settings = {
    actions: false,
    columns: {
      prodCode: {
        title: 'Product Code',
      },
      productCodeCount: {
        title: 'Count'
      }
    },
  };


  ngOnInit(): void {
    this.fetchNoStockSearchCountReportData();
  }

  fetchNoStockSearchCountReportData(){
    this.reportService.getNoStockSearchCountReportData().pipe(first()).subscribe((data: any) => {
      this.noStockReportData = data;
    });
  }

}
