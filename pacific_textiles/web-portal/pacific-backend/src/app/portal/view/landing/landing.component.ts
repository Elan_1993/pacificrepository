import { Component, OnInit } from '@angular/core';
import { NavService } from '../../service/nav.service';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounce, zoomOut, zoomIn, fadeIn, bounceIn } from 'ng-animate';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {CodeTable} from '../../../portal/model/codetable';
import {DataService} from '../../../portal/service/data.service';
import { environment} from  '../../../../environments/environment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  animations: [
    trigger('animateRoute', [transition('* => *', useAnimation(fadeIn, {
      // Set the duration to 5seconds and delay to 2 seconds
      //params: { timing: 3}
    }))])
  ]
})
export class LandingComponent implements OnInit {

  httpOptions = {};

  public right_side_bar: boolean;
  public layoutType: string = 'RTL';
  public layoutClass: boolean = false;

  constructor(public navServices: NavService, private http: HttpClient, private dataService: DataService) { }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  public rightSidebar($event) {
    this.right_side_bar = $event
  }

  public clickRtl(val) {
    if (val === 'RTL') {
      document.body.className = 'rtl';
      this.layoutClass = true;
      this.layoutType = 'LTR';
    } else {
      document.body.className = '';
      this.layoutClass = false;
      this.layoutType = 'RTL';
    }
  }

  ngOnInit() {
    this.fetchCodeTableData();
  }

  fetchCodeTableData() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
      })
    };
    this.http.get(`${environment.externalApiUrl}/fetch-code-table`, httpOptions).subscribe(
      (res: CodeTable[]) => {
        this.dataService.setCodeTable(res);
      }, (err: HttpErrorResponse) => {         
      }
    );
  }
}
