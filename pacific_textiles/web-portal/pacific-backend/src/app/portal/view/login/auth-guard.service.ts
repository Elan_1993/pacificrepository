import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {TokenService} from '../../service/token.service';


@Injectable()
export class AuthGuard {

  constructor(private router: Router, private tokenService: TokenService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkLoggedIn(state.url);
  }

  checkLoggedIn(url: string): boolean {
    const status = sessionStorage.getItem('loginstatus');

    if (status !== 'success' && url.includes('/landing')) {
      this.router.navigate(['/']);
      return false;
    }

    if (this.tokenService.getRefreshToken()) {
      return true;
    }


    return true;
  }
}
