import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { environment} from  '../../../../environments/environment';
import { User } from '../../model/user';
import { DataService } from '../../service/data.service';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
@Injectable()
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMsg: string;
  loading = false;
  clicked: boolean = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient,
              private router: Router,
              private route: ActivatedRoute,
              private dataService: DataService,
              private authService: AuthService) {
  }

  owlcarousel = [
    {
      title: 'Welcome to IndraCart',
      desc: '',
    }
  ];
  owlcarouselOptions = {
    loop: true,
    items: 1,
    dots: true
  };


  loginMessage() {
    return this.errorMsg;
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      loginCode: [''],
      loginPassWord: [''],
    });
  }

  onSubmit() {
    this.errorMsg = '';
    this.clicked = true;
    const oauthHttpOptions = {};
    // this.http.post(`${environment.apiUrl}` + '/oauth/token?grant_type=password&scope=user_info&username='
    //   + this.loginForm.get('loginCode').value + '&password=' + this.loginForm.get('loginPassWord').value, oauthHttpOptions)
    this.authService.login(this.loginForm.value)
      .subscribe((resToken: any) => {
        console.log(resToken);
        this.loading = true;
        // sessionStorage.setItem('access_token', resToken.access_token);
        const httpOptions = {
          // headers: new HttpHeaders({
          //   'Content-Type': 'application/json',
          //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
          //
          // })
        };

        this.http.post(`${environment.externalApiUrl}/get-user-info`, JSON.stringify({
          userCode: this.loginForm.get('loginCode').value,
          password: this.loginForm.get('loginPassWord').value,
          companyUserMenu: true
        }), httpOptions).subscribe(
          (res: User) => {
            if (res.loginStatus) {
              this.loading = false;
              this.dataService.setUser(res);
              this.errorMsg = res.message;
              this.router.navigate(['./landing/dashboard']);
              this.dataService.setRoleAccess(res.userRoleAccess);
            } else {
              this.loading = false;
              this.errorMsg = res.message;
              this.clicked = false;
            }
          }, (err: HttpErrorResponse) => {
            this.loading = false;
            console.error(err.message);
            this.errorMsg = 'Login Failed';
            this.clicked = false;
          }
        );
      }, (err: HttpErrorResponse) => {
        this.loading = false;
        console.error(err.message);
        this.errorMsg = 'Login Failed';
        this.clicked = false;
      });

  }
}
