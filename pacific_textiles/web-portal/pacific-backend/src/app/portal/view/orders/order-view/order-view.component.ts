import { Component, OnInit, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, ɵNgNoValidate } from '@angular/forms';
import { SalesService } from '../../../service/sales.service';
import { OrderD } from '../../../model/orderD';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { DatatableComponent } from "@swimlane/ngx-datatable";

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss']
})

export class OrderViewComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  order: FormGroup;
  private dataList = [];
  private saveobj: any;
  loading = false;
  submitted = false;
  errorMsg = "";
  public orders = [];
  public temp = [];
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private salesService: SalesService,
    private toastr: ToastrService) {
    this.order = this.formBuilder.group({
      orderNo: new FormControl('', [Validators.required]),
      orderAddress: new FormControl('', [Validators.required]),
      trackingId: new FormControl('', [Validators.required]),
      totalQty: new FormControl('', [Validators.required]),
      eCourier:new FormControl('', [Validators.required]),
      aCourier:new FormControl('',[Validators.required]),
      customerMobile:new FormControl('',[Validators.required]),
      customerName:new FormControl('',[Validators.required]),
      shippingAmount:new FormControl('',[Validators.required])
    });
  }

  ngOnInit(): void {
    if (history.state.isDataModel) {
      this.viewOrder(history.state.orderNo);
    }
  }

  saveOrder(): void {
    this.loading = true;
    if (this.order.get('trackingId')?.value === null || this.order.get('trackingId')?.value === ""
      || this.order.get('trackingId')?.value == 0) {
      this.toastr.error('Please enter valid tracking id');
      this.loading = false;
      return;
    }
    this.saveobj.orderTrackingId = this.order.get('trackingId')?.value;
    this.saveobj.orderActualCourier = this.order.get('aCourier')?.value;    
    this.salesService.saveOrder(this.saveobj).pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.submitted = false;
          this.toastr.success('Order Dispatched SuccessFully');
          this.router.navigate(['./landing/orders']);
        },
        error => {
          this.loading = false;
          this.submitted = false;
          this.errorMsg = ""
          this.toastr.error("Order Not Saved");
        });
  }

  viewOrder(orderNo: string): void {
    this.saveobj = {};
    this.salesService.fetchOrderByOrderNUmber(orderNo).pipe(first()).subscribe((data: any) => {
      this.saveobj = data;  
      console.log(data);          
      this.order.setValue({
        orderNo: data.orderNo, orderAddress: data.orderAddress+",*****"+data.orderStateName+"*****", 
        trackingId: data.orderTrackingId, totalQty: data.orderQty, eCourier:data.orderExpectedCourier , 
        aCourier:data.orderActualCourier , shippingAmount:data.orderShipAmount,customerName:data.customerName,
        customerMobile:data.customerMobile
      });
      data.orderDs.forEach(a => {
        let orderD: any;
        orderD = {
          Product: a.imgSrc,
          OrderProdCode: a.id.orderProdCode,
          OrderQty: a.orderQty
        };
        this.dataList.push(orderD);
      });
      this.orders = this.dataList;
      this.source.load(this.dataList);
    });
  }
}
