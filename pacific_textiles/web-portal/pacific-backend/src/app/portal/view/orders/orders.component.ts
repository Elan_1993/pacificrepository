import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SalesService } from '../../service/sales.service';
import { LocalDataSource } from 'ng2-smart-table';
import { StockReport } from '../../model/stock-report';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  status = '2';
  source: LocalDataSource = new LocalDataSource();
  currentDate = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  bsRangeValue: Date[];

  constructor(private router: Router, private salesService: SalesService) {
    this.bsConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue', rangeInputFormat: 'YYYY-MM-DD',
      dateInputFormat: 'YYYY-MM-DD', showWeekNumbers: false
    });
    this.currentDate.setDate(this.currentDate.getDate() - 7);

    this.bsRangeValue = [this.currentDate, new Date()];
  }


  public settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    columns: {
      orderNo: {
        title: 'Order No',
      },
      orderQty: {
        title: 'Qty'
      }
    },
  };

  ngOnInit(): void {
  }

  onCustomAction(event) {
    this.router.navigate(['./landing/order-view'], { state: { isDataModel: true, orderNo: event.data.orderNo } });
  }

  fetchOrder() {
    this.salesService.fetchOrderByState({
      status: this.status,
      fromDate: this.bsRangeValue[0],
      toDate: this.bsRangeValue[1]
    }).
      subscribe((data: StockReport[]) => {
        this.source.load(data);
      });
  }
}
