
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UploadFilesService } from '../../service/upload-files.service';
import { ProductService } from '../../service/product.service';
import { Product } from '../../model/product';
import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { DatatableComponent } from "@swimlane/ngx-datatable";

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss']
})
export class ImageUploadComponent implements OnInit {
  constructor(private uploadService: UploadFilesService,
    private productService: ProductService) {
  }

  selectedFiles: FileList;
  progressInfos = [];
  message = '';
  fileInfos: Observable<any>;
  // Variable For Auto Complete
  productList = [];
  selectedItem = {};
  keyword = 'prodName';
  errorProdCode = false;
  successCount=0;
  public orders = [];
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  ngOnInit() {

  }
  selectEvent(item) {    
    this.selectedItem = item;
    this.progressInfos = [];    
    this.fileInfos = this.uploadService.getFiles(this.selectedItem['prodCmpCode'], this.selectedItem['prodCatalogId']);
  }

  onChangeSearch(val: any) {
    this.progressInfos = [];
    this.message='';
    this.fileInfos=null;
    this.productService.getProductsByProdName(val.toLowerCase()).pipe(first()).subscribe((data: Product[]) => {
      this.productList = data;
    });
  }

  onFocused(e) {
    setTimeout(() => { // this will make the execution after the above boolean has changed
      this.errorProdCode = false;
    }, 0);
  }

  selectFiles(event) {    
    this.progressInfos = [];
    this.message = '';
    const files = event.target.files;
    let isImage = true;

    for (let i = 0; i < files.length; i++) {
      if (files.item(i).type.match('image.*')) {
        continue;
      } else {
        isImage = false;
        alert('invalid format!');
        break;
      }
    }

    if (isImage) {
      this.selectedFiles = event.target.files;
    } else {
      this.selectedFiles = undefined;
      event.srcElement.percentage = null;
    }
  }

  upload(idx, file) {   
    this.progressInfos[idx] = { value: 0, fileName: file.name };
    this.uploadService.upload(file, this.selectedItem['prodCmpCode'], this.selectedItem['prodCatalogId']).subscribe(
      event => {        
        if (event.type === HttpEventType.UploadProgress) {
          this.progressInfos[idx].percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {          
          this.fileInfos = this.uploadService.getFiles(this.selectedItem['prodCmpCode'], this.selectedItem['prodCatalogId']);
          alert("Image uploaded succssfully")
        }
      },
      err => {
        this.progressInfos[idx].percentage = 0;
        this.message = 'Could not upload the file:' + file.name;
      });  
  }

  uploadFiles() { 
     if(this.selectedFiles.length==undefined || this.selectedFiles.length == 0){
      alert('Please choose images');
      return;
    }
    this.message = '';
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }
}




