import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../service/product.service';
import { Product } from '../../model/product';
import { LocalDataSource } from 'ng2-smart-table';
import { first } from 'rxjs/operators';
import { InventoryService } from '../../service/inventory.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-stockadjustment',
  templateUrl: './stock-adjustment.component.html',
  styleUrls: ['./stock-adjustment.component.scss']
})
export class StockAdjustmentComponent implements OnInit {

  allProducts = [];
  dataList = [];
  stkQty: number;
  qty: number;
  prodCode: number;
  selectedItem = {};
  errorQty: boolean = false;
  errorProdCode: boolean = false;
  stockAdjustmentLoading = false;
  source: LocalDataSource = new LocalDataSource();
  constructor(private productService: ProductService,
              private inventoryService: InventoryService,
              private router: Router,
              private toaster: ToastrService) { }
  public settings = {
    actions: {
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: false,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      prodCode: {
        title: 'Product Code',
        filter: true,

      },
      prodQty: {
        title: 'Quantity',
      },
      prodPrice: {
        title: 'Price',
      },
      prodMrp: {
        title: 'Total Price',
      },
    },
  };
  ngOnInit() {
  }
  keyword = 'prodCode';


  selectEvent(item) {
    this.selectedItem = item;
    this.stkQty = item.stkQty;
    this.qty = item.stkQty;
  }
  onChangeSearch(val: any) {
    this.productService.getProductsByProdCode(val, true).subscribe((data: Product[]) => {
      this.allProducts = data;
    });
  }

  onFocused(e) {
    setTimeout(() => { // this will make the execution after the above boolean has changed
      this.errorProdCode = false;
    }, 0);
  }

  addToStockAdjustmentQty() {
    this.errorProdCode = false;
    if (this.qty < 0 || this.qty === undefined) {
      this.errorQty = true;
      return;
    }
    this.dataList.forEach(a => {
      if (this.selectedItem['prodCode'] === a.prodCode) {
        this.errorProdCode = true;
        return;
      }
    });
    if (!this.errorProdCode && !this.errorQty) {
      let productTable: any;
      if (!this.addValidation()) {
        return;
      }
      productTable = {
        prodName: this.selectedItem['prodName'],
        prodCode: this.selectedItem['prodCode'],
        prodPrice: this.selectedItem['prodPrice'],
        prodCmpCode: this.selectedItem['prodCmpCode'],
        prodQty: this.qty,
        prodMrp: this.selectedItem['prodPrice'] * this.qty

      };
      this.dataList.push(productTable);
      this.source.load(this.dataList);
      this.reset();
    }
  }
  reset() {
    this.qty = 0;
    this.prodCode = 0;
    this.selectedItem = "";
    this.stkQty = 0;
  }


  addValidation(): boolean {
    if (this.selectedItem['prodName'] === "" || this.selectedItem['prodName'] === undefined ||
      this.selectedItem['prodCmpCode'] === "" || this.selectedItem['prodCmpCode'] === undefined) {
      alert("Please select product");
      return false;
    }
    return true;
  }

  onChangeQtyBox() {
    this.errorQty = false;
  }

  saveStockAdjustment() {
    this.stockAdjustmentLoading = true;
    this.source.getAll().then(res => {
      const stockAdjustmentList = [];
      for (const dynItemData of res) {
        stockAdjustmentList.push({
          id: { sadProdCode: dynItemData.prodCode, sadCmpCode: dynItemData.prodCmpCode },
          sadQty: dynItemData.prodQty,
          sadRate: dynItemData.prodPrice,
          sadMrp: dynItemData.prodMrp
        }
        );
      }
      this.inventoryService.saveStockAdjustment({
        id: { sahCmpCode: sessionStorage.getItem('loginUserCmpCode') },
        createdBy: sessionStorage.getItem('loginUserCode'),
        modifiedBy: sessionStorage.getItem('loginUserCode'),
        stockAdjustmentDs: stockAdjustmentList
      })
        .pipe(first())
        .subscribe(
          data => {
            this.showSuccess();
            this.router.navigate(['./landing/stock-report']);
            this.stockAdjustmentLoading = false;
          },
          error => {
            this.stockAdjustmentLoading = false;
            this.showError();
          });
    });

  }
  showSuccess() {
    this.toaster.success('Data successfully saved', 'Success', {
      timeOut: 10000,
      closeButton: true
    });
  }
  showError() {
    this.toaster.error('Something went wrong', 'Error', {
      timeOut: 10000,
      extendedTimeOut: 5000,
      closeButton: true
    });
  }
  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.dataList.forEach(a => {
        if (event.data.prodCode === a.prodCode) {
          this.dataList.splice(a, 1);
        }
      });
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event) {
    if (window.confirm('Are you sure you want to save?')) {
      event.confirm.resolve(event.newData);
    } else {
      event.confirm.reject();
    }
  }

}
