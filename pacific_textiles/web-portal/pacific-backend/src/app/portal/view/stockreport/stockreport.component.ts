import { Component, OnInit } from '@angular/core';
import {ReportService} from '../../service/report.service';
import {StockReport} from '../../model/stock-report';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-stockreport',
  templateUrl: './stockreport.component.html',
  styleUrls: ['./stockreport.component.scss']
})
export class StockReportComponent implements OnInit {

  constructor(private reportService: ReportService ) { }

  public stockReportData: StockReport[];

  public settings = {
    actions: false,
    columns: {
      stkProdCode: {
        title: 'Product Code',
      },
      prodName: {
        title: 'Product Name'
      },
      stkRate: {
        title: 'Rate'
      },
      stkMrp: {
        title: 'Mrp'
      },
      stkQty: {
        title: 'Quantity'
      }
    },
  };


  ngOnInit(): void {
    this.fetchStockReportData();
  }

  fetchStockReportData(){
    this.reportService.getStockReportData().pipe(first()).subscribe((data: StockReport[]) => {
      this.stockReportData = data;
    });
  }

}
