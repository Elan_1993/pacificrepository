import {Injectable} from '@angular/core';

@Injectable()
export class CodeTable {
  description: string;
  id: CodeTableId;

}
export class CodeTableId{
  tableName: string;
  code: string;
}
