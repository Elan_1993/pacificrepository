import {Injectable} from '@angular/core';

@Injectable()
export class Screen {
  moduleNo: number;
  screenNo: number;
  moduleName: string;
  screenName: string;
  moduleIcon: string;
  screenIcon: string;
  routerLink: string;
  hasSubMenu: boolean;
  items: Screen[];
  label: string;
}
