export interface Product {
  prodName: string;
  productSKU: number;
  prodCode: number;
  prodQty: number;
  prodGender: string;
  prodBrand: string;
  prodMaterial: string;
  prodDescription: string;
  prodWeightType: string;
  prodWeight: number;
  prodCatalogId: number;
  prodCategory: string;
  prodPrice: number;
  prodColor: string;
  prodSize: string;
  stkQty: number;
  updateCheck: boolean;
  id: Iid;
}
export interface Iid {
  prodCode: string;
  prodCmpCode: string;
}
