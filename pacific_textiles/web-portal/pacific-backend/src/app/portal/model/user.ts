import {Injectable} from '@angular/core';
import {Screen} from './screen';
import {RoleAccess} from './roleaccess';

@Injectable()
export class User {
  userCode: string;
  userName: string; 
  userGender: string;
  userDob: string;
  loginStatus: boolean;
  message: string;
  userMail: string;
  userPhone: string;
  userAddress: string;
  userCity: string;
  userPincode: string;
  userState: string;
  userCountry: string;
  screenList: Screen[];
  groupCode?: string;
  companyUser: boolean;
  userCmpCode: string;
  userRoleAccess: RoleAccess[];
  loginCode: string;
}
