import {Injectable} from '@angular/core';
import {UserGroupPrivilege} from './user.group.privilege';

@Injectable()
export class UserGroup {
  groupName: string;
  groupCode: string;
  createdBy: string;
  createdTime: Date;
  lastModBy: string;
  lastModTime: Date;
  privileges: UserGroupPrivilege[];
}
