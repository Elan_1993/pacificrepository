import {Injectable} from '@angular/core';

@Injectable()
export class StockReport {

  stkProdCode: string;
  prodName: string;
  stkRate: number;
  stkMrp: number;
  stkQty: number;
}
