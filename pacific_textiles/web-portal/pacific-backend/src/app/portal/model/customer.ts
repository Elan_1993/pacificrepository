import {Injectable} from '@angular/core';
import {CustomerAddress} from './customerAddress';

@Injectable()
export class Customer {    
	cusCode: string;
	cusDob :Date;
	cusGender: string;
	cusName :string ;
	cusCountry: string ;
	cusMobile :string ;
	cusEmailId: string ;
	cusIsPaid  :boolean;
	cusType:number;
	cusPaidRefNo :number ;
	dealershipExpiryDate :Date;
	customerAddress: CustomerAddress[];
}

