import {Injectable} from '@angular/core';

@Injectable()
export class UserGroupPrivilege {
  groupCode: string;
  moduleNo: number;
  screenNo: number;
  moduleName: string;
  screenName: string;
  createAccess: boolean;
  editAccess: boolean;
  viewAccess: boolean;
  deleteAccess: boolean;
  lastModBy: string;
  lastModTime: Date;
}
