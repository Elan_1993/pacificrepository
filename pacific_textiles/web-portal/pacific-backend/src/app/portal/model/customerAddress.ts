import { Injectable } from '@angular/core';

@Injectable()
export class CustomerAddress {
    id: iId;
    cusAdress1: String;
    cusAdress2: String;
    cusPincode: String;
    cusCity: String;
    cusState: String;
    cusCountry: String;
    cusIsDefaultAddress: String;
    cusPhoneNumber: String;
    cusAlternatePhoneNo: String;
}

export class iId {
    cusAddressId: string;
    cusCode: string;
}
