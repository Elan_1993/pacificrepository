import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };

  constructor(private http: HttpClient) { }


  saveOpeningStock(openingStockH: any){
    return this.http.post(`${environment.inventoryApiUrl}/save-opening-stock`, JSON.stringify(openingStockH), this.httpOptions);
  }

  saveStockAdjustment(stockAdjustment: any){
    return this.http.post(`${environment.inventoryApiUrl}/save-stock-adjustment`, JSON.stringify(stockAdjustment), this.httpOptions);
  }

}
