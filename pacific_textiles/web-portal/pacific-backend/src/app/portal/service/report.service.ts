import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };

  getStockReportData() { // get Stock report list
    return this.http.get(`${environment.reportApiUrl}/fetch-stock-report`, this.httpOptions);
  }

  getNoStockSearchCountReportData() { // get product search count list
    return this.http.get(`${environment.reportApiUrl}/fetch-product-search-count`, this.httpOptions);
  }

  constructor(private http: HttpClient) {
  }
}
