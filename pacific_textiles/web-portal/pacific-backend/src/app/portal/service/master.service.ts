import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MasterService {

  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };
  constructor(private http: HttpClient) { }

  getUserGroup() {
    return this.http.get(`${environment.externalApiUrl}/load-user-groups`, this.httpOptions);
  }

}
