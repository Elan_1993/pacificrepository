import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  httpOptions = {
  };

  constructor(private http: HttpClient) { }


  fetchDashBoardData() { // get user list
    return this.http.get(`${environment.reportApiUrl}/fetch-dashboard-data`, this.httpOptions);
  }

}
