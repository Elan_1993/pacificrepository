import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})

export class UserService {
  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };
  constructor(private http: HttpClient) { }
  saveUser(user: any): Observable<any>{
    return this.http.post(`${environment.masterApiUrl}/save-user`, JSON.stringify(user), this.httpOptions);
  }

  getAllUser(cmpCode: string) { // get user list
    return this.http.get(`${environment.masterApiUrl}/fetch-user-by-company/` + cmpCode, this.httpOptions);
  }

  saveCustomer(customer: any): Observable<any> {
    return this.http.post(`${environment.masterApiUrl}/save-customer`, JSON.stringify(customer), this.httpOptions);
  }

  fetchCustomerByPhoneOrMail(phoneOrMail: string) {
    return this.http.get(`${environment.masterApiUrl}` + '/get-customer-by-id/' + phoneOrMail, this.httpOptions);
  }

  fetchUserByUserCode(userCode: string) {
    return this.http.get(`${environment.masterApiUrl}` + '/get-user/' + userCode, this.httpOptions);
  }
}
