import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';
import {RoleAccess} from '../model/roleaccess';
import {CodeTable} from '../model/codetable';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  roleAccess: RoleAccess[];

  constructor(private http: HttpClient) {
  }

  setUser(userIn: User) {
    sessionStorage.setItem('user', JSON.stringify(userIn));
    sessionStorage.setItem('loginstatus', 'success');
    sessionStorage.setItem('loginUserCode', userIn.userCode);
    sessionStorage.setItem('loginUserCmpCode', userIn.userCmpCode);
  }


  setRoleAccess(roleAccess: RoleAccess[]) {
    sessionStorage.setItem('roleAccess', JSON.stringify(roleAccess));
  }

  getRoleAccess() {
    this.roleAccess = JSON.parse(sessionStorage.getItem('roleAccess'));
    return this.roleAccess;
  }

  setCodeTable(codeTableList: CodeTable[]) {
    sessionStorage.setItem('codeTable', JSON.stringify(codeTableList));
  }


}
