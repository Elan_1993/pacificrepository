import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../model/product';
import { environment} from  '../../../environments/environment'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {

  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };

  constructor(private http: HttpClient) { }
  saveProducts(product: Product[]) {
    return this.http.post(`${environment.masterApiUrl}/save-product`, JSON.stringify(product), this.httpOptions);
  }

  getAllCatalogData() { // get product list
    return this.http.get(`${environment.masterApiUrl}/fetch-all-catalogs`, this.httpOptions);
  }

  fetchDataByCatalogId(productCatalogId: number) { // get product list
    return this.http.get(`${environment.masterApiUrl}` + '/get-product-by-catalog-id/' + productCatalogId, this.httpOptions);
  }

  deleteProduct(productCode: string, prodCmpCode: string){ // get product list by prodCode
    return this.http.post(`${environment.masterApiUrl}` + '/delete-product/' + productCode + '/' + prodCmpCode,
      JSON.stringify({}), this.httpOptions);
  }

  getAllProducts() { // get all product list
    return this.http.get(`${environment.masterApiUrl}/fetch-all-products`, this.httpOptions);
  }

  getProductsByProdCode(productCode: number, withStock: boolean) { // get product list by prodCode
    return this.http.get(`${environment.masterApiUrl}` + '/product-search/' + productCode + '/' + withStock, this.httpOptions);
  }

  getProductByProdCode(productCode: number) { // get product list by prodCode
    return this.http.get(`${environment.masterApiUrl}` + '/product-search/' + productCode , this.httpOptions);
  }
  getAllDropDowns() {
    return this.http.get(`${environment.externalApiUrl}/load-all-dropdowns`, this.httpOptions);
  }
  getProductCategory() { // get product list by prodCode
    return this.http.get(`${environment.externalApiUrl}/fetch-product-category`, this.httpOptions);
  }

  getProductsByProdName(keyWord: any): Observable<any> { // get product list by prodCode
    return this.http.get(`${environment.masterApiUrl}/product-autocomplete-search-product-name/` + keyWord, this.httpOptions);
  }
}
