import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class SalesService {
  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };

  constructor(private http: HttpClient) {
  }

  saveOrder(order: any) {
    return this.http.post(`${environment.billingApiUrl}/save-order`, JSON.stringify(order), this.httpOptions);
  }

  fetchOrderByState(object: any) {
    return this.http.post(`${environment.billingApiUrl}/fetch-orders-by-state`, JSON.stringify(object), this.httpOptions);
  }

  fetchOrderByOrderNUmber(orderNumber: any) {
    return this.http.get(`${environment.billingApiUrl}/get-order-by-id/` + orderNumber, this.httpOptions);
  }
}

