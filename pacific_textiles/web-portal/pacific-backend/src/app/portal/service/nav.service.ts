import { Injectable, HostListener, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WINDOW } from './windows.service';

// Menu
export interface Menu {
	path?: string;
	title?: string;
	icon?: string;
	type?: string;
	badgeType?: string;
	badgeValue?: string;
	active?: boolean;
	bookmark?: boolean;
	children?: Menu[];
}

@Injectable({
	providedIn: 'root'
})

export class NavService {

	public screenWidth: any;
	public collapseSidebar: boolean = false;
  public open: boolean = false;

	constructor(@Inject(WINDOW) private window) {
		this.onResize();
		if (this.screenWidth < 991) {
			this.collapseSidebar = true;
			this.open = true;
		}
	}

	// Windows width
	@HostListener("window:resize", ['$event'])
	onResize(event?) {
		this.screenWidth = window.innerWidth;
	}

	MENUITEMS: Menu[] = [
		{
			path: '/dashboard', title: 'Dashboard', icon: 'home', type: 'link', badgeType: 'primary', active: false
		},
		{
			title: 'Master', icon: 'box', type: 'sub', active: false, children: [
				{
					path: '/products', title: 'Products', type: 'link'
				},
        {
          path: '/user', title: 'User', type: 'link'
        }
			]
		},
    {
      title: 'Inventory', icon: 'box', type: 'sub', active: false, children: [
        {
          path: '/opening-stock', title: 'Opening Stock', type: 'link'
        },
        {
          path: '/stock-adjustment', title: 'Stock Adjustment', type: 'link'
        }
      ]
    }

	];

	// Array
	items = new BehaviorSubject<Menu[]>(this.MENUITEMS);


}
