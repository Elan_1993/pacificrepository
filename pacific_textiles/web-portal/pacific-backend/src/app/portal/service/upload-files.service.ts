import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadFilesService {

  httpOptions = {
    // headers: new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   Authorization: 'Bearer ' + sessionStorage.getItem('access_token')
    // })
  };

  constructor(private http: HttpClient) { }

  upload(file: File , cmpCode: any , catalogId: any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('PUT', `${environment.externalApiUrl}/upload/` + cmpCode + '/' + catalogId, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    const modified = req.clone({
      setHeaders: {Authorization: 'Bearer ' + sessionStorage.getItem('access_token')}
    });

    // return next.handle(modified);

    return this.http.request(modified);
  }

  getFiles(cmpCode: any , catalogId: any): Observable<any> {
    return this.http.get(`${environment.externalApiUrl}/files/` + cmpCode + '/' + catalogId, this.httpOptions);
  }
}
