import {Component, Input, OnInit, AfterViewInit, ViewChild , ViewEncapsulation} from '@angular/core';

import { Router, NavigationEnd } from '@angular/router';
import {User} from './portal/model/user';
import {MenuItem} from './portal/model/MenuItem';
import {LandingComponent} from './portal/view/landing/landing.component';
import {NavService} from './portal/service/nav.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {

  // public menuItems: Menu[];
  @Input() reset: boolean;
  user: User;
  model: any[];
  public url: any;
  public fileurl: any;


  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    this.model = this.user.screenList;
  }


  constructor(private router: Router) {
    // this.navServices.items.subscribe(menuItems => {
    //   this.menuItems = menuItems
    //   this.router.events.subscribe((event) => {
    //     if (event instanceof NavigationEnd) {
    //       menuItems.filter(items => {
    //         if (items.path === event.url)
    //           this.setNavActive(items)
    //         if (!items.children) return false
    //         items.children.filter(subItems => {
    //           if (subItems.path === event.url)
    //             this.setNavActive(subItems)
    //           if (!subItems.children) return false
    //           subItems.children.filter(subSubItems => {
    //             if (subSubItems.path === event.url)
    //               this.setNavActive(subSubItems)
    //           })
    //         })
    //       })
    //     }
    //   })
    // })
  }

  // Active Nave state
  // setNavActive(item) {
  //   this.menuItems.filter(menuItem => {
  //     if (menuItem != item)
  //       menuItem.active = false
  //     if (menuItem.children && menuItem.children.includes(item))
  //       menuItem.active = true
  //     if (menuItem.children) {
  //       menuItem.children.filter(submenuItems => {
  //         if (submenuItems.children && submenuItems.children.includes(item)) {
  //           menuItem.active = true
  //           submenuItems.active = true
  //         }
  //       })
  //     }
  //   })
  // }
  //
  // // Click Toggle menu
  // toggletNavActive(item) {
  //   if (!item.active) {
  //     this.menuItems.forEach(a => {
  //       if (this.menuItems.includes(item))
  //         a.active = false
  //       if (!a.children) return false
  //       a.children.forEach(b => {
  //         if (a.children.includes(item)) {
  //           b.active = false
  //         }
  //       })
  //     });
  //   }
  //   item.active = !item.active
  // }

  //Fileupload
  readUrl(event: any) {
    if (event.target.files.length === 0)
      return;
    //Image upload validation
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    // Image upload
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.url = reader.result;
    }
  }
}

  @Component({
    /* tslint:disable:component-selector */
    selector: '[app-submenu]',
    /* tslint:enable:component-selector */
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li [ngClass]="{'active': isActive(i)}" [class]="child.badgeStyleClass" *ngIf="child.visible === false ? false : true">
              <a [href]="child.url||'#'" (click)="itemClick($event,child,i)"
                   *ngIf="!child.routerLink" class="sidebar-header"
                   [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target">
                    <span class="menu-icon" [ngClass]="child.icon"></span>
                    <span>{{child.label}}</span>
                    <i class="fa fa-angle-right pull-right" *ngIf="child.items"></i>
                    <span class="badge badge-primary" *ngIf="child.badge">{{child.badge}}</span>
                </a>

                <a (click)="itemClick($event,child,i)"  *ngIf="child.routerLink"
                   [routerLink]="child.routerLink" routerLinkActive="active" class="sidebar-header"
                   [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target">
                    <span class="menu-icon" [ngClass]="child.icon"></span>
                    <span class="text-overflow-ellipsis" title="{{child.label}}">{{child.label}}</span>
                    <i class="fa fa-angle-right pull-right" *ngIf="child.items"></i>
                    <span class="badge badge-primary" *ngIf="child.badge">{{child.badge}}</span>
                </a>
              <ul app-submenu [item]="child" *ngIf="child.items" class="sidebar-submenu" [visible]="isActive(i)" [reset]="reset" [parentActive]="isActive(i)"></ul>
            </li>
        </ng-template>
    `,
  })
  export class AppSubMenuComponent {
    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    _parentActive: boolean;

    _reset: boolean;

    activeIndex: number;

    constructor(private app: LandingComponent, private appMenu: SidebarComponent, private navServices: NavService) {
    }

    // setNavActive(items) {
    //   this.item.filter(menuItem => {
    //     if (menuItem !== items) {
    //       menuItem.active = false
    //     }
    //     if (menuItem.children && menuItem.children.includes(item))
    //       menuItem.active = true
    //     if (menuItem.children) {
    //       menuItem.children.filter(submenuItems => {
    //         if (submenuItems.children && submenuItems.children.includes(item)) {
    //           menuItem.active = true
    //           submenuItems.active = true
    //         }
    //       })
    //     }
    //   })
    // }


    itemClick(event: Event, item: MenuItem, index: number) {

      sessionStorage.setItem('moduleNo', item['moduleNo']);
      sessionStorage.setItem('screenNo', item['screenNo']);



      // avoid processing disabled items
      if (item.disabled) {
        event.preventDefault();
        return true;
      }

      // activate current item and deactivate active sibling if any
      this.activeIndex = (this.activeIndex === index) ? null : index;

      // execute command
      if (item.command) {
        item.command({originalEvent: event, item});
      }

      // prevent hash change
      if (item.items || (!item.url && !item.routerLink)) {
        event.preventDefault();
      }

      // hide menu
      if (!item.items) {
        this.navServices.open = !this.navServices.open;
        this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
        // item.visible = !item.visible;
      }

    }

    isActive(index: number): boolean {
      return this.activeIndex === index;
    }

    @Input() get reset(): boolean {
      return this._reset;
    }

    set reset(val: boolean) {
      this._reset = val;

      if (this._reset) {
        this.activeIndex = null;
      }
    }

    @Input() get parentActive(): boolean {
      return this._parentActive;
    }

    set parentActive(val: boolean) {
      this._parentActive = val;

      if (!this._parentActive) {
        this.activeIndex = null;
      }
    }

    onMouseEnter(index: number) {
      if (this.root) {
        this.activeIndex = index;
      }
    }

  }
