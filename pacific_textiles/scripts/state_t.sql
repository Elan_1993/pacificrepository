/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.23 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `state_t` (
	`state_id` int (11),
	`module_name` varchar (150),
	`is_transaction_success` tinyint (1),
	`current_state` tinyint (4),
	`current_status` varchar (150),
	`next_state` tinyint (11),
	`next_status` varchar (150)
); 
insert into `state_t` (`state_id`, `module_name`, `is_transaction_success`, `current_state`, `current_status`, `next_state`, `next_status`) values('1','ORDER','1','1','Created','2','Accepted');
insert into `state_t` (`state_id`, `module_name`, `is_transaction_success`, `current_state`, `current_status`, `next_state`, `next_status`) values('2','ORDER','0','1','Created','4','Rejected');
insert into `state_t` (`state_id`, `module_name`, `is_transaction_success`, `current_state`, `current_status`, `next_state`, `next_status`) values('3','ORDER','1','2','Accepted','3','Dispatched');
insert into `state_t` (`state_id`, `module_name`, `is_transaction_success`, `current_state`, `current_status`, `next_state`, `next_status`) values('4','ORDER','1','4','Rejected','2','Accepted');
