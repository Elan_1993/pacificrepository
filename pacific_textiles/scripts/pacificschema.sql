-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: pacific
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autonumber_d`
--

DROP TABLE IF EXISTS `autonumber_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autonumber_d` (
  `auto_rid` int NOT NULL AUTO_INCREMENT,
  `auto_prefix` varchar(100) DEFAULT NULL,
  `auto_suffix` varchar(100) DEFAULT NULL,
  `auto_sequence_number` int DEFAULT NULL,
  `auto_fin_year` varchar(100) DEFAULT NULL,
  `auto_cmp_code` varchar(100) DEFAULT NULL,
  `auto_category` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`auto_rid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brand_m`
--

DROP TABLE IF EXISTS `brand_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand_m` (
  `brand_id` int NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cart_details`
--

DROP TABLE IF EXISTS `cart_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart_details` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `cart_cmp_code` varchar(50) NOT NULL,
  `cart_prod_code` varchar(100) NOT NULL,
  `cart_user_code` varchar(50) NOT NULL,
  `cart_prod_qty` decimal(22,6) DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`cart_cmp_code`,`cart_prod_code`,`cart_user_code`),
  KEY `fk_cart_prod_code` (`cart_prod_code`,`cart_cmp_code`),
  KEY `fk_cart_user_code` (`cart_user_code`),
  CONSTRAINT `fk_cart_prod_code` FOREIGN KEY (`cart_prod_code`, `cart_cmp_code`) REFERENCES `products` (`prod_code`, `prod_cmp_code`),
  CONSTRAINT `fk_cart_user_code` FOREIGN KEY (`cart_user_code`) REFERENCES `user_m` (`user_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `cat_id` int NOT NULL,
  `cat_name` varchar(100) DEFAULT NULL,
  `cat_parent_rid` int DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code_table`
--

DROP TABLE IF EXISTS `code_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `code_table` (
  `table_name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`table_name`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company_m`
--

DROP TABLE IF EXISTS `company_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_m` (
  `cmp_code` varchar(20) NOT NULL,
  `cmp_name` varchar(100) DEFAULT NULL,
  `cmp_gst_no` varchar(50) DEFAULT NULL,
  `cmp_address` varchar(100) DEFAULT NULL,
  `cmp_city` varchar(100) DEFAULT NULL,
  `cmp_state` varchar(50) DEFAULT NULL,
  `cmp_country` varchar(50) DEFAULT NULL,
  `createdby` int DEFAULT NULL,
  `modifiedby` int DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_address`
--

DROP TABLE IF EXISTS `customer_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_address` (
  `cus_address_id` int NOT NULL,
  `cus_code` varchar(50) NOT NULL,
  `cus_adress1` varchar(100) DEFAULT NULL,
  `cus_adress2` varchar(100) DEFAULT NULL,
  `cus_pincode` varchar(20) DEFAULT NULL,
  `cus_city` varchar(100) DEFAULT NULL,
  `cus_state` varchar(100) DEFAULT NULL,
  `cus_country` varchar(100) DEFAULT NULL,
  `cus_is_default_address` tinyint DEFAULT NULL,
  `cus_phone_number` varchar(100) DEFAULT NULL,
  `cus_alternate_phone_no` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cus_address_id`,`cus_code`),
  KEY `fk_cus_code` (`cus_code`),
  CONSTRAINT `fk_cus_code` FOREIGN KEY (`cus_code`) REFERENCES `customer_m` (`cus_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_m`
--

DROP TABLE IF EXISTS `customer_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_m` (
  `cus_dob` date DEFAULT NULL,
  `cus_gender` varchar(10) DEFAULT NULL,
  `cus_code` varchar(50) NOT NULL,
  `cus_name` varchar(100) DEFAULT NULL,
  `cus_country` varchar(50) DEFAULT NULL,
  `cus_mobile` varchar(50) DEFAULT NULL,
  `cus_email_id` varchar(100) DEFAULT NULL,
  `cus_is_paid` tinyint DEFAULT NULL,
  `cus_paid_ref_no` varchar(100) DEFAULT NULL,
  `dealership_expiry_date` date DEFAULT NULL,
  PRIMARY KEY (`cus_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `material_m`
--

DROP TABLE IF EXISTS `material_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `material_m` (
  `material_id` int NOT NULL AUTO_INCREMENT,
  `material_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_token` (
  `authentication_id` varchar(255) NOT NULL,
  `token_id` varchar(255) NOT NULL,
  `token` blob NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `authentication` blob NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) NOT NULL,
  `token` blob NOT NULL,
  `authentication` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `opening_stock_d`
--

DROP TABLE IF EXISTS `opening_stock_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opening_stock_d` (
  `osd_cmp_code` varchar(50) NOT NULL,
  `os_code` varchar(50) NOT NULL,
  `osd_prod_code` varchar(50) NOT NULL,
  `osd_rate` decimal(22,6) DEFAULT '0.000000',
  `osd_mrp` decimal(22,6) DEFAULT '0.000000',
  `osd_qty` decimal(22,6) DEFAULT '0.000000',
  PRIMARY KEY (`osd_cmp_code`,`os_code`,`osd_prod_code`),
  KEY `fk_osd_prod_code` (`osd_prod_code`,`osd_cmp_code`),
  KEY `fk_osd_os_rid` (`os_code`),
  KEY `fk_os_prod_code` (`osd_cmp_code`,`osd_prod_code`),
  CONSTRAINT `fk_os_code` FOREIGN KEY (`osd_cmp_code`, `os_code`) REFERENCES `opening_stock_h` (`os_cmp_code`, `os_code`),
  CONSTRAINT `fk_os_prod_code` FOREIGN KEY (`osd_prod_code`, `osd_cmp_code`) REFERENCES `products` (`prod_code`, `prod_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `opening_stock_h`
--

DROP TABLE IF EXISTS `opening_stock_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opening_stock_h` (
  `os_cmp_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `os_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`os_cmp_code`,`os_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_d`
--

DROP TABLE IF EXISTS `order_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_d` (
  `order_no` varchar(50) NOT NULL,
  `order_cmp_code` varchar(50) NOT NULL,
  `order_prod_code` varchar(50) NOT NULL,
  `order_d_cus_code` varchar(50) NOT NULL,
  `order_qty` decimal(22,6) DEFAULT '0.000000',
  `order_rate` decimal(22,6) DEFAULT '0.000000',
  `order_gross_amt` decimal(22,6) DEFAULT '0.000000',
  `order_tax_amt` decimal(22,6) DEFAULT '0.000000',
  `order_disc` decimal(22,6) DEFAULT '0.000000',
  `order_net_amt` decimal(22,6) DEFAULT '0.000000',
  PRIMARY KEY (`order_no`,`order_cmp_code`,`order_prod_code`),
  KEY `fk_order_no` (`order_no`),
  KEY `fk_order_prod_code` (`order_prod_code`,`order_cmp_code`),
  CONSTRAINT `fk_order_no` FOREIGN KEY (`order_no`) REFERENCES `order_h` (`order_no`),
  CONSTRAINT `fk_order_prod_code` FOREIGN KEY (`order_prod_code`, `order_cmp_code`) REFERENCES `products` (`prod_code`, `prod_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_h`
--

DROP TABLE IF EXISTS `order_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_h` (
  `order_no` varchar(50) NOT NULL,
  `order_cus_code` varchar(50) NOT NULL,
  `order_amount` decimal(22,6) DEFAULT '0.000000',
  `order_qty` decimal(22,6) DEFAULT '0.000000',
  `order_state` int DEFAULT NULL,
  `order_status` varchar(50) DEFAULT NULL,
  `order_address_id` int DEFAULT NULL,
  `order_ship_amount` decimal(22,6) DEFAULT '0.000000',
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `prod_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `prod_cmp_code` varchar(100) NOT NULL,
  `prod_name` varchar(200) DEFAULT NULL,
  `prod_price` decimal(22,6) DEFAULT '0.000000',
  `prod_discount` decimal(22,6) DEFAULT '0.000000',
  `prod_gender` varchar(20) DEFAULT NULL,
  `prod_category` varchar(100) DEFAULT NULL,
  `prod_material` varchar(100) DEFAULT NULL,
  `prod_brand` varchar(100) DEFAULT NULL,
  `prod_description` varchar(200) DEFAULT NULL,
  `prod_weight_type` varchar(20) DEFAULT NULL,
  `prod_weight` decimal(11,2) DEFAULT NULL,
  `prod_is_active` tinyint(1) DEFAULT NULL,
  `prod_size` varchar(50) DEFAULT NULL,
  `prod_color` varchar(50) DEFAULT NULL,
  `prod_catalog_id` int DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`prod_code`,`prod_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `screen_m`
--

DROP TABLE IF EXISTS `screen_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `screen_m` (
  `module_no` int NOT NULL,
  `screen_no` int NOT NULL,
  `module_name` varchar(50) DEFAULT NULL,
  `screen_name` varchar(50) DEFAULT NULL,
  `module_icon` varchar(30) NOT NULL,
  `screen_icon` varchar(30) DEFAULT NULL,
  `router_link` varchar(50) DEFAULT NULL,
  `sub_menu` tinyint(1) NOT NULL DEFAULT '0',
  `sub_menu_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`module_no`,`screen_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock` (
  `stk_prod_code` varchar(50) NOT NULL,
  `stk_cmp_code` varchar(50) NOT NULL,
  `stk_date` date DEFAULT NULL,
  `stk_rate` decimal(22,6) DEFAULT '0.000000',
  `stk_mrp` decimal(22,6) DEFAULT '0.000000',
  `stk_qty` double(11,2) DEFAULT NULL,
  `createdby` int DEFAULT NULL,
  `modifiedby` int DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`stk_prod_code`,`stk_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_adjustment_d`
--

DROP TABLE IF EXISTS `stock_adjustment_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_adjustment_d` (
  `sah_code` varchar(50) NOT NULL,
  `sad_cmp_code` varchar(50) NOT NULL,
  `sad_prod_code` varchar(50) NOT NULL,
  `sad_rate` decimal(22,6) DEFAULT '0.000000',
  `sad_mrp` decimal(22,6) DEFAULT '0.000000',
  `sad_qty` decimal(22,6) DEFAULT '0.000000',
  PRIMARY KEY (`sah_code`,`sad_cmp_code`,`sad_prod_code`),
  KEY `fk_sad_prod_code` (`sad_prod_code`,`sad_cmp_code`),
  CONSTRAINT `fk_sad_prod_code` FOREIGN KEY (`sad_prod_code`, `sad_cmp_code`) REFERENCES `products` (`prod_code`, `prod_cmp_code`),
  CONSTRAINT `fk_sah_code` FOREIGN KEY (`sah_code`, `sad_cmp_code`) REFERENCES `stock_adjustment_h` (`sah_code`, `sah_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_adjustment_h`
--

DROP TABLE IF EXISTS `stock_adjustment_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_adjustment_h` (
  `sah_code` varchar(50) NOT NULL,
  `sah_date` date DEFAULT NULL,
  `sah_cmp_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sah_remarks_desc` varchar(200) DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`sah_code`,`sah_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_transaction`
--

DROP TABLE IF EXISTS `stock_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_transaction` (
  `st_code` varchar(50) NOT NULL,
  `st_date` date NOT NULL,
  `st_cmp_code` varchar(20) NOT NULL,
  `st_prod_code` varchar(50) NOT NULL,
  `stTranCode` int DEFAULT NULL,
  `st_tran_code` int DEFAULT NULL,
  `st_receipt_qty` decimal(22,6) DEFAULT '0.000000',
  `st_issue_qty` decimal(22,6) DEFAULT '0.000000',
  `st_mrp` decimal(22,6) DEFAULT '0.000000',
  `st_cost_price` decimal(22,6) DEFAULT '0.000000',
  `st_state` int DEFAULT NULL,
  `st_ref_no` varchar(50) DEFAULT NULL,
  `st_stock_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`st_code`,`st_date`,`st_prod_code`,`st_cmp_code`),
  KEY `fk_prod_code` (`st_prod_code`,`st_cmp_code`),
  CONSTRAINT `fk_prod_code` FOREIGN KEY (`st_prod_code`, `st_cmp_code`) REFERENCES `products` (`prod_code`, `prod_cmp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_access_details`
--

DROP TABLE IF EXISTS `user_access_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_access_details` (
  `id` varchar(50) NOT NULL,
  `userCode` varchar(50) NOT NULL,
  `remoteAddr` varchar(50) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `tokenGenerationTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_group_m`
--

DROP TABLE IF EXISTS `user_group_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_group_m` (
  `group_code` varchar(10) NOT NULL,
  `group_name` varchar(120) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_group_privilege_m`
--

DROP TABLE IF EXISTS `user_group_privilege_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_group_privilege_m` (
  `group_code` varchar(10) NOT NULL,
  `module_no` int NOT NULL,
  `screen_no` int NOT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_view` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_code`,`module_no`,`screen_no`),
  KEY `FK_user_group_privilege_m_screen_m_idx` (`module_no`,`screen_no`),
  CONSTRAINT `FK_user_group_privilege_m_screen_m` FOREIGN KEY (`module_no`, `screen_no`) REFERENCES `screen_m` (`module_no`, `screen_no`),
  CONSTRAINT `FK_user_group_privilege_m_user_group_m` FOREIGN KEY (`group_code`) REFERENCES `user_group_m` (`group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_logs_t`
--

DROP TABLE IF EXISTS `user_logs_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_logs_t` (
  `user_code` varchar(255) NOT NULL,
  `last_login_time` datetime NOT NULL,
  `last_login_ip` varchar(50) NOT NULL,
  `is_logged_in` tinyint(1) NOT NULL,
  `logout_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_m`
--

DROP TABLE IF EXISTS `user_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_m` (
  `user_code` varchar(50) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `user_phone` varchar(50) DEFAULT NULL,
  `user_mail` varchar(50) DEFAULT NULL,
  `user_address` varchar(100) DEFAULT NULL,
  `user_city` varchar(50) DEFAULT NULL,
  `user_pincode` varchar(50) DEFAULT NULL,
  `user_state` varchar(50) DEFAULT NULL,
  `user_country` varchar(50) DEFAULT NULL,
  `user_gender` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_dob` date DEFAULT NULL,
  `is_user_active` tinyint(1) DEFAULT '1',
  `password_expiry_date` date DEFAULT NULL,
  `is_company_user` tinyint(1) DEFAULT '0',
  `user_cmp_code` varchar(50) DEFAULT NULL,
  `is_super_admin` tinyint(1) DEFAULT '0',
  `is_locked` tinyint(1) DEFAULT '0',
  `incorrect_pwd_count` int DEFAULT '0',
  `group_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account_locked_till_date` datetime DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`user_code`),
  KEY `fk_user_group_m` (`group_code`),
  CONSTRAINT `fk_user_group_code` FOREIGN KEY (`group_code`) REFERENCES `user_group_m` (`group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-23 13:18:07
