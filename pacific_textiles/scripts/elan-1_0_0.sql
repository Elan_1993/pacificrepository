-- Dec 30 2020

insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) Values ('10','0','Home','Home','fa fa-fw fa-home','fa fa-fw fa-home','./dashboard','0',NULL);
insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) Values ('20','1','Master','Products','fa fa-fw fa-line-chart','fa fa-fw fa-line-chart','./products','1',NULL);
insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) Values ('20','2','Master','User','fa fa-fw fa-line-chart','fa fa-fw fa-sign-in','./user','1',NULL);
insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) Values ('30','1','Inventory','Opening Stock','fa fa-fw fa-line-chart','fa fa-fw fa-sign-in','./opening-stock','1',NULL);
insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) Values ('30','2','Inventory','Stock Adjustment','fa fa-fw fa-line-chart','fa fa-fw fa-sign-in','./stock-adjustment','1',NULL);


insert into user_group_privilege_m(group_code, module_no, screen_no, is_create, is_edit, is_view, is_delete, modified_by, modified_date) SELECT 'Test',module_no,screen_no,'1','1','1','1','Admin',now() from screen_m;

insert into code_table (table_name, code, description) values ('Timeout','Timeout','9999');


-- 01/01/2021

Delete from code_table where table_name='Timeout';

insert into code_table(table_name, code, description) VALUES ('gender','F','Female'),
                                                             ('gender','M','Male');
insert into code_table(table_name, code, description) VALUES ('color','Brown','Brown'),
                                                             ('color','Multicolor','Multicolor');
insert into code_table(table_name, code, description) VALUES ('size','XL','Xtra-Large'),
                                                             ('size','D','Default');
                                                             
insert into company_m (cmp_code, cmp_name, cmp_gst_no, cmp_address, cmp_city, cmp_state, cmp_country)
value ('pac','Pacifc','0000000','0000','00000','00000','India');

insert into code_table(table_name, code, description)
VALUES ('product_weight','KG','Kilogram'),('product_weight','GM','Gram');

alter table category_level drop column cat_cmp_code;
alter table category drop column cat_cmp_code;
ALTER TABLE `autonumber_d` modify column auto_rid INT NOT NULL AUTO_INCREMENT;

-- 03/01/2021

alter table stock_transaction drop column st_status,drop column st_ref_id;
alter table stock_transaction
    add st_tran_code int DEFAULT 0;

-- 10/01/2021
    
alter table opening_stock_h modify createdby varchar(50) null;
alter table opening_stock_h modify modifiedby varchar(50) null;
alter table stock_adjustment_h modify createdby varchar(50) null;
alter table stock_adjustment_h modify modifiedby varchar(50) null;

-- 20/01/2021

alter table user_m modify createdby varchar(50) null;
alter table user_m modify modifiedby varchar(50) null;

drop table category_level;
alter table category drop column cat_level_code;

insert into category(cat_id, cat_name, cat_parent_rid) values (1,'Women Fashion',null);
insert into category(cat_id, cat_name, cat_parent_rid) values (2,'Clothing',1);
insert into category(cat_id, cat_name, cat_parent_rid) values (3,'Ethnic Wear',2);
insert into category(cat_id, cat_name, cat_parent_rid) values (4,'Sarees',3);

-- 04/02/2021

insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name)
Values ('20','3','Master','Image Upload','fa fa-fw fa-line-chart','fa fa-fw fa-cloud-upload','./image-upload','1',NULL);

insert into user_group_privilege_m(group_code, module_no, screen_no, is_create, is_edit, is_view, is_delete, modified_by, modified_date)
values ('Test','20','3',1,1,1,1,'Admin',now());


-- 11/02/2021

DELETE from opening_stock_d;
DELETE from opening_stock_h;
DELETE from stock_adjustment_d;
DELETE from stock_adjustment_h;
DELETE from stock_transaction;
DELETE from products;
DELETE FROM stock;

INSERT INTO material_m (`material_id` , `material_name`) VALUES ('1' , 'Silk');
INSERT INTO material_m (`material_id` , `material_name`) VALUES ('2' , 'Cotton');
INSERT INTO material_m (`material_id` , `material_name`) VALUES ('3' , 'Silk Cotton');


INSERT INTO `brand_m` (`brand_id` , `brand_name`) VALUES ('1' , 'DefaultBrand');

INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12001','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12002','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12003','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12004','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12005','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12006','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12007','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12008','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12009','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('12010','pac','Semi - Kuppadam soft silk sarees','F','4','1','1','799','10','GM','550','1','19');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13001','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13002','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13003','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13004','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13005','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13006','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13007','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13008','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('13009','pac','Saree with all over empose design','F','4','1','1','850','10','GM','500','1','20');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('14001','pac','Checked kerala saree','F','4','2','1','500','10','GM','550','1','21');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15001','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15002','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15003','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15004','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15005','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15006','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15007','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15008','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15009','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15010','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15011','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15012','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15013','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');
INSERT into products(prod_code, prod_cmp_code, prod_name,prod_gender, prod_category,prod_material, prod_brand, prod_price, prod_discount, prod_weight_type, prod_weight, prod_is_active, prod_catalog_id) Values ('15014','pac','Merserized Kuppadam Rich Silk Cotton','F','4','3','1','750','10','GM','600','1','22');

update products set prod_description=prod_name,createdby='Admin',modifiedby='Admin',created_datetime=now(),modified_datetime=now();
update products set prod_color='Brown',prod_size='M' where prod_color is null;
alter table products modify createdby varchar(50) null,modify modifiedby varchar(50) null;
UPDATE autonumber_d set auto_sequence_number = (Select max(prod_catalog_id) from products)+1 where auto_category='Catalog';


-- 22/02/2021

INSERT into autonumber_d(auto_prefix, auto_suffix, auto_sequence_number, auto_fin_year, auto_cmp_code, auto_category)
values ('Cus',null,1,null,null,'Customer');

insert into user_group_m(group_code, group_name, created_by, created_date, modified_by, modified_date)
values ('Dummy','Front-End-Users','Admin',now(),'Admin',now());

-- 24/03/2021

CREATE TABLE `product_search_count_t` (
  `prod_code` varchar(50) NOT NULL,
  `prod_cmp_code` varchar(50) NOT NULL,
  `product_code_count` int DEFAULT NULL,
  PRIMARY KEY (`prod_code`,`prod_cmp_code`)
) ENGINE=InnoDB;

insert into screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) Values ('100','2','Report','No Stock Search Count','fa fa-fw fa-line-chart','fa fa-fw fa-sign-in','./no-stock-search-count-report','1',NULL);

-- 25/05/2021

CREATE TABLE `courier` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `name` varchar(30) NOT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `courierprice` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `price` decimal(11,2) default 0.00,
                          `courier_id` int NOT NULL DEFAULT '1',
                          `state_id` int NOT NULL DEFAULT '1',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

insert into courier(id,  name) values (1,'IndiaPost'),
                                 (2,'India Post(Speed)'),(3,'Others');


INSERT INTO courierprice(state_id, courier_id,price) VALUES ('1','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('2','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('3','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('4','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('5','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('6','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('7','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('8','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('9','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('10','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('11','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('12','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('13','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('14','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('15','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('16','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('17','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('18','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('19','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('20','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('21','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('22','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('23','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('24','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('25','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('26','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('27','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('28','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('29','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('30','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('31','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('32','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('33','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('34','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('35','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('36','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('37','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('38','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('39','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('40','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('41','1','40');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('1','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('2','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('3','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('4','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('5','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('6','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('7','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('8','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('9','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('10','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('11','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('12','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('13','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('14','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('15','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('16','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('17','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('18','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('19','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('20','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('21','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('22','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('23','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('24','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('25','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('26','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('27','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('28','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('29','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('30','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('31','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('32','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('33','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('34','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('35','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('36','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('37','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('38','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('39','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('40','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('41','2','50');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('1','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('2','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('3','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('4','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('5','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('6','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('7','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('8','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('9','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('10','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('11','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('12','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('13','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('14','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('15','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('16','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('17','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('18','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('19','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('20','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('21','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('22','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('23','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('24','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('25','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('26','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('27','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('28','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('29','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('30','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('31','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('32','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('33','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('34','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('35','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('36','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('37','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('38','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('39','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('40','3','60');
INSERT INTO courierprice(state_id, courier_id,price) VALUES ('41','3','60');

---27-07-2021

alter table stock
    add un_saleable_qty decimal(22,6) default 0.00 null;

alter table stock modify stk_qty decimal(22,6) default 0.00 null;




