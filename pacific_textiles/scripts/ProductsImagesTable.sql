/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.23 : Database - pacific
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pacific` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `pacific`;

/*Table structure for table `product_images` */

DROP TABLE IF EXISTS `product_images`;

CREATE TABLE `product_images` (
  `prod_cmp_code` varchar(50) NOT NULL,
  `prod_code` varchar(50) NOT NULL,
  `prod_catalog_id` int NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `image_type` varchar(11) DEFAULT NULL,
  `original_file_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`prod_cmp_code`,`prod_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `product_images` */

insert  into `product_images`(`prod_cmp_code`,`prod_code`,`prod_catalog_id`,`image_path`,`image_type`,`original_file_name`) values 
('pac','12001',19,'https://indracart1.s3.ap-south-1.amazonaws.com/productimages/pac/19/10001.jpeg','image/jpeg','10001.jpeg'),
('pac','12002',19,'https://indracart1.s3.ap-south-1.amazonaws.com/productimages/pac/19/10002.jpeg','image/jpeg','10002.jpeg'),
('pac','12003',19,'https://indracart1.s3.ap-south-1.amazonaws.com/productimages/pac/19/10005.jpeg','image/jpeg','10005.jpeg'),
('pac','12004',19,'https://indracart1.s3.ap-south-1.amazonaws.com/productimages/pac/19/10006.jpeg','image/jpeg','10006.jpeg'),
('pac','12005',19,'https://indracart1.s3.ap-south-1.amazonaws.com/productimages/pac/19/10006.jpeg','image/jpeg','10008.jpeg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
