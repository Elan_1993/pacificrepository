-- Dec 31 2020

INSERT INTO screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) VALUES ('100','1','Report','Stock Report','fa fa-fw fa-home','fa fa-fw fa-home','./stock-report','1',NULL);              
delete from user_group_privilege_m;
insert into user_group_privilege_m(group_code, module_no, screen_no, is_create, is_edit, is_view, is_delete, modified_by, modified_date) SELECT '1',module_no,screen_no,'1','1','1','1','Admin',now() from screen_m;

INSERT INTO autonumber_d(auto_sequence_number , auto_category) VALUES(1, "Catalog");


-- product table new coloum (02/01/2021)
ALTER TABLE products
ADD COLUMN prod_weight_type VARCHAR(20) AFTER prod_description;
ALTER TABLE products
ADD COLUMN prod_weight DECIMAL(11,2) AFTER prod_weight_type;


INSERT INTO autonumber_d(auto_prefix , auto_sequence_number , auto_category) VALUES("ops" ,1, "Op_stock_h") 
INSERT INTO autonumber_d(auto_prefix , auto_sequence_number , auto_category) VALUES("sah" ,1, "Stk_adj") 

ALTER TABLE stock
ADD COLUMN stk_qty DOUBLE(11,2) AFTER stk_mrp;

ALTER TABLE `opening_stock_d` DROP FOREIGN KEY `fk_os_prod_code`; 
ALTER TABLE `opening_stock_d` ADD CONSTRAINT `fk_os_prod_code` FOREIGN KEY (`osd_prod_code`, `osd_cmp_code`) REFERENCES `pacific`.`products`(`prod_code`, `prod_cmp_code`);

ALTER TABLE `pacific`.`user_m` ADD COLUMN `user_cmp_code` VARCHAR(50) NULL AFTER `is_company_user`; 

--28-01-2021

CREATE TABLE `pacific`.`brand_m` ( `brand_id` INT(11) NOT NULL AUTO_INCREMENT, `brand_name` VARCHAR(50), PRIMARY KEY (`brand_id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci; 

CREATE TABLE `pacific`.`material_m` ( `material_id` INT(11) NOT NULL AUTO_INCREMENT, `material_name` VARCHAR(50), PRIMARY KEY (`material_id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci; 

--26-02-2021

CREATE TABLE `pacific`.`state_t` ( `module_name` VARCHAR(50), `current_state` TINYINT(11), `current_status` VARCHAR(50), `next_state` TINYINT(11), `next_status` VARCHAR(50) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci; 

 
INSERT INTO state_t(module_name , current_state , current_status, next_state , next_status) VALUES("ORDER" , 1 , "Accepted" , 2 ,"Packed");
INSERT INTO state_t(module_name , current_state , current_status, next_state , next_status) VALUES("ORDER" , 2 , "Packed" , 3 ,"Dispatched");
ALTER TABLE state_t CHANGE `module_name` `module_name` VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci NOT NULL, CHANGE `current_state` `current_state` TINYINT NOT NULL, ADD PRIMARY KEY (`module_name`, `current_state`); 

--14-03-2021
INSERT  INTO autonumber_d (auto_prefix , auto_sequence_number , auto_category) VALUES ('order' , 1 , 'Order')

CREATE TABLE `state_t` (
  `state_id` int NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) DEFAULT NULL,
  `current_state` tinyint DEFAULT NULL,
  `current_status` varchar(50) DEFAULT NULL,
  `next_state` tinyint DEFAULT NULL,
  `next_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert  into `state_t`(`state_id`,`module_name`,`current_state`,`current_status`,`next_state`,`next_status`) values 
(1,'ORDER',1,'Accepted',2,'Dispatched');

--04-04-2021
ALTER TABLE `order_h` ADD COLUMN `order_tracking_id` VARCHAR(50) NULL AFTER `order_status`; 

--02-05-2021
INSERT INTO screen_m(module_no, screen_no, module_name, screen_name, module_icon, screen_icon, router_link, sub_menu, sub_menu_name) VALUES ('40','1','Sales','Orders','fa fa-fw fa-line-chart','fa fa-fw fa-sign-in','./orders','1',NULL);              

INSERT INTO user_group_privilege_m(group_code, module_no, screen_no, is_create, is_edit, is_view, is_delete, modified_by, modified_date) VALUES ('Test','40', '1' , '1' , '1' , '1' , '1' ,'Admin' ,'2021-02-23 23:26:06');

--16-05-2021

CREATE TABLE `product_images` (
  `prod_cmp_code` varchar(50) NOT NULL,
  `prod_code` varchar(50) NOT NULL,
  `prod_catalog_id` int NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `image_type` varchar(11) DEFAULT NULL,
  `original_file_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`prod_cmp_code`,`prod_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `order_h` ADD COLUMN `order_gross_amount` DECIMAL(22,6) DEFAULT 0.000000 NULL AFTER `order_qty`, ADD COLUMN `order_tax_amount` DECIMAL(22,6) DEFAULT 0.000000 NULL AFTER `order_gross_amount`, ADD COLUMN `order_disc_amount` DECIMAL(22,6) DEFAULT 0.000000 NULL AFTER `order_tax_amount`, CHANGE `order_ship_amount` `order_ship_amount` DECIMAL(22,6) DEFAULT 0.000000 NULL AFTER `order_disc_amount`, CHANGE `order_amount` `order_amount` DECIMAL(22,6) DEFAULT 0.000000 NULL AFTER `order_ship_amount`; 
ALTER TABLE `order_h` ADD COLUMN `order_courier_name` VARCHAR(50) NULL AFTER `order_address`; 
ALTER TABLE `order_h` ADD COLUMN `order_rpay_no` VARCHAR(50) NULL AFTER `order_status`, ADD COLUMN `order_rpay_status` VARCHAR(50) NULL AFTER `order_rpay_no`, ADD COLUMN `order_rpay_payment_no` VARCHAR(50) NULL AFTER `order_rpay_status`, ADD COLUMN `order_rpay_payment_status` VARCHAR(50) NULL AFTER `order_rpay_payment_no`, ADD COLUMN `order_signature` VARCHAR(200) NULL AFTER `order_rpay_payment_status`; 
ALTER TABLE `order_h` CHANGE `order_signature` `order_rpay_signature` VARCHAR(200) CHARSET utf8 COLLATE utf8_general_ci NULL; 
ALTER TABLE `order_h` ADD COLUMN `order_actual_courier` VARCHAR(100) NULL AFTER `order_address`, CHANGE `order_courier_name` `order_excepted_courier` VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci NULL; 
ALTER TABLE `order_h` CHANGE `order_excepted_courier` `order_expected_courier` VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci NULL; 
CREATE TABLE `payment` ( `order_no` VARCHAR(50) NOT NULL, `rpay_order_no` VARCHAR(50) NOT NULL, `rpay_payment_id` VARCHAR(50), `order_amount` DOUBLE(11,2), `rpay_signature` VARCHAR(200), PRIMARY KEY (`order_no`, `rpay_order_no`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci; 
ALTER TABLE `payment` CHANGE `rpay_order_no` `rpay_order_no` VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (`order_no`); 
ALTER TABLE `payment` CHANGE `order_amount` `order_amount` DOUBLE(22,6) NULL;


07-08-2021
ALTER TABLE `pacific`.`order_h` ADD COLUMN `order_state_id` INT NULL AFTER `order_address`, ADD COLUMN `order_state_name` VARCHAR(100) NULL AFTER `order_state_id`, DROP PRIMARY KEY, ADD PRIMARY KEY (`order_no`); 

 
01-09-2021
ALTER TABLE `pacific`.`customer_m` ADD COLUMN `cus_type` TINYINT(10) NULL AFTER `cus_country`;

15-09-2021
ALTER TABLE `pacific`.`customer_m` ADD COLUMN `created_datetime` DATETIME NULL AFTER `dealership_expiry_date`, ADD COLUMN `modified_datetime` DATETIME NULL AFTER `created_datetime`; 
UPDATE customer_m c JOIN user_m u ON u.user_code = c.cus_code SET c.created_datetime = u.created_datetime , c.modified_datetime = u.modified_datetime