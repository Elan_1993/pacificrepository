package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.pacific.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, String>{
}
