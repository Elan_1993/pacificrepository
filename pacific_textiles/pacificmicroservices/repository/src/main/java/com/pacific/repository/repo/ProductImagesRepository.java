package com.pacific.repository.repo;

import com.pacific.model.ProductImages;
import com.pacific.model.ProductImagesId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProductImagesRepository extends JpaRepository<ProductImages, ProductImagesId>, JpaSpecificationExecutor<ProductImages> {

    List<ProductImages> findByIdProdCmpCodeAndProdCatalogId(String prodCmpCode,Integer prodCatalogId);

}
