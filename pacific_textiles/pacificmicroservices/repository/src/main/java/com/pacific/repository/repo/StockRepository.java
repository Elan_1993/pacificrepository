package com.pacific.repository.repo;

import com.pacific.model.Brand;
import com.pacific.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import com.pacific.model.StockId;
import com.pacific.model.Stock;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StockRepository extends JpaRepository<Stock, StockId> , JpaSpecificationExecutor<Stock> {

    @Query("SELECT s.id.stkProdCode FROM Stock s WHERE s.id.stkProdCode IN (:listOfProdCode) AND s.stkQty =0")
    List<String> findStockIsNotAvailableProductCode(@Param("listOfProdCode") List<String> listOfProdCode);
	
}
