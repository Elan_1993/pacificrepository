package com.pacific.repository.repo;

import com.pacific.model.CartDetails;
import com.pacific.model.CartDetailsId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartDetailsRepository extends JpaRepository<CartDetails, CartDetailsId> {

}
