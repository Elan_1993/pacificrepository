package com.pacific.repository.repo;

import com.pacific.model.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface StateRepository extends JpaRepository<State , Integer>, JpaSpecificationExecutor<State> {

    List<State> findByModuleNameAndCurrentState(String moduleName, Integer currentState);
}


