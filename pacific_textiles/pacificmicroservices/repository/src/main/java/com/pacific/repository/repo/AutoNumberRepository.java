package com.pacific.repository.repo;

import com.pacific.model.AutoNumber;
import com.pacific.model.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AutoNumberRepository extends JpaRepository<AutoNumber, Integer>, JpaSpecificationExecutor<State> {


    AutoNumber findAutoNumberByAutoCategory(String autoCategory);
}
