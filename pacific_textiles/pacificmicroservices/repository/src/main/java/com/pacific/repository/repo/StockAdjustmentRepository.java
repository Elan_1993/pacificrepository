package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pacific.model.StockAdjustmentH;
import com.pacific.model.StockAdjustmentHId;

public interface StockAdjustmentRepository extends JpaRepository<StockAdjustmentH, StockAdjustmentHId> {

}
