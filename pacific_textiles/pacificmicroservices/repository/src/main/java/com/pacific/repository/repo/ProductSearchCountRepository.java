package com.pacific.repository.repo;


import com.pacific.model.ProductSearchCount;
import com.pacific.model.ProductSearchCountId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductSearchCountRepository extends JpaRepository<ProductSearchCount, ProductSearchCountId> {

    @Modifying
    @Query("UPDATE ProductSearchCount SET productCodeCount = 0 where id.prodCode IN :listOfProdCode AND id.prodCmpCode IN :listOfCmpCode")
    void deactivateNoStocksProduct(@Param("listOfProdCode") List<String> listOfProdCode,@Param("listOfCmpCode") List<String> listOfCmpCode);
}
