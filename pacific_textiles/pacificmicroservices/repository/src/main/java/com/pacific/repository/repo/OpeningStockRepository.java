package com.pacific.repository.repo;

import com.pacific.model.OpeningStockHId;
import com.pacific.model.OpeningStockH;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OpeningStockRepository extends JpaRepository<OpeningStockH, OpeningStockHId> {

}
