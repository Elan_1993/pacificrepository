package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.pacific.model.Material;

public interface MaterialRepository extends JpaRepository<Material, Integer>, JpaSpecificationExecutor<Material>{

}
