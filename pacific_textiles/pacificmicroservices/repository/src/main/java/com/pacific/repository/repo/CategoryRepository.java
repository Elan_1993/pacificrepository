package com.pacific.repository.repo;

import com.pacific.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author elan
 */
public interface CategoryRepository extends JpaRepository<Category, Integer>, JpaSpecificationExecutor<Integer> {

    @Query("SELECT ct FROM Category ct LEFT JOIN Category ct1 on ct.catId=ct1.catParentRid " +
            "WHERE ct.catParentRid is not null AND ct1.catId is null")
    List<Category> fetchAllLeastCategory();
}
