package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.pacific.model.OrderH;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<OrderH, String>, JpaSpecificationExecutor<OrderH> {

    @Query(value = "from OrderH t where createdDatetime BETWEEN :startDate AND :endDate AND orderState = :state")
    List<OrderH> getAllBetweenDatesAndOrderStatus(@Param("startDate") Date startDate,
                                                         @Param("endDate")Date endDate,
                                                         @Param("state")Integer state);

    @Query(value = "from OrderH t where createdDatetime BETWEEN :startDate AND :endDate AND orderCusCode = :orderCusCode")
    List<OrderH> getAllBetweenDatesAndOrderOrderCusCode(@Param("startDate") Date startDate,
                                                         @Param("endDate")Date endDate,
                                                         @Param("orderCusCode")String orderCusCode);

    @Query(value = "from OrderH t where createdDatetime BETWEEN :startDate AND :endDate")
    List<OrderH> getAllBetweenDates(@Param("startDate") Date startDate,
                                                         @Param("endDate")Date endDate);
}
