package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.pacific.model.StockTransaction;

public interface StockTransactionRepository extends JpaRepository<StockTransaction, String> {

}
