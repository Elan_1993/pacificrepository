package com.pacific.repository.repo;

import com.pacific.model.UserToken;
import com.pacific.model.UserTokenId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserTokenRepository extends JpaRepository<UserToken, UserTokenId> {


    /**
     * Method to fetch the user access details with token.
     * @param accessKey accessKey
     * @return User Token
     */
    Optional<UserToken> findByToken(String accessKey);



}
