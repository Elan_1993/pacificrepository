package com.pacific.repository.repo;

import com.pacific.model.Product;
import com.pacific.model.ProductId;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, ProductId> {

	@Query(value = "SELECT * FROM products p GROUP BY p.prod_catalog_id", nativeQuery = true)
	List<Product> findAllCatalog();

	List<Product> findByProdCatalogId(Integer prodCatalogId);

	List<Product> findByProdCatalogIdAndProdIsActive(Integer prodCatalogId,Boolean prodIsActive);

	List<Product> findByIdProdCodeContainingIgnoreCase(String prodCode);

	@Query("SELECT m FROM Product m LEFT join Stock stock on m.id.prodCode=stock.id.stkProdCode AND m.id.prodCmpCode=stock.id.stkCmpCode WHERE m.id.prodCode LIKE %:prodCode% " +
			"AND stock.id.stkProdCode is null ")
	List<Product> searchByIdProdCodeLike(@Param("prodCode") String prodCode);

	@Query("SELECT m FROM Product m INNER JOIN Stock stock on m.id.prodCode=stock.id.stkProdCode AND m.id.prodCmpCode=stock.id.stkCmpCode WHERE m.id.prodCode LIKE %:prodCode%")
	List<Product> searchByProductCodeWithStock(@Param("prodCode") String prodCode);
	
	@Query("SELECT p FROM Product p WHERE p.id.prodCode IN (:listOfProdCode)")
	List<Product> searchByMultipeProductCode(@Param("listOfProdCode") List<String> listOfProdCode);

	@Query("SELECT m FROM Product m WHERE m.prodName LIKE %:prodName% GROUP BY m.prodCatalogId")
	List<Product> searchByProdNameLike(String prodName);

	@Query("SELECT p FROM Product p INNER JOIN ProductImages pi ON p.id.prodCode = pi.id.prodCode " +
			"AND p.id.prodCmpCode = pi.id.prodCmpCode AND p.prodCatalogId=pi.prodCatalogId " +
			"WHERE p.id.prodCode LIKE %:prodCode%")
	List<Product> searchByProdCodeLike(String prodCode);


}
