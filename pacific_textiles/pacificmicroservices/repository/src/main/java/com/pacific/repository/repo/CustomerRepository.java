package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pacific.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String>	 {
	   
	Customer findByCusMobileOrCusEmailId(String cusMobile, String cusEmailId); 
}
