package com.pacific.repository.repo;


import com.pacific.model.UserAccessDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author elan
 */
public interface UserAccessRepository extends JpaRepository<UserAccessDetails, String>, JpaSpecificationExecutor<UserAccessDetails> {

    /**
     * Method to fetch the user access details with token.
     * @param accessKey accessKey
     * @return user access detail
     */
    Optional<UserAccessDetails> findByToken(String accessKey);
}
