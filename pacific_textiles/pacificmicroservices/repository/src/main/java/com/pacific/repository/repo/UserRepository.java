package com.pacific.repository.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import com.pacific.model.User;

public interface UserRepository extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM User c WHERE c.userPhone = :userPhone OR c.userMail=:userMail")
	boolean existsByUserPhoneOrUserMail(@Param("userPhone") String userPhone, @Param("userMail") String userMail);

	User findByUserCodeOrUserPhoneOrUserMail(String userCode, String userPhone, String userMail);
	
	List<User> findByUserCmpCodeAndIsUserActive(String userCmpCode, boolean isUserActive);
}
