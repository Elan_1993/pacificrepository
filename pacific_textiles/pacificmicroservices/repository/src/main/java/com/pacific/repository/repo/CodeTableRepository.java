package com.pacific.repository.repo;

import com.pacific.model.CodeTable;
import com.pacific.model.CodeTableId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CodeTableRepository extends JpaRepository<CodeTable, CodeTableId>, JpaSpecificationExecutor<CodeTable> {

    List<CodeTable> findByIdTableName(String tableName);

}
