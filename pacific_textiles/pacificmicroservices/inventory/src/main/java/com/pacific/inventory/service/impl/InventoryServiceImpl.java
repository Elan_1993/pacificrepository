package com.pacific.inventory.service.impl;

import com.pacific.common.constants.StringConstants;
import com.pacific.common.exception.DulabException;
import com.pacific.common.service.CodeGeneratorService;
import com.pacific.common.util.CommonUtil;
import com.pacific.inventory.service.IInventoryService;
import com.pacific.model.*;
import com.pacific.repository.repo.*;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author abdul
 */
@Service
@Transactional
public class InventoryServiceImpl extends CodeGeneratorService implements IInventoryService {

    /**
     * openingStockRepository.
     */
    private final OpeningStockRepository openingStockRepository;

    /**
     * stockAdjustmentRepository.
     */
    private final StockAdjustmentRepository stockAdjustmentRepository;

    /**
     * stockTransactionRepository.
     */
    private final StockTransactionRepository stockTransactionRepository;

    /**
     * stockRepository.
     */
    private final StockRepository stockRepository;

    /**
     * productSearchCountRepository.
     */
    private final ProductSearchCountRepository  productSearchCountRepository;

    /*Stock*/
    private static final String BOOK_STOCK = "Book Stock";
    private static final String STOCK_ADJUSTMENT = "Stock Adjustment";
    private static final String OPENING_STOCK = "Opening Stock";
    private static final String ORDER = "Order";
    private static final String ORDER_DRAFT = "ORDER_DRAFT";
    private static final String ORDER_REVERSAL = "ORDER_REVERSAL";


    /**
     * This method is used to inject the dependency.
     *
     * @param openingStockRepositoryIn
     * @param stockAdjustmentRepositoryIn
     * @param stockTransactionRepositoryIn
     * @param stockRepositoryIn
     * @param productSearchCountRepositoryIn
     */
    public InventoryServiceImpl(final OpeningStockRepository openingStockRepositoryIn,
                                final StockAdjustmentRepository stockAdjustmentRepositoryIn,
                                final StockTransactionRepository stockTransactionRepositoryIn,
                                final StockRepository stockRepositoryIn,
                                final ProductSearchCountRepository productSearchCountRepositoryIn) {
        this.openingStockRepository = openingStockRepositoryIn;
        this.stockAdjustmentRepository = stockAdjustmentRepositoryIn;
        this.stockTransactionRepository = stockTransactionRepositoryIn;
        this.stockRepository = stockRepositoryIn;
        this.productSearchCountRepository = productSearchCountRepositoryIn;
    }

    /**
     * @param openingStockH
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public OpeningStockH saveOpeningStock(final OpeningStockH openingStockH) {
        if (openingStockH != null) {
            if (CommonUtil.isEmpty(openingStockH.getId().getOsCode())) {
                openingStockH.getId().setOsCode(generateCode(StringConstants.OP_STOCK_H));
            }
            openingStockH.setCreatedDatetime(new Date());
            openingStockH.setModifiedDatetime(new Date());
            openingStockH.getOpeningStockDs()
                    .forEach(openingStockD -> openingStockD.getId().setOsCode(openingStockH.getId().getOsCode()));
        }
        stockByOpeningStockH(openingStockH);
        var productCodeList = openingStockH.getOpeningStockDs().stream().map(x->x.getId().getOsdProdCode()).collect(Collectors.toList());
        var productCmpCodeList = openingStockH.getOpeningStockDs().stream().map(x->x.getId().getOsdCmpCode()).collect(Collectors.toList());
        productSearchCountRepository.deactivateNoStocksProduct(productCodeList,productCmpCodeList);
        return openingStockRepository.save(openingStockH);
    }

    private void stockByOpeningStockH(OpeningStockH openingStockH) {
        Integer ddStkTypeIndex = 4;
        Double issueQty = 0.00d;
        for (var openingStockDData : openingStockH.getOpeningStockDs()) {
            handleStock(1, 1, openingStockDData.getId().getOsdProdCode(), openingStockDData.getOsdQty(),
                    issueQty, openingStockDData.getOsdRate(), openingStockDData.getOsdMrp(), openingStockH.getId().getOsCode(),
                    BOOK_STOCK, ddStkTypeIndex, openingStockDData.getId().getOsdCmpCode(), OPENING_STOCK);
        }
    }

    /**
     * @param stockAdjustmentH
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public StockAdjustmentH saveStockAdjustment(StockAdjustmentH stockAdjustmentH) {
        if (stockAdjustmentH != null) {
            if (CommonUtil.isEmpty(stockAdjustmentH.getId().getSahCode())) {
                stockAdjustmentH.getId().setSahCode(generateCode(StringConstants.STOCK_ADJUSTMENT));
            }
            stockAdjustmentH.setSahDate(new Date());
            stockAdjustmentH.setCreatedDatetime(new Date());
            stockAdjustmentH.setModifiedDatetime(new Date());
            stockAdjustmentH.getStockAdjustmentDs().forEach(
                    stockAdjustmentD -> stockAdjustmentD.getId().setSahCode(stockAdjustmentH.getId().getSahCode()));
        }
        stockByStockAdjustment(stockAdjustmentH);
        var productCodeList = stockAdjustmentH.getStockAdjustmentDs().stream().map(x->x.getId().getSadProdCode()).collect(Collectors.toList());
        var productCmpCodeList = stockAdjustmentH.getStockAdjustmentDs().stream().map(x->x.getId().getSadCmpCode()).collect(Collectors.toList());
        productSearchCountRepository.deactivateNoStocksProduct(productCodeList,productCmpCodeList);
        return stockAdjustmentRepository.save(stockAdjustmentH);
    }

    @Override
    public void reduceStockByOrder(OrderH orderH,String orderType) throws Exception{
        for (OrderD orderD : orderH.getOrderDs()) {
            Integer ddStkTypeIndex = 4;
            Double issueQty = orderD.getOrderQty();
            var tranCode = 0;
            var stkType = 0;
            switch (orderType){
                case ORDER_DRAFT:
                    tranCode = -2;
                    stkType = 0;
                 break;
                case ORDER:
                    tranCode = 0;
                    stkType = 0;
                 break;
                case ORDER_REVERSAL:
                    tranCode = 1;
                    stkType = 1;
                 break;
            }
            handleStock(stkType, tranCode, orderD.getId().getOrderProdCode(), orderD.getOrderQty(),
                    issueQty, orderD.getOrderRate(), orderD.getOrderRate(), orderD.getId().getOrderNo(),
                    BOOK_STOCK, ddStkTypeIndex, orderD.getId().getOrderCmpCode(), orderType);
        }

    }

    private void stockByStockAdjustment(final StockAdjustmentH stockAdjustmentH) {
        Integer stkType = 0, tranCode = 0, ddStkTypeIndex = 4;
        String ddStockType = BOOK_STOCK;
        for (StockAdjustmentD stockAdjustmentDData : stockAdjustmentH.getStockAdjustmentDs()) {
            Double issueQty = 0.00d, receiptQty = 0.00d;
            var stockHList = getStock(stockAdjustmentDData.getId().getSadProdCode(), stockAdjustmentDData.getId().getSadCmpCode());
            if (stockHList != null) {
                if (stockHList.getStkQty() < stockAdjustmentDData.getSadQty()) {
                    stockAdjustmentDData.setSadQty(stockAdjustmentDData.getSadQty() - stockHList.getStkQty());
                    stkType = 1;
                    tranCode = 1;
                    receiptQty = stockAdjustmentDData.getSadQty();
                } else if (stockHList.getStkQty() > stockAdjustmentDData.getSadQty()) {
                    stockAdjustmentDData.setSadQty(stockHList.getStkQty() - stockAdjustmentDData.getSadQty());
                    stkType = 0;
                    tranCode = 0;
                    issueQty = stockAdjustmentDData.getSadQty();
                } else if (Objects.equals(stockHList.getStkQty(), stockAdjustmentDData.getSadQty())) {
                    stockAdjustmentDData.setSadQty(0.00d);
                    stkType = 1;
                    tranCode = 2;
                    receiptQty = 0.00d;
                    issueQty = 0.00d;
                }
            }

            if (issueQty != 0 || receiptQty != 0) {
                handleStock(stkType, tranCode, stockAdjustmentDData.getId().getSadProdCode(), receiptQty,
                        issueQty, stockAdjustmentDData.getSadRate(), stockAdjustmentDData.getSadMrp(), stockAdjustmentH.getId().getSahCode(),
                        ddStockType, ddStkTypeIndex, stockAdjustmentH.getId().getSahCmpCode(), STOCK_ADJUSTMENT);
            }

        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void handleStock(Integer stkType, Integer tranCode, String productCode,
                             Double receiptQty, Double issueQty, Double rate, Double mrp, String refCode,
                             String ddStockType, Integer ddStkTypeIndex, String cmpCode, String stockTransaction) throws DulabException {
        var stockHList = getStock(productCode, cmpCode);
        if (stockHList != null) {
            if (stkType == 0) {
                receiptQty = issueQty;
            }
            updateStockInOut(stkType, stockTransaction,receiptQty, rate, mrp, stockHList);
            if (receiptQty.equals(issueQty)) {
                receiptQty = 0.00d;
            }
            insertStockTransaction(tranCode, productCode, rate, mrp, receiptQty, issueQty, refCode,
                    cmpCode, stockTransaction);
        } else {
            stockHList = new Stock();
            stockHList.setId(new StockId(productCode, cmpCode));
            updateStockInOut(stkType, stockTransaction, receiptQty,rate, mrp, stockHList);
            insertStockTransaction(tranCode, productCode, rate, mrp, receiptQty, issueQty, refCode,
                    cmpCode, stockTransaction);
        }
    }

    @Override
    public List<String> findStockIsNotAvailableProductCode(List<String> productCodeList) {
        return stockRepository.findStockIsNotAvailableProductCode(productCodeList);
    }

    private void updateStockInOut(Integer stkType,String stockTransaction, Double qty, Double cp, Double sp,
                                 Stock stockPresentData) throws DulabException {
        Double stkQty = 0.00d;
        Double unSaleableQty = 0.00d;
        stkQty = stockPresentData.getStkQty() != null ? stockPresentData.getStkQty() : stkQty;
        unSaleableQty = stockPresentData.getUnSaleableQty() != null ? stockPresentData.getUnSaleableQty() : unSaleableQty;
        if (stkType == 1) {
            if(stockTransaction.equals(ORDER_REVERSAL)){
                if ((unSaleableQty - qty) < 0) {
                    throw new DulabException("Invalid Qty");
                }
                stockPresentData.setUnSaleableQty(unSaleableQty - qty);
            }
            stockPresentData.setStkQty(stkQty + qty);
        } else if (stkType == 0 && (stkQty != 0|| unSaleableQty !=0)) {
            switch (stockTransaction){
                case ORDER_DRAFT:
                    if ((stkQty - qty) < 0) {
                        throw new DulabException("Invalid Qty");
                    }
                    stockPresentData.setStkQty(stkQty - qty);
                    stockPresentData.setUnSaleableQty(unSaleableQty + qty);
                    break;
                case ORDER:
                    if ((unSaleableQty - qty) < 0) {
                        throw new DulabException("Invalid Qty");
                    }
                    stockPresentData.setUnSaleableQty(unSaleableQty - qty);
                    break;
                default:
                    if ((stkQty - qty) < 0) {
                        throw new DulabException("Invalid Qty");
                    }

                    stockPresentData.setStkQty(stkQty - qty);
                    break;
            }
        } else if (stkType == 0 && stockPresentData.getStkQty() == 0) {
            throw new DulabException("Invalid Qty");
        }
        stockPresentData.setStkMrp(sp);
        stockPresentData.setStkRate(cp);
        stockPresentData.setCreatedDatetime(new Date());
        stockPresentData.setModifiedDatetime(new Date());
        stockPresentData.setStkDate(new Date());

        stockRepository.save(stockPresentData);
    }

    /**
     * @param prodCode
     * @param cmpCode
     * @return
     */
    public Stock getStock(final String prodCode, final String cmpCode) {
        var stock = stockRepository.findById(new StockId(prodCode, cmpCode));
        return stock.isPresent() ? stock.get() : null;
    }

    private void insertStockTransaction(Integer tranCode, String prodCode,
                                       Double rate, Double mrp, Double receiptQty, Double issueQty, String refCode,
                                       String cmpCode, String stockTransactionType) throws DulabException {
        var stState = 0;
        if (refCode != null) {
            if (stockTransactionType.contains(OPENING_STOCK)) {
                stState = 1;
            } else if (stockTransactionType.contains(STOCK_ADJUSTMENT)) {
                stState = 2;
            } else if(stockTransactionType.contains(ORDER)){
                stState = 3;
            } else if(stockTransactionType.contains(ORDER_DRAFT)){
                stState = 4;
                tranCode = -1;
            } else if(stockTransactionType.contains(ORDER_REVERSAL)){
                refCode = refCode.concat("/");
                stState = 4;
                tranCode = -1;
            }
        }
        if(!stockTransactionType.equalsIgnoreCase(ORDER_DRAFT)){
            var stockTransactionId = new StockTransactionId(refCode, new Date(), prodCode, cmpCode);
            var stockTransaction = new StockTransaction(stockTransactionId, receiptQty, issueQty, mrp, rate, stState, tranCode, refCode, stockTransactionType);
            stockTransactionRepository.save(stockTransaction);
        }

    }




}
