package com.pacific.inventory.controller;

import com.pacific.inventory.service.IInventoryService;
import com.pacific.model.OpeningStockH;
import com.pacific.model.StockAdjustmentH;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/inventory/controller")
public class InventoryController {


	/** stockAdjustmentService. */
	@Autowired
	private IInventoryService inventoryService;

	@PostMapping("/save-opening-stock")
	public OpeningStockH saveOpeningStock(@RequestBody final OpeningStockH openingStockH) {
		return inventoryService.saveOpeningStock(openingStockH);
	}

	@PostMapping("/save-stock-adjustment")
	public StockAdjustmentH saveStockAdjustment(@RequestBody final StockAdjustmentH stockAdjustmentH) {
		return inventoryService.saveStockAdjustment(stockAdjustmentH);
	}
}
