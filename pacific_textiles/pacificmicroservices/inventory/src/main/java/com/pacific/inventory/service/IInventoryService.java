package com.pacific.inventory.service;

import com.pacific.common.exception.DulabException;
import com.pacific.model.OpeningStockH;
import com.pacific.model.Stock;
import com.pacific.model.StockAdjustmentH;
import com.pacific.model.OrderH;

import java.util.List;

/**
 * @author abdul
 */
public interface IInventoryService {

    //--------------------------------------------------Opening Stock-------------------------------------------------

    /**
     *
     * @param openingStockH
     * @return
     */
    OpeningStockH saveOpeningStock(OpeningStockH openingStockH);

    //--------------------------------------------------Stock Adjustment----------------------------------------------

    /**
     *
     * @param stockAdjustmentH
     * @return
     */
    StockAdjustmentH saveStockAdjustment(StockAdjustmentH stockAdjustmentH);

    //--------------------------------------------------Stock Reduce From Order----------------------------------------------

    /**
     *
     * @param orderH
     * @return
     */
    void reduceStockByOrder(OrderH orderH,String orderType) throws Exception;

    /**
     *
     * @param stkType
     * @param tranCode
     * @param productCode
     * @param receiptQty
     * @param issueQty
     * @param rate
     * @param mrp
     * @param refCode
     * @param ddStockType
     * @param ddStkTypeIndex
     * @param cmpCode
     * @param stockTransaction
     * @throws DulabException
     */
    public void handleStock(Integer stkType, Integer tranCode, String productCode,
                            Double receiptQty, Double issueQty, Double rate, Double mrp, String refCode,
                            String ddStockType, Integer ddStkTypeIndex, String cmpCode, String stockTransaction) throws DulabException;

    /**
     *
     * @param productCodeList
     * @return
     */
    List<String> findStockIsNotAvailableProductCode(List<String> productCodeList);

    Stock getStock(final String prodCode, final String cmpCode);
}
