package com.pacific.common.constants;

public final class StringConstants {

	/** MODULE_NO. */
	public static final String MODULENO = "moduleNo";
	/** SUB_MENU. */
	public static final String SUB_MENU = "subMenu";
	/** SCREEN_NO. */
	public static final String SCREENNO = "screenNo";
	/** SCREEN_NAME. */
	public static final String SCREEN_NAME = "screenName";
	/** ROUTER_LINK. */
	public static final String ROUTER_LINK = "routerLink";
	/** SCREEN_ICON. */
	public static final String SCREEN_ICON = "screenIcon";
	/** MODULE_NAME. */
	public static final String MODULE_NAME = "moduleName";
	/** MODULE_ICON. */
	public static final String MODULE_ICON = "moduleIcon";
	/** SUB_MENU_NAME */
	public static final String SUB_MENU_NAME = "subMenuName";
	/** AUTO_NUMBER_USER */
	public static final String USER = "User";
	/** AUTO_NUMBER_CUSTOMER */
	public static final String CUSTOMER = "Customer";
	/** AUTO_NUMBER_CATALOG. */
	public static final String CATALOG = "Catalog";
	/** AUTO_NUMBER_OP_STOCK_H */
	public static final String OP_STOCK_H = "Op_stock_h";
	/** AUTO_NUMBER_STOCK */
	public static final String STOCK = "Stock";
	/** AUTO_NUMBER_STOCK_ADJUSTMENT. */
	public static final String STOCK_ADJUSTMENT = "Stk_adj";
	/** AUTO_NUMBER_ORDER. */
	public static final String ORDER = "Order";

	/** AUTHORIZATION. */
	public static final String AUTHORIZATION = "X-Auth-Token";

	/** PARAMETERMISSING. */
	public static final String PARAMETERMISSING = "X-Auth-Token is missing";

	/** SESSIONEXPIRED. */
	public static final String SESSIONEXPIRED = "Session Expired";

	/** INVALIDPARAMETERS. */
	public static final String INVALIDPARAMETERS = "Invalid X-Auth-Token";

	/** VALIDATIONSUCCESS. */
	public static final String VALIDATIONSUCCESS = "Success";

	/** MASK1. */
	public static final int MASK1 = 0xff;

	/** MASK2. */
	public static final int MASK2 = 0x10;

	/** MASK3. */
	public static final int MASK3 = 0xFF;

	/** QUERY_AND. */
	public static final String QUERY_AND = "\nAND ";

	/** WHERE. */
	public static final String WHERE = " WHERE ";

	/** ORDER_BY. */
	public static final String ORDER_BY = " ORDER BY ";

	/** NEW_COLLECTIONS. */
	public static final String NEW_COLLECTIONS = "New Collections";

	/** TRENDING_COLLECTIONS. */
	public static final String TRENDING_COLLECTIONS = "Trending Collections";

	/** SPECIAL_OFFERS. */
	public static final String  SPECIAL_OFFERS = "Special Offers";

	/** PRODUCT_IMAGES. */
	public static final String  PRODUCT_IMAGES = "ProductImages1";

	/** IMAGE_WIDTH. */
	public static final Integer IMAGE_WIDTH = 120;

	/** IMAGE_HEIGHT. */
	public static final Integer IMAGE_HEIGHT = 120;

	public static final Integer NUMBER_FOUR = 4;

	public static final Integer RETAILER = 1;

	public static final Integer RESELLER = 2;

	public static final Integer DEALER = 3;

	/*Stock*/
	private static final String BOOK_STOCK = "Book Stock";
	private static final String INVENTORY_STOCK_ADJUSTMENT = "Stock Adjustment";
	private static final String OPENING_STOCK = "Opening Stock";
	private static final String INVENTORY_ORDER = "Order";
	private static final String ORDER_DRAFT = "ORDER_DRAFT";
	private static final String ORDER_REVERSAL = "ORDER_REVERSAL";
}
