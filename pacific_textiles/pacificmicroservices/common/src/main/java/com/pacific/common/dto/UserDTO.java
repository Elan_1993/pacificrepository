package com.pacific.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is used to get User DTO object.
 */
public class UserDTO implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private String userCode;
    private String userName;
    private String phone;
    private String loginPwd;
    private String mailId;
    private Character companyUser;
    private Boolean paid;
    private String paidRefNo;
    private Boolean userActive;
    private Boolean userApproved;
    private Character gender;
    private Date dob;
    private String streetAddr;
    private String city;
    private String country;
    private String pincode;
    private Date passwordexpirydate;
    private Date accntLockedTillDtime;
    private Boolean locked;
    private Integer incorrectPasswordCntr;
    private String groupCode;
    private String userCmpCode;

    /**
     * @return the userCode
     */
    public final String getUserCode() {
        return userCode;
    }

    /**
     * @param userCodeIn the userCode to set
     */
    public final void setUserCode(final String userCodeIn) {
        userCode = userCodeIn;
    }

    /**
     * @return the userName
     */
    public final String getUserName() {
        return userName;
    }

    /**
     * @param userNameIn the userName to set
     */
    public final void setUserName(final String userNameIn) {
        userName = userNameIn;
    }

    /**
     * @return the phone
     */
    public final String getPhone() {
        return phone;
    }

    /**
     * @param phoneIn the phone to set
     */
    public final void setPhone(final String phoneIn) {
        phone = phoneIn;
    }

    /**
     * @return the loginPwd
     */
    public final String getLoginPwd() {
        return loginPwd;
    }

    /**
     * @param loginPwdIn the loginPwd to set
     */
    public final void setLoginPwd(final String loginPwdIn) {
        loginPwd = loginPwdIn;
    }

    /**
     * @return the mailId
     */
    public final String getMailId() {
        return mailId;
    }

    /**
     * @param mailIdIn the mailId to set
     */
    public final void setMailId(final String mailIdIn) {
        mailId = mailIdIn;
    }

    /**
     * @return the companyUser
     */
    public final Character getCompanyUser() {
        return companyUser;
    }

    /**
     * @param companyUserIn the companyUser to set
     */
    public final void setCompanyUser(final Character companyUserIn) {
        companyUser = companyUserIn;
    }

    /**
     * @return the paid
     */
    public final Boolean getPaid() {
        return paid;
    }

    /**
     * @param paidIn the paid to set
     */
    public final void setPaid(final Boolean paidIn) {
        paid = paidIn;
    }

    /**
     * @return the paidRefNo
     */
    public final String getPaidRefNo() {
        return paidRefNo;
    }

    /**
     * @param paidRefNoIn the paidRefNo to set
     */
    public final void setPaidRefNo(final String paidRefNoIn) {
        paidRefNo = paidRefNoIn;
    }

    /**
     * @return the userActive
     */
    public final Boolean getUserActive() {
        return userActive;
    }

    /**
     * @param userActiveIn the userActive to set
     */
    public final void setUserActive(final Boolean userActiveIn) {
        userActive = userActiveIn;
    }

    /**
     * @return the userApproved
     */
    public final Boolean getUserApproved() {
        return userApproved;
    }

    /**
     * @param userApprovedIn the userApproved to set
     */
    public final void setUserApproved(final Boolean userApprovedIn) {
        userApproved = userApprovedIn;
    }

    /**
     * @return the gender
     */
    public final Character getGender() {
        return gender;
    }

    /**
     * @param genderIn the gender to set
     */
    public final void setGender(final Character genderIn) {
        gender = genderIn;
    }

    /**
     * @return the dob
     */
    public final Date getDob() {
        return dob;
    }

    /**
     * @param dobIn the dob to set
     */
    public final void setDob(final Date dobIn) {
        dob = dobIn;
    }

    /**
     * @return the streetAddr
     */
    public final String getStreetAddr() {
        return streetAddr;
    }

    /**
     * @param streetAddrIn the streetAddr to set
     */
    public final void setStreetAddr(final String streetAddrIn) {
        streetAddr = streetAddrIn;
    }

    /**
     * @return the city
     */
    public final String getCity() {
        return city;
    }

    /**
     * @param cityIn the city to set
     */
    public final void setCity(final String cityIn) {
        city = cityIn;
    }

    /**
     * @return the country
     */
    public final String getCountry() {
        return country;
    }

    /**
     * @param countryIn the country to set
     */
    public final void setCountry(final String countryIn) {
        country = countryIn;
    }

    /**
     * @return the pincode
     */
    public final String getPincode() {
        return pincode;
    }

    /**
     * @param pincodeIn the pincode to set
     */
    public final void setPincode(final String pincodeIn) {
        pincode = pincodeIn;
    }

    /**
     * @return the passwordexpirydate
     */
    public final Date getPasswordexpirydate() {
        return passwordexpirydate;
    }

    /**
     * @param passwordexpirydateIn the passwordexpirydate to set
     */
    public final void setPasswordexpirydate(final Date passwordexpirydateIn) {
        passwordexpirydate = passwordexpirydateIn;
    }

    /**
     * @return the accntLockedTillDtime
     */
    public final Date getAccntLockedTillDtime() {
        return accntLockedTillDtime;
    }

    /**
     * @param accntLockedTillDtimeIn the accntLockedTillDtime to set
     */
    public final void setAccntLockedTillDtime(final Date accntLockedTillDtimeIn) {
        accntLockedTillDtime = accntLockedTillDtimeIn;
    }

    /**
     * @return the locked
     */
    public final Boolean getLocked() {
        return locked;
    }

    /**
     * @param lockedIn the locked to set
     */
    public final void setLocked(final Boolean lockedIn) {
        locked = lockedIn;
    }

    /**
     * @return the incorrectPasswordCntr
     */
    public final Integer getIncorrectPasswordCntr() {
        return incorrectPasswordCntr;
    }

    /**
     * @param incorrectPasswordCntrIn the incorrectPasswordCntr to set
     */
    public final void setIncorrectPasswordCntr(final Integer incorrectPasswordCntrIn) {
        incorrectPasswordCntr = incorrectPasswordCntrIn;
    }

    /**
     * @return the groupCode
     */
    public final String getGroupCode() {
        return groupCode;
    }

    /**
     * @param groupCodeIn the groupCode to set
     */
    public final void setGroupCode(final String groupCodeIn) {
        groupCode = groupCodeIn;
    }

    /**
     * @return the userCmpCode
     */
    public final String getUserCmpCode() {
        return userCmpCode;
    }

    /**
     * @param userCmpCodeIn the userCmpCode to set
     */
    public final void setUserCmpCode(final String userCmpCodeIn) {
        userCmpCode = userCmpCodeIn;
    }
}
