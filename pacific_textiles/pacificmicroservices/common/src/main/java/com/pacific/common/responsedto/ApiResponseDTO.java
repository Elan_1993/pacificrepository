package com.pacific.common.responsedto;


public class ApiResponseDTO extends BaseResponseDTO {

    private String id;
    private boolean status;

    public ApiResponseDTO(String id, String message) {
        super(message);
        this.id = id;
    }

    public ApiResponseDTO(String message, String id, boolean status) {
        super(message);
        this.id = id;
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
