package com.pacific.common.constants;

/**
 * Class contains the messages.
 * @author elan
 */
public final class MessageConstants {
    /** USER_STATUS. */
    public static final String USER_STATUS = "Login Id is deactivated,Please contact Admin";

    /** INVALID_LOGIN. */
    public static final String INVALID_LOGIN = "Either username/password is invalid";

    /** SUCCESS. */
    public static final String SUCCESS = "success";

    /** INVALID_DIRECTCHAINDISTR. */
    public static final String INVALID_DIRECTCHAINDISTR = "Login not allowed for Direct Chain Distributors";

    /** DUPLICATE_LOGIN. */
    public static final String DUPLICATE_LOGIN = "Duplicate login/user code found";

    /**
     * Empty constructor.
     */
    private MessageConstants() {

    }
}
