package com.pacific.common.util;


import com.pacific.common.constants.StringConstants;
import com.pacific.common.exception.DulabException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * PasswordEncryptor class to digest the password.
 * @author elan
 */
public final class PasswordEncryptor {

    /**
     * Empty constructor.
     */
    private PasswordEncryptor() {

    }

    /**
     * Method to digest the input values for the password.
     * @param password password
     * @param salt salt
     * @return response
     */
    public static String digest(final String password, final String salt) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(salt.toLowerCase().getBytes());
            md.digest();
            return toHex(md.digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new DulabException(e);
        }
    }

    /**
     * Method to convert the byte[] to string.
     * @param bytes bytes
     * @return string
     */
    private static String toHex(final byte[] bytes) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            if ((StringConstants.MASK1 & bytes[i]) < StringConstants.MASK2) {
                buf.append("0" + Integer.toHexString(StringConstants.MASK3 & bytes[i]));
            } else {
                buf.append(Integer.toHexString(StringConstants.MASK3 & bytes[i]));
            }
        }
        return buf.toString();
    }
}
