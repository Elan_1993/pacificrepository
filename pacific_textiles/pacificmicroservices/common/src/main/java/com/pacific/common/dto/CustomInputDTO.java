package com.pacific.common.dto;

import java.util.Date;

public class CustomInputDTO {

    /** first. */
    private int first;

    /** rows. */
    private int rows;

    /** productCode. */
    private String productCode;

    /** billingUser. */
    private boolean billingUser;

    /** conditions. */
    private ConditionInputDTO conditions;

    /** fromDate. */
    private Date fromDate;

    /** toDate. */
    private Date toDate;

    /** status. */
    private Integer status;

    /** status. */
    private String userCode;

    /** customerType. */
    private Integer cusType;

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * @return the conditions
     */
    public final ConditionInputDTO getConditions() {
        return conditions;
    }

    /**
     * @param conditionsIn the conditions to set
     */
    public final void setConditions(final ConditionInputDTO conditionsIn) {
        conditions = conditionsIn;
    }


    /**
     * @return the productCode
     */
    public final String getProductCode() {
        return productCode;
    }

    /**
     * @param productCodeIn the productCode to set
     */
    public final void setProductCode(final String productCodeIn) {
        productCode = productCodeIn;
    }

    /**
     *
     * @return the billingUser
     */
    public final boolean isBillingUser() {
        return billingUser;
    }

    /**
     *
     * @param billingUserIn the billingUser to set
     */
    public final void setBillingUser(final boolean billingUserIn) {
        this.billingUser = billingUserIn;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Integer getCusType() {
        return cusType;
    }

    public void setCusType(Integer cusType) {
        this.cusType = cusType;
    }
}
