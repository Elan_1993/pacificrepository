package com.pacific.common.constants;

public final class QueryNames {

    /** USER_INFO. */
    public static final String USER_INFO = "USER_INFO";

    /** INSERT_USER_LOGIN_LOG. */
    public static final String INSERT_USER_LOGIN_LOG = "INSERT_USER_LOGIN_LOG";

    /** SELECT_SCREENS. */
    public static final String SELECT_SCREENS = "SELECT_SCREENS";

    /** SELECT_ROLE_ACCESS. */
    public static final String SELECT_ROLE_ACCESS = "SELECT_ROLE_ACCESS";

    /** USER_LOGIN. */
    public static final String USER_LOGIN = "USER_LOGIN";

    /** UPDATE_USER_LOGOUT_LOG. */
    public static final String UPDATE_USER_LOGOUT_LOG = "UPDATE_USER_LOGOUT_LOG";

    /** SELECT_AUTO_NUMBER. */
    public static final String SELECT_AUTO_NUMBER = "SELECT_AUTO_NUMBER";

    /** SELECT_AUTO_NUMBER. */
    public static final String SELECT_AUTO_CATALOG_NUMBER = "SELECT_AUTO_CATALOG_NUMBER";

    /** UPDATE_AUTO_NUMBER_SEQUENCE. */
    public static final String UPDATE_AUTO_NUMBER_SEQUENCE = "UPDATE_AUTO_NUMBER_SEQUENCE";

    /** SELECT_USER_GROUP. */
    public static final String SELECT_USER_GROUP = "SELECT_USER_GROUP";
    
    /** SELECT_BRAND. */
    public static final String SELECT_BRAND = "SELECT_BRAND";
    
    /** SELECT_MATERIAL. */
    public static final String SELECT_MATERIAL = "SELECT_MATERIAL";

    /** SELECT_PRODUCT_INFO. */
    public static final String SELECT_PRODUCT_INFO = "SELECT_PRODUCT_INFO";

    /** SELECT_NEW_COLLECTIONS. */
    public static final String SELECT_NEW_COLLECTIONS = "SELECT_NEW_COLLECTIONS";

    //** SELECT_SPECIAL_OFFERS. */
    public static final String SELECT_SPECIAL_OFFERS = "SELECT_SPECIAL_OFFERS";

    //** INSERT_PRODUCT_SEARCH_COUNT .*/
    public static final String INSERT_PRODUCT_SEARCH_COUNT ="INSERT_PRODUCT_SEARCH_COUNT";

}
