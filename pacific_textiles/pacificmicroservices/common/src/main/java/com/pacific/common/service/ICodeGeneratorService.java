package com.pacific.common.service;

import java.util.HashMap;

public interface ICodeGeneratorService {

	/**
	 *
	 * @param autoCategory autoCategory
	 * @return String
	 */
	String generateCode(String autoCategory);

	Integer generateSequence(String autoCategory);

}
