package com.pacific.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CommonUtil {

    /**
     * @param str
     * @return
     */
    public static boolean isEmpty(final String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isEmpty(final Integer integer) {
        return integer == null || integer == 0;
    }

    public static boolean isEmpty(final Object obj) {
        return obj == null;
    }

    public static boolean isEmpty(final List list) {
        return list == null || list.isEmpty();
    }

    public static String validString(final String str) {
        return isEmpty(str) ? "" : str.trim();
    }


    public static String convertRupeeToPaise(String rupee) {
        BigDecimal b = new BigDecimal(rupee);
        BigDecimal value = b.multiply(new BigDecimal("100"));
        return value.setScale(0, RoundingMode.UP).toString();
    }


}
