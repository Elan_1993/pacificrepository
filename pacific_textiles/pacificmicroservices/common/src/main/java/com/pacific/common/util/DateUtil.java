package com.pacific.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    static SimpleDateFormat dateFormat_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String convertDateToString(Date date) {
        return dateFormat_YYYY_MM_DD.format(date);
    }

    public static Date convertStringToDate(String date) throws ParseException {
        return dateFormat_YYYY_MM_DD.parse(date);
    }

    public static String getYesterdayDateString() {
        return dateFormat_YYYY_MM_DD.format(yesterday());
    }

    public static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static String getDateStringAsGivenDays(Integer days) {
        return dateFormat_YYYY_MM_DD.format(getDateByGivenInteger(days));
    }

    public static Date getDateByGivenInteger(Integer days) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -days);
        return cal.getTime();
    }
}
