package com.pacific.common.exception;

import com.pacific.common.responsedto.ResponseBeanDTO;

public class CommonExceptionHandler {

    public static void ExceptionHandler(Exception e, ResponseBeanDTO<?> orderHResponse) throws Exception {
        try {
            throw e;
        } catch (DulabException ex) {
            orderHResponse.setResponseCode(91);
            orderHResponse.setMessage(e.getMessage());
        }
    }
}
