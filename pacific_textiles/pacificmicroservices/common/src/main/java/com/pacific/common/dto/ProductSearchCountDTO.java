package com.pacific.common.dto;


public class ProductSearchCountDTO {

    private String prodCode;
    private String prodCmpCode;
    private Integer productCodeCount;

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getProdCmpCode() {
        return prodCmpCode;
    }

    public void setProdCmpCode(String prodCmpCode) {
        this.prodCmpCode = prodCmpCode;
    }

    public Integer getProductCodeCount() {
        return productCodeCount;
    }

    public void setProductCodeCount(Integer productCodeCount) {
        this.productCodeCount = productCodeCount;
    }
}
