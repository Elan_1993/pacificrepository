package com.pacific.common.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author Acer-pc
 *
 */
public class DropDownValuesDTO {
	/**
	 * brandList
	 */
	private List<DropDownDTO> brandList;

	/**
	 * materialList
	 */
	private List<DropDownDTO> materialList;

	/**
	 * categoryList
	 */
	private List<DropDownDTO> categoryList;

	/**
	 * categoryList
	 */
	private List<DropDownDTO> courierList;

	/**
	 * categoryList
	 */
	private Map<String, BigDecimal> courierPriceList;

	/**
	 * stateList
	 */
	private List<DropDownDTO> stateList;

	public List<DropDownDTO> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<DropDownDTO> brandList) {
		this.brandList = brandList;
	}

	public List<DropDownDTO> getMaterialList() {
		return materialList;
	}

	public void setMaterialList(List<DropDownDTO> materialList) {
		this.materialList = materialList;
	}

	public List<DropDownDTO> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<DropDownDTO> categoryList) {
		this.categoryList = categoryList;
	}

	public List<DropDownDTO> getStateList() {
		return stateList;
	}

	public void setStateList(List<DropDownDTO> stateList) {
		this.stateList = stateList;
	}

	public List<DropDownDTO> getCourierList() {
		return courierList;
	}

	public void setCourierList(List<DropDownDTO> courierList) {
		this.courierList = courierList;
	}

	public Map<String, BigDecimal> getCourierPriceList() {
		return courierPriceList;
	}

	public void setCourierPriceList(Map<String, BigDecimal> courierPriceList) {
		this.courierPriceList = courierPriceList;
	}
}
