package com.pacific.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ProductDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double prodPrice;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String prodName;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String prodCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String prodCmpCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double stkQty;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer prodCatalogId;


    private String  id;
    private String title;
    private String description;
    private String  type;
    private String brand;
    private String category;
    private Double  price;
    private boolean sale =true;
    private Double discount;
    private Double stock;
    private boolean newProduct = false;
    private Double quantity;
    private String  cmpCode;
    private String  prodWeightType;
    private Double  prodWeight;
    private String  alt;
    private String  src;
    private String  size;
    private String  color;

    public ProductDTO() {
        // Default Constructor
    }

    public ProductDTO(String prodName, String prodCode, String prodCmpCode, Integer prodCatalogId) {
        this.prodPrice = prodPrice;
        this.prodName = prodName;
        this.prodCode = prodCode;
        this.prodCmpCode = prodCmpCode;
        this.prodCatalogId = prodCatalogId;
    }

    /**
     * @return the prodName
     */
    public final String getProdName() {
        return prodName;
    }

    /**
     * @param prodNameIn the prodName to set
     */
    public final void setProdName(final String prodNameIn) {
        prodName = prodNameIn;
    }

    /**
     * @return the prodPrice
     */
    public final Double getProdPrice() {
        return prodPrice;
    }

    /**
     * @param prodPriceIn the prodPrice to set
     */
    public final void setProdPrice(final Double prodPriceIn) {
        prodPrice = prodPriceIn;
    }


    /**
     * @return the prodCode
     */
    public final String getProdCode() {
        return prodCode;
    }

    /**
     * @param prodCodeIn the prodCode to set
     */
    public final void setProdCode(final String prodCodeIn) {
        prodCode = prodCodeIn;
    }

    /**
     * @return the prodCmpCode
     */
    public final String getProdCmpCode() {
        return prodCmpCode;
    }

    /**
     * @param prodCmpCodeIn the prodCmpCode to set
     */
    public final void setProdCmpCode(final String prodCmpCodeIn) {
        prodCmpCode = prodCmpCodeIn;
    }

    /**
     * @return the stkQty
     */
    public final Double getStkQty() {
        return stkQty;
    }

    /**
     * @param stkQtyIn the stkQty to set
     */
    public final void setStkQty(final Double stkQtyIn) {
        stkQty = stkQtyIn;
    }

    /**
     * @return the id
     */
    public final String getId() {
        return id;
    }

    /**
     * @param idIn the id to set
     */
    public final void setId(final String idIn) {
        id = idIn;
    }

    /**
     * @return the title
     */
    public final String getTitle() {
        return title;
    }

    /**
     * @param titleIn the title to set
     */
    public final void setTitle(final String titleIn) {
        title = titleIn;
    }

    /**
     * @return the description
     */
    public final String getDescription() {
        return description;
    }

    /**
     * @param descriptionIn the description to set
     */
    public final void setDescription(final String descriptionIn) {
        description = descriptionIn;
    }

    /**
     * @return the type
     */
    public final String getType() {
        return type;
    }

    /**
     * @param typeIn the type to set
     */
    public final void setType(final String typeIn) {
        type = typeIn;
    }

    /**
     * @return the brand
     */
    public final String getBrand() {
        return brand;
    }

    /**
     * @param brandIn the brand to set
     */
    public final void setBrand(final String brandIn) {
        brand = brandIn;
    }

    /**
     * @return the category
     */
    public final String getCategory() {
        return category;
    }

    /**
     * @param categoryIn the category to set
     */
    public final void setCategory(final String categoryIn) {
        category = categoryIn;
    }

    /**
     * @return the price
     */
    public final Double getPrice() {
        return price;
    }

    /**
     * @param priceIn the price to set
     */
    public final void setPrice(final Double priceIn) {
        price = priceIn;
    }

    /**
     * @return the sale
     */
    public final boolean isSale() {
        return sale;
    }

    /**
     * @param saleIn the sale to set
     */
    public final void setSale(final boolean saleIn) {
        sale = saleIn;
    }

    /**
     * @return the discount
     */
    public final Double getDiscount() {
        return discount;
    }

    /**
     * @param discountIn the discount to set
     */
    public final void setDiscount(final Double discountIn) {
        discount = discountIn;
    }

    /**
     * @return the stock
     */
    public final Double getStock() {
        return stock;
    }

    /**
     * @param stockIn the stock to set
     */
    public final void setStock(final Double stockIn) {
        stock = stockIn;
    }

    /**
     * @return the newProduct
     */
    public final boolean isNewProduct() {
        return newProduct;
    }

    /**
     * @param newProductIn the newProduct to set
     */
    public final void setNewProduct(final boolean newProductIn) {
        newProduct = newProductIn;
    }

    /**
     * @return the quantity
     */
    public final Double getQuantity() {
        return quantity;
    }

    /**
     * @param quantityIn the quantity to set
     */
    public final void setQuantity(final Double quantityIn) {
        quantity = quantityIn;
    }

    /**
     * @return the cmpCode
     */
    public final String getCmpCode() {
        return cmpCode;
    }

    /**
     * @param cmpCodeIn the cmpCode to set
     */
    public final void setCmpCode(final String cmpCodeIn) {
        cmpCode = cmpCodeIn;
    }

    /**
     * @return the prodWeightType
     */
    public final String getProdWeightType() {
        return prodWeightType;
    }

    /**
     * @param prodWeightTypeIn the prodWeightType to set
     */
    public final void setProdWeightType(final String prodWeightTypeIn) {
        prodWeightType = prodWeightTypeIn;
    }

    /**
     * @return the prodWeight
     */
    public final Double getProdWeight() {
        return prodWeight;
    }

    /**
     * @param prodWeightIn the prodWeight to set
     */
    public final void setProdWeight(final Double prodWeightIn) {
        prodWeight = prodWeightIn;
    }


    /**
     * @return the alt
     */
    public final String getAlt() {
        return alt;
    }

    /**
     * @param altIn the alt to set
     */
    public final void setAlt(final String altIn) {
        alt = altIn;
    }

    /**
     * @return the src
     */
    public final String getSrc() {
        return src;
    }

    /**
     * @param srcIn the src to set
     */
    public final void setSrc(final String srcIn) {
        src = srcIn;
    }

    /**
     * @return the size
     */
    public final String getSize() {
        return size;
    }

    /**
     * @param sizeIn the size to set
     */
    public final void setSize(final String sizeIn) {
        size = sizeIn;
    }

    /**
     * @return the color
     */
    public final String getColor() {
        return color;
    }

    /**
     * @param colorIn the color to set
     */
    public final void setColor(final String colorIn) {
        color = colorIn;
    }


    public Integer getProdCatalogId() {
        return prodCatalogId;
    }

    public void setProdCatalogId(Integer prodCatalogId) {
        this.prodCatalogId = prodCatalogId;
    }
}
