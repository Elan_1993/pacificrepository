package com.pacific.common.dto;

public class FileInfoDTO {

    private String name;
    private String url;

    public FileInfoDTO(String name, String url) {
        this.name = name;
        this.url = url;
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * @param nameIn the name to set
     */
    public final void setName(final String nameIn) {
        name = nameIn;
    }

    /**
     * @return the url
     */
    public final String getUrl() {
        return url;
    }

    /**
     * @param urlIn the url to set
     */
    public final void setUrl(final String urlIn) {
        url = urlIn;
    }
}
