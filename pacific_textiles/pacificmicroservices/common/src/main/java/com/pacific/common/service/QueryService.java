package com.pacific.common.service;


import com.pacific.common.dto.QueriesDTO;
import com.pacific.common.util.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class QueryService implements IQueryService{

    /**
     * queryMap.
     */
    private final Map<String, String> queryMap = new HashMap<>();

    /**
     * queryLocation.
     */
    @Value("${query.location}")
    private String queryLocation;

    /**
     * This method is used to load all queries from xml.
     * @throws IOException if any i/o exception
     */
    @PostConstruct
    public final void init() throws IOException {
        // load common queries
        var indraCartCommonQueries = new File(queryLocation + File.separator + "pacific-queries.xml");
        loadQueries(indraCartCommonQueries);
        // load client specific query
        var clientQueryPath = new File(queryLocation + File.separator + "query");
        for (var file : Objects.requireNonNull(clientQueryPath.listFiles(f -> f.getName().endsWith(".xml")))) {
            loadQueries(file);
        }
    }

    /**
     * This method is used to load SQL queries from external file.
     * @param file Query File
     * @throws IOException if any i/o exception
     */
    private void loadQueries(File file) throws IOException {
        var xmlStr = Files.readString(file.toPath());
        var queries = (QueriesDTO) Function.fromXML(xmlStr, QueriesDTO.class);
        queries.getQueries().forEach(q -> queryMap.put(q.getName(), q.getQuery()));
    }

    /**
     * This method is used to get SQL query by name.
     * @param queryName Query name
     * @return SQL query
     */
    @Override
    public final String get(final String queryName) {
        return queryMap.get(queryName);
    }       
}
