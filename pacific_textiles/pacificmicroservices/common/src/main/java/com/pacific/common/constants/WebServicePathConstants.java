package com.pacific.common.constants;


/**
 * Class contains the web service path constant value.
 *
 * @author elan
 */
public final class WebServicePathConstants {

    /** SAVE_USER. */
    public static final String SAVE_USER = "/save-user";

    /** GET_USER_BY_ID.*/
    public static final String GET_USER_BY_ID = "/get-user/";

    /** FETCH_ALL_USER.*/
    public static final String FETCH_ALL_USER = "/fetch-all-user";

    /** URL_ACCESS_LOGIN. */
    public static final String URL_ACCESS_LOGIN = "/login";

    /** URL_ACCESS_ERROR. */
    public static final String URL_ACCESS_ERROR = "/error";
}
