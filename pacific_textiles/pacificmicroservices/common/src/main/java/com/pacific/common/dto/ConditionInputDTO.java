package com.pacific.common.dto;

import java.io.Serializable;

/**
 * This pojo class is used to hold report input parameters.
 * @author Elan
 */
public class ConditionInputDTO implements Serializable {

    /** serialVersionUID. **/
    private static final long serialVersionUID = 1L;

    /** category. */
    private String category;

    /** prodCatalogId. */
    private Integer prodCatalogId;

    /** prodExcludeCode. */
    private String prodExcludeCode;

    /** stkQtyNonZeroEnable. */
    private boolean stkQtyNonZeroEnable;

    /**
     * @return the category
     */
    public final String getCategory() {
        return category;
    }

    /**
     * @param categoryIn the category to set
     */
    public final void setCategory(final String categoryIn) {
        category = categoryIn;
    }


    /**
     * @return the prodCatalogId
     */
    public final Integer getProdCatalogId() {
        return prodCatalogId;
    }

    /**
     * @param prodCatalogIdIn the prodCatalogId to set
     */
    public final void setProdCatalogId(final Integer prodCatalogIdIn) {
        prodCatalogId = prodCatalogIdIn;
    }

    /**
     * @return the prodExcludeCode
     */
    public final String getProdExcludeCode() {
        return prodExcludeCode;
    }

    /**
     * @param prodExcludeCodeIn the prodExcludeCode to set
     */
    public final void setProdExcludeCode(final String prodExcludeCodeIn) {
        prodExcludeCode = prodExcludeCodeIn;
    }


    /**
     * @return the stkQtyNonZeroEnable
     */
    public final boolean isStkQtyNonZeroEnable() {
        return stkQtyNonZeroEnable;
    }

    /**
     * @param stkQtyNonZeroEnableIn the stkQtyNonZeroEnable to set
     */
    public final void setStkQtyNonZeroEnable(final boolean stkQtyNonZeroEnableIn) {
        stkQtyNonZeroEnable = stkQtyNonZeroEnableIn;
    }
}
