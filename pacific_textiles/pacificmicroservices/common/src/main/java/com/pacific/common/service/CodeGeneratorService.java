package com.pacific.common.service;

import com.pacific.repository.repo.AutoNumberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class CodeGeneratorService implements ICodeGeneratorService {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CodeGeneratorService.class);

    @Autowired
    private AutoNumberRepository autoNumberRepository;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public String generateCode(String autoCategory) {
        var autonumber = autoNumberRepository.findAutoNumberByAutoCategory(autoCategory);
        var sequenceNumber = autonumber.getAutoPrefix().concat(String.valueOf(autonumber.getAutoSeqNumber()));
        autonumber.setAutoSeqNumber(autonumber.getAutoSeqNumber() + 1);
        autoNumberRepository.save(autonumber);
        return sequenceNumber;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Integer generateSequence(String autoCategory) {
        var autonumber = autoNumberRepository.findAutoNumberByAutoCategory(autoCategory);
        var sequenceNumber = autonumber.getAutoSeqNumber();
        autonumber.setAutoSeqNumber(autonumber.getAutoSeqNumber() + 1);
        autoNumberRepository.save(autonumber);
        return sequenceNumber;
    }


}
