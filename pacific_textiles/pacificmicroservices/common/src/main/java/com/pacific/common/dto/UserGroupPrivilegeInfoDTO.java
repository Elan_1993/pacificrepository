package com.pacific.common.dto;

import java.util.Date;

/**
 * This class is used to hold user group privilege.
 * @author elan
 */
public class UserGroupPrivilegeInfoDTO {

    /**
     * groupCode.
     */
    private String groupCode;
    /**
     * moduleNo.
     */
    private int moduleNo;
    /**
     * screenNo.
     */
    private int screenNo;
    /**
     * screenName.
     */
    private String screenName;
    /**
     * moduleName.
     */
    private String moduleName;
    /**
     * isCreate.
     */
    private boolean createAccess;
    /**
     * isEdit.
     */
    private boolean editAccess;
    /**
     * isView.
     */
    private boolean viewAccess;
    /**
     * isDelete.
     */
    private boolean deleteAccess;

    /** lastModBy. */
    private String lastModBy;

    /** lastModTime. */
    private Date lastModTime;

    /**
     * @return the screenNo
     */
    public final int getScreenNo() {
        return screenNo;
    }

    /**
     * @param screenNoIn the screenNo to set
     */
    public final void setScreenNo(final int screenNoIn) {
        screenNo = screenNoIn;
    }

    /**
     * @return the createAccess
     */
    public final boolean isCreateAccess() {
        return createAccess;
    }

    /**
     * @param createAccessIn the createAccess to set
     */
    public final void setCreateAccess(final boolean createAccessIn) {
        createAccess = createAccessIn;
    }

    /**
     * @return the editAccess
     */
    public final boolean isEditAccess() {
        return editAccess;
    }

    /**
     * @param editAccessIn the editAccess to set
     */
    public final void setEditAccess(final boolean editAccessIn) {
        editAccess = editAccessIn;
    }

    /**
     * @return the viewAccess
     */
    public final boolean isViewAccess() {
        return viewAccess;
    }

    /**
     * @param viewAccessIn the viewAccess to set
     */
    public final void setViewAccess(final boolean viewAccessIn) {
        viewAccess = viewAccessIn;
    }

    /**
     * @return the deleteAccess
     */
    public final boolean isDeleteAccess() {
        return deleteAccess;
    }

    /**
     * @param deleteAccessIn the deleteAccess to set
     */
    public final void setDeleteAccess(final boolean deleteAccessIn) {
        deleteAccess = deleteAccessIn;
    }

    /**
     * @return the moduleNo
     */
    public final int getModuleNo() {
        return moduleNo;
    }

    /**
     * @param moduleNoIn the moduleNo to set
     */
    public final void setModuleNo(final int moduleNoIn) {
        moduleNo = moduleNoIn;
    }

    /**
     * @return the screenName
     */
    public final String getScreenName() {
        return screenName;
    }

    /**
     * @param screenNameIn the screenName to set
     */
    public final void setScreenName(final String screenNameIn) {
        screenName = screenNameIn;
    }

    /**
     * @return the groupCode
     */
    public final String getGroupCode() {
        return groupCode;
    }

    /**
     * @param groupCodeIn the groupCode to set
     */
    public final void setGroupCode(final String groupCodeIn) {
        groupCode = groupCodeIn;
    }

    /**
     * @return the moduleName
     */
    public final String getModuleName() {
        return moduleName;
    }

    /**
     * @param moduleNameIn the moduleName to set
     */
    public final void setModuleName(final String moduleNameIn) {
        moduleName = moduleNameIn;
    }

    /**
     * @return the lastModBy
     */
    public final String getLastModBy() {
        return lastModBy;
    }

    /**
     * @param lastModByIn the lastModBy to set
     */
    public final void setLastModBy(final String lastModByIn) {
        lastModBy = lastModByIn;
    }

    /**
     * @return the lastModTime
     */
    public final Date getLastModTime() {
        return lastModTime;
    }

    /**
     * @param lastModTimeIn the lastModTime to set
     */
    public final void setLastModTime(final Date lastModTimeIn) {
        lastModTime = lastModTimeIn;
    }
}
