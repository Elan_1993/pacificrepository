package com.pacific.common.dto;
/**
 * This pojo class is used to hold user login credentials.
 * @author elan.
 */
public class LoginDTO {

    /** userCode. */
    private String userCode;

    /** password. */
    private String password;

    /** isCompanyUser. */
    private Boolean companyUser;

    /** isCompanyUserMenu. */
    private Boolean companyUserMenu;

    /** is

    /**
     * @return the userCode
     */
    public final String getUserCode() {
        return userCode;
    }

    /**
     * @param userCodeIn the userCode to set
     */
    public final void setUserCode(final String userCodeIn) {
        userCode = userCodeIn;
    }

    /**
     * @return the password
     */
    public final String getPassword() {
        return password;
    }

    /**
     * @param passwordIn the password to set
     */
    public final void setPassword(final String passwordIn) {
        password = passwordIn;
    }

    /**
     * @return the companyUser
     */
    public final Boolean getCompanyUser() {
        return companyUser;
    }

    /**
     * @param companyUserIn the companyUser to set
     */
    public final void setCompanyUser(final Boolean companyUserIn) {
        companyUser = companyUserIn;
    }

    /**
     * @return the companyUserMenu
     */
    public final Boolean getCompanyUserMenu() {
        return companyUserMenu;
    }

    /**
     * @param companyUserMenuIn the companyUserMenu to set
     */
    public final void setCompanyUserMenu(final Boolean companyUserMenuIn) {
        companyUserMenu = companyUserMenuIn;
    }
}
