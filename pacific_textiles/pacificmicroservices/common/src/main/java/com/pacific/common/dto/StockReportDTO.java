package com.pacific.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is used to get Stock Report object.
 */
public class StockReportDTO implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /*stkProdCode.*/
    private String stkProdCode;

    /*prodName.*/
    private String prodName;

    /*stkRate.*/
    private Double stkRate;

    /*stkMrp.*/
    private Double stkMrp;

    /*stkQty.*/
    private Double stkQty;

    /**
     * @return the stkProdCode
     */
    public final String getStkProdCode() {
        return stkProdCode;
    }

    /**
     * @param stkProdCodeIn the stkProdCode to set
     */
    public final void setStkProdCode(String stkProdCodeIn) {
        stkProdCode = stkProdCodeIn;
    }

    /**
     * @return the prodName
     */
    public final String getProdName() {
        return prodName;
    }

    /**
     * @param prodNameIn the prodName to set
     */
    public final void setProdName(String prodNameIn) {
        prodName = prodNameIn;
    }

    /**
     * @return the stkRate
     */
    public final Double getStkRate() {
        return stkRate;
    }

    /**
     * @param stkRateIn the stkRate to set
     */
    public final void setStkRate(Double stkRateIn) {
        stkRate = stkRateIn;
    }

    /**
     * @return the stkMrp
     */
    public final Double getStkMrp() {
        return stkMrp;
    }

    /**
     * @param stkMrpIn the stkMrp to set
     */
    public final void setStkMrp(Double stkMrpIn) {
        stkMrp = stkMrpIn;
    }

    /**
     * @return the stkQty
     */
    public final Double getStkQty() {
        return stkQty;
    }

    /**
     * @param stkQtyIn the stkQty to set
     */
    public final void setStkQty(Double stkQtyIn) {
        stkQty = stkQtyIn;
    }
}
