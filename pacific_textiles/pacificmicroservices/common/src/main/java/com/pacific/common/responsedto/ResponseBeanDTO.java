package com.pacific.common.responsedto;

import java.io.Serializable;
import java.util.List;

public class ResponseBeanDTO<T> extends BaseResponseDTO implements Serializable {

    public ResponseBeanDTO() {
    }

    public ResponseBeanDTO(T object, int responseCode, String responseMessage) {
        super(responseMessage);
        this.object = object;
        this.responseCode = responseCode;
    }

    public ResponseBeanDTO(T object) {
        super("Success");
        this.object = object;
        this.responseCode = 0;
    }

    public ResponseBeanDTO(List<T> listObject) {
        super("Success");
        this.listObject = listObject;
        this.responseCode = 0;
    }

    private T object;
    private List<T> listObject;
    private int responseCode;


    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public List<T> getListObject() {
        return listObject;
    }

    public void setListObject(List<T> listObject) {
        this.listObject = listObject;
    }
}
