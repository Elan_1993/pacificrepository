package com.pacific.common.responsedto;


public class BaseResponseDTO {
    private String message;

    public BaseResponseDTO() {
    }

    public BaseResponseDTO(String message) {
        this.message = message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
