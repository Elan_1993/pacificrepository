package com.pacific.common.dto;

public class Product {

    private String  id;
    private String title;
    private String description;
    private String  type;
    private String brand;
    private String category;
    private Double  price;
    private boolean sale =true;
    private Double discount;
    private Double stock;
    private boolean newProduct = false;
    private Double quantity;
    private String  cmpCode;


    /**
     * @return the id
     */
    public final String getId() {
        return id;
    }

    /**
     * @param idIn the id to set
     */
    public final void setId(final String idIn) {
        id = idIn;
    }

    /**
     * @return the title
     */
    public final String getTitle() {
        return title;
    }

    /**
     * @param titleIn the title to set
     */
    public final void setTitle(final String titleIn) {
        title = titleIn;
    }

    /**
     * @return the description
     */
    public final String getDescription() {
        return description;
    }

    /**
     * @param descriptionIn the description to set
     */
    public final void setDescription(final String descriptionIn) {
        description = descriptionIn;
    }

    /**
     * @return the type
     */
    public final String getType() {
        return type;
    }

    /**
     * @param typeIn the type to set
     */
    public final void setType(final String typeIn) {
        type = typeIn;
    }

    /**
     * @return the brand
     */
    public final String getBrand() {
        return brand;
    }

    /**
     * @param brandIn the brand to set
     */
    public final void setBrand(final String brandIn) {
        brand = brandIn;
    }

    /**
     * @return the category
     */
    public final String getCategory() {
        return category;
    }

    /**
     * @param categoryIn the category to set
     */
    public final void setCategory(final String categoryIn) {
        category = categoryIn;
    }

    /**
     * @return the price
     */
    public final Double getPrice() {
        return price;
    }

    /**
     * @param priceIn the price to set
     */
    public final void setPrice(final Double priceIn) {
        price = priceIn;
    }

    /**
     * @return the sale
     */
    public final boolean isSale() {
        return sale;
    }

    /**
     * @param saleIn the sale to set
     */
    public final void setSale(final boolean saleIn) {
        sale = saleIn;
    }

    /**
     * @return the discount
     */
    public final Double getDiscount() {
        return discount;
    }

    /**
     * @param discountIn the discount to set
     */
    public final void setDiscount(final Double discountIn) {
        discount = discountIn;
    }

    /**
     * @return the stock
     */
    public final Double getStock() {
        return stock;
    }

    /**
     * @param stockIn the stock to set
     */
    public final void setStock(final Double stockIn) {
        stock = stockIn;
    }

    /**
     * @return the newProduct
     */
    public final boolean isNewProduct() {
        return newProduct;
    }

    /**
     * @param newProductIn the newProduct to set
     */
    public final void setNewProduct(final boolean newProductIn) {
        newProduct = newProductIn;
    }

    /**
     * @return the quantity
     */
    public final Double getQuantity() {
        return quantity;
    }

    /**
     * @param quantityIn the quantity to set
     */
    public final void setQuantity(final Double quantityIn) {
        quantity = quantityIn;
    }

    /**
     * @return the cmpCode
     */
    public final String getCmpCode() {
        return cmpCode;
    }

    /**
     * @param cmpCodeIn the cmpCode to set
     */
    public final void setCmpCode(String cmpCodeIn) {
        cmpCode = cmpCodeIn;
    }
}
