package com.pacific.common.dto;

import java.util.Date;
import java.util.List;

/**
 * This POJO is used to hold user group information.
 * @author elan
 */
public class UserGroupDTO {

    /** groupCode. */
    private String groupCode;

    /** groupName. */
    private String groupName;

    /** createdBy. */
    private String createdBy;

    /** createdTime. */
    private Date createdTime;

    /** lastModBy. */
    private String lastModBy;

    /** lastModTime. */
    private Date lastModTime;

    /** privileges. */
    private List<UserGroupPrivilegeInfoDTO> privileges;

    /**
     * @return the groupCode
     */
    public final String getGroupCode() {
        return groupCode;
    }

    /**
     * @param groupCodeIn the groupCode to set
     */
    public final void setGroupCode(final String groupCodeIn) {
        groupCode = groupCodeIn;
    }

    /**
     * @return the groupName
     */
    public final String getGroupName() {
        return groupName;
    }

    /**
     * @param groupNameIn the groupName to set
     */
    public final void setGroupName(final String groupNameIn) {
        groupName = groupNameIn;
    }

    /**
     * @return the createdBy
     */
    public final String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdByIn the createdBy to set
     */
    public final void setCreatedBy(final String createdByIn) {
        createdBy = createdByIn;
    }

    /**
     * @return the createdTime
     */
    public final Date getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTimeIn the createdTime to set
     */
    public final void setCreatedTime(final Date createdTimeIn) {
        createdTime = createdTimeIn;
    }

    /**
     * @return the lastModBy
     */
    public final String getLastModBy() {
        return lastModBy;
    }

    /**
     * @param lastModByIn the lastModBy to set
     */
    public final void setLastModBy(final String lastModByIn) {
        lastModBy = lastModByIn;
    }

    /**
     * @return the lastModTime
     */
    public final Date getLastModTime() {
        return lastModTime;
    }

    /**
     * @param lastModTimeIn the lastModTime to set
     */
    public final void setLastModTime(final Date lastModTimeIn) {
        lastModTime = lastModTimeIn;
    }

    /**
     * @return the privileges
     */
    public final List<UserGroupPrivilegeInfoDTO> getPrivileges() {
        return privileges;
    }

    /**
     * @param privilegesIn the privileges to set
     */
    public final void setPrivileges(final List<UserGroupPrivilegeInfoDTO> privilegesIn) {
        privileges = privilegesIn;
    }
}
