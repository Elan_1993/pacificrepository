package com.pacific.common;

import com.pacific.common.service.IQueryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
class CommonApplicationTests {

	/** queryService. */
	@Autowired
	private IQueryService queryService;

	/**
	 * This method is to test application context loaded properly.
	 */
	@Test
	public void contextLoads() {
		try {
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
	}

	/**
	 * Test method for QueryService.
	 */
	@Test
	public void testQueryService() {
		var query = queryService.get("USER_INFO");
		Assertions.assertNotNull(query);
	}

	/**
	 * Configuration method.
	 */
	@SpringBootApplication
	static class TestConfiguration {
	}

}
