package com.pacific.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "order_h")
public class OrderH {
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	private String orderNo;
	private String orderCusCode;
	private Double orderGrossAmount;
	private Double orderDiscountAmount;
	private Double orderTaxAmount;
	private Double orderShipAmount;
	private Double orderAmount;
	private Double orderQty;
	private Integer orderState;
	private String orderStatus;
	private String orderExpectedCourier;
	private String orderActualCourier;
	private String orderRpayNo;
	private String orderRpayStatus;
	private String orderRpayPaymentNo;
	private String orderRpayPaymentStatus;
	private String orderRpaySignature;
	private Integer orderAddressId;
	private String orderAddress;
	private Integer orderStateId;
	private String orderStateName;
	private String orderTrackingId;
	private Date createdDatetime;
	private Date modifiedDatetime;
	private String customerName;
	private String customerMobile;
	private Set<OrderD> orderDs = new HashSet<OrderD>(0);

	public OrderH() {
	}

	public OrderH(String orderNo, String orderCusCode) {
		this.orderNo = orderNo;
		this.orderCusCode = orderCusCode;
	}

	public OrderH(String orderNo, String orderCusCode, Double orderGrossAmount, Double orderTaxAmount, Double orderDiscountAmount, Double orderShipAmount, Double orderAmount, Double orderQty, Integer orderState, String orderStatus, String orderExpectedCourier , String orderActualCourier, String orderRpayNo , String orderRpayStatus , String orderRpayPaymentNo , String orderRpayPaymentStatus,String orderRpaySignature ,Integer orderAddressId, String orderAddress , Integer orderStateId, String orderStateName ,String orderTrackingId , Date createdDatetime, Date modifiedDatetime, Set<OrderD> orderDs) {
		this.orderNo = orderNo;
		this.orderCusCode = orderCusCode;
		this.orderAmount = orderGrossAmount;
		this.orderAmount = orderTaxAmount;
		this.orderAmount = orderDiscountAmount;
		this.orderShipAmount = orderShipAmount;
		this.orderAmount = orderAmount;
		this.orderQty = orderQty;
		this.orderState = orderState;
		this.orderStatus = orderStatus;
		this.orderActualCourier = orderActualCourier;
		this.orderExpectedCourier = orderExpectedCourier;
		this.orderRpayNo = orderRpayNo;
		this.orderRpayStatus = orderRpayStatus;
		this.orderRpayPaymentNo = orderRpayPaymentNo;
		this.orderRpayPaymentStatus = orderRpayPaymentStatus;
		this.orderRpaySignature = orderRpaySignature;
		this.orderAddressId = orderAddressId;
		this.orderAddress = orderAddress;
		this.orderStateId = orderStateId;
		this.orderStateName = orderStateName;
		this.orderTrackingId = orderTrackingId;
		this.createdDatetime = createdDatetime;
		this.modifiedDatetime = modifiedDatetime;
		this.orderDs = orderDs;
	}

	@Id
	@Column(name="order_no", unique=true, nullable=false, length=50)
	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="order_cus_code", nullable=false, length=50)
	public String getOrderCusCode() {
		return this.orderCusCode;
	}

	public void setOrderCusCode(String orderCusCode) {
		this.orderCusCode = orderCusCode;
	}

	@Column(name="order_gross_amount", precision=22, scale=6)
	public Double getOrderGrossAmount() {
		return this.orderGrossAmount;
	}
	public void setOrderGrossAmount(Double orderGrossAmount) {
		this.orderGrossAmount = orderGrossAmount;
	}

	@Column(name="order_disc_amount", precision=22, scale=6)
	public Double getOrderDiscountAmount() {
		return this.orderDiscountAmount;
	}
	public void setOrderDiscountAmount(Double orderDiscountAmount) {
		this.orderDiscountAmount = orderDiscountAmount;
	}

	@Column(name="order_tax_amount", precision=22, scale=6)
	public Double getOrderTaxAmount() {
		return this.orderTaxAmount;
	}
	public void setOrderTaxAmount(Double orderTaxAmount) {
		this.orderTaxAmount = orderTaxAmount;
	}

	@Column(name="order_ship_amount", precision=22, scale=6)
	public Double getOrderShipAmount() {
		return this.orderShipAmount;
	}
	public void setOrderShipAmount(Double orderShipAmount) {
		this.orderShipAmount = orderShipAmount;
	}

	@Column(name="order_amount", precision=22, scale=6)
	public Double getOrderAmount() {
		return this.orderAmount;
	}

	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}

	@Column(name="order_qty", precision=22, scale=6)
	public Double getOrderQty() {
		return this.orderQty;
	}

	public void setOrderQty(Double orderQty) {
		this.orderQty = orderQty;
	}

	@Column(name="order_state")
	public Integer getOrderState() {
		return this.orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	@Column(name="order_status", length=50)
	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Column(name="order_expected_courier", length=100)
	public String getOrderExpectedCourier() {
		return this.orderExpectedCourier;
	}

	public void setOrderExpectedCourier(String orderExpectedCourier) {
		this.orderExpectedCourier = orderExpectedCourier;
	}

	@Column(name="order_actual_courier", length=100)
	public String getOrderActualCourier() {
		return this.orderActualCourier;
	}

	public void setOrderActualCourier(String orderActualCourier) {
		this.orderActualCourier = orderActualCourier;
	}

	@Column(name="order_rpay_no", length=50)
	public String getOrderRpayNo(){return this.orderRpayNo;	}

	public void setOrderRpayNo(String orderRpayNo) {
		this.orderRpayNo = orderRpayNo;
	}

	@Column(name="order_rpay_status", length=50)
	public String getOrderRpayStatus() {
		return this.orderRpayStatus;
	}

	public void setOrderRpayStatus(String orderRpayStatus) {
		this.orderRpayStatus = orderRpayStatus;
	}

	@Column(name="order_rpay_payment_no", length=50)
	public String getOrderRpayPaymentNo() {
		return this.orderRpayPaymentNo;
	}

	public void setOrderRpayPaymentNo(String orderRpayPaymentNo) {
		this.orderRpayPaymentNo = orderRpayPaymentNo;
	}

	@Column(name="order_rpay_payment_status", length=50)
	public String getOrderRpayPaymentStatus() {
		return this.orderRpayPaymentStatus;
	}

	public void setOrderRpayPaymentStatus(String orderRpayPaymentStatus) {
		this.orderRpayPaymentStatus = orderRpayPaymentStatus;
	}

	@Column(name="order_rpay_signature", length=50)
	public String getOrderRpaySignature() {
		return this.orderRpaySignature;
	}

	public void setOrderRpaySignature(String orderRpaySignature) {
		this.orderRpaySignature = orderRpaySignature;
	}

	@Column(name="order_address_id")
	public Integer getOrderAddressId() {
		return this.orderAddressId;
	}

	public void setOrderAddressId(Integer orderAddressId) {
		this.orderAddressId = orderAddressId;
	}

	@Column(name="order_address", length=500)
	public String getOrderAddress() {
		return this.orderAddress;
	}

	public void setOrderAddress(String orderAddress) {
		this.orderAddress = orderAddress;
	}

	@Column(name="order_state_id")
	public Integer getOrderStateId() {return orderStateId;}

	public void setOrderStateId(Integer orderStateId) {this.orderStateId = orderStateId;}

	@Column(name="order_state_name" , length = 100)
	public String getOrderStateName() {	return orderStateName;}

	public void setOrderStateName(String orderStateName) {this.orderStateName = orderStateName;	}

	public void setOrderTrackingId(String orderTrackingId) {
		this.orderTrackingId = orderTrackingId;
	}

	@Column(name="order_tracking_id",  length=50)
	public String getOrderTrackingId() {return this.orderTrackingId;}

	@Column(name = "created_datetime",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Column(name = "modified_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifiedDatetime() {
		return this.modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="orderH")
	public Set<OrderD> getOrderDs() {
		return this.orderDs;
	}

	public void setOrderDs(Set<OrderD> orderDs) {
		this.orderDs = orderDs;
	}

	@Transient
	public String getCustomerName() {return customerName;}

	public void setCustomerName(String customerName) {this.customerName = customerName;}

	@Transient
	public String getCustomerMobile() {return customerMobile;}

	public void setCustomerMobile(String customerMobile) {this.customerMobile = customerMobile;}
}
