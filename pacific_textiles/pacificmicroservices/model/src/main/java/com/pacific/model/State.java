package com.pacific.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "state_t")
public class State {
    private int stateId;
    private String moduleName;
    private Boolean istransactionSuccess;
    private int currentState;
    private String currentStatus;
    private int nextState;
    private String nextStatus;

    @Id
    @Column(name = "state_id")
    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    @Column(name = "module_name")
    public String getModuleName() {
        return moduleName;
    }

    @Column(name = "is_transaction_success")
    public Boolean getIstransactionSuccess() {return istransactionSuccess;}
    public void setIstransactionSuccess(Boolean istransactionSuccess) {this.istransactionSuccess = istransactionSuccess;    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
    @Column(name = "current_state")
    public int getCurrentState() {
        return currentState;
    }

    public void setCurrentState(int currentState) {
        this.currentState = currentState;
    }
    @Column(name = "current_status")
    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
    @Column(name = "next_state")
    public int getNextState() {
        return nextState;
    }

    public void setNextState(int nextState) {
        this.nextState = nextState;
    }
    @Column(name = "next_status")
    public String getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(String nextStatus) {
        this.nextStatus = nextStatus;
    }
}
