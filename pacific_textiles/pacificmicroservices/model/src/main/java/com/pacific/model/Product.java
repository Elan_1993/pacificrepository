package com.pacific.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {

	private ProductId id;
	private String prodName;
	private Double prodPrice;
	private Double prodDiscount;
	private String prodGender;
	private String prodCategory;
	private String prodMaterial;
	private String prodBrand;
	private String prodDescription;
	private Boolean prodIsActive;
	private String prodSize;
	private String prodColor;
	private Integer prodCatalogId;
	private String prodWeightType;
	private Double prodWeight;
	private String createdBy;
	private String modifiedBy;
	private Date createdDatetime;
	private Date modifiedDatetime;
	private Double stkQty;
	private boolean updateCheck;

	public Product() {
	}

	public Product(ProductId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "prodCode", column = @Column(name = "prod_code", nullable = false, length = 50)),
			@AttributeOverride(name = "prodCmpCode", column = @Column(name = "prod_cmp_code", nullable = false, length = 100)) })
	public ProductId getId() {
		return this.id;
	}

	public void setId(ProductId id) {
		this.id = id;
	}

	@Column(name = "prod_name", length = 200)
	public String getProdName() {
		return this.prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	@Column(name = "prod_weight_type", length = 20)
	public String getProdWeightType() {
		return prodWeightType;
	}

	public void setProdWeightType(String prodWeightType) {
		this.prodWeightType = prodWeightType;
	}

	@Column(name = "prod_weight", length = 200)
	public Double getProdWeight() {
		return prodWeight;
	}

	public void setProdWeight(Double prodWeight) {
		this.prodWeight = prodWeight;
	}

	@Column(name = "prod_price", precision = 22, scale = 6)
	public Double getProdPrice() {
		return this.prodPrice;
	}

	public void setProdPrice(Double prodPrice) {
		this.prodPrice = prodPrice;
	}

	@Column(name = "prod_discount", precision = 22, scale = 6)
	public Double getProdDiscount() {
		return this.prodDiscount;
	}

	public void setProdDiscount(Double prodDiscount) {
		this.prodDiscount = prodDiscount;
	}

	@Column(name = "prod_gender", length = 20)
	public String getProdGender() {
		return this.prodGender;
	}

	public void setProdGender(String prodGender) {
		this.prodGender = prodGender;
	}

	@Column(name = "prod_category", length = 100)
	public String getProdCategory() {
		return this.prodCategory;
	}

	public void setProdCategory(String prodCategory) {
		this.prodCategory = prodCategory;
	}

	@Column(name = "prod_material", length = 100)
	public String getProdMaterial() {
		return this.prodMaterial;
	}

	public void setProdMaterial(String prodMaterial) {
		this.prodMaterial = prodMaterial;
	}

	@Column(name = "prod_brand", length = 100)
	public String getProdBrand() {
		return this.prodBrand;
	}

	public void setProdBrand(String prodBrand) {
		this.prodBrand = prodBrand;
	}

	@Column(name = "prod_description", length = 200)
	public String getProdDescription() {
		return this.prodDescription;
	}

	public void setProdDescription(String prodDescription) {
		this.prodDescription = prodDescription;
	}

	@Column(name = "prod_is_active")
	public Boolean getProdIsActive() {
		return this.prodIsActive;
	}

	public void setProdIsActive(Boolean prodIsActive) {
		this.prodIsActive = prodIsActive;
	}

	@Column(name = "prod_size", length = 50)
	public String getProdSize() {
		return this.prodSize;
	}

	public void setProdSize(String prodSize) {
		this.prodSize = prodSize;
	}

	@Column(name = "prod_color", length = 50)
	public String getProdColor() {
		return this.prodColor;
	}

	public void setProdColor(String prodColor) {
		this.prodColor = prodColor;
	}

	@Column(name = "prod_catalog_id")
	public Integer getProdCatalogId() {
		return this.prodCatalogId;
	}

	public void setProdCatalogId(Integer prodCatalogId) {
		this.prodCatalogId = prodCatalogId;
	}

	@Column(name = "createdby", updatable = false)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "modifiedby")
	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_datetime", updatable = false, length = 19)
	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_datetime", length = 19)
	public Date getModifiedDatetime() {
		return this.modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	@Transient
	public Double getStkQty() {
		return stkQty;
	}

	public void setStkQty(Double stkQty) {
		this.stkQty = stkQty;
	}

	@Transient
	public boolean isUpdateCheck() {
		return updateCheck;
	}

	public void setUpdateCheck(boolean updateCheck) {
		this.updateCheck = updateCheck;
	}

}
