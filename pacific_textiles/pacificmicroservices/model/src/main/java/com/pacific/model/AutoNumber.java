package com.pacific.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "autonumber_d")
public class AutoNumber implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private int autoRid;
    private String autoPrefix;
    private String autoSuffix;
    private int autoSeqNumber;
    private String autoFinYear;
    private String autoCmpCode;
    private String autoCategory;

    public AutoNumber() {

    }

    public AutoNumber(int autoRid) {
        this.autoRid = autoRid;
    }

    @Id
    @Column(name = "auto_rid", unique = true, nullable = false, length = 11)
    public int getAutoRid() {
        return autoRid;
    }

    public void setAutoRid(int autoRid) {
        this.autoRid = autoRid;
    }

    @Column(name = "auto_prefix", length = 100)
    public String getAutoPrefix() {
        return autoPrefix;
    }

    public void setAutoPrefix(String autoPrefix) {
        this.autoPrefix = autoPrefix;
    }

    @Column(name = "auto_suffix", length = 100)
    public String getAutoSuffix() {
        return autoSuffix;
    }

    public void setAutoSuffix(String autoSuffix) {
        this.autoSuffix = autoSuffix;
    }

    @Column(name = "auto_sequence_number", length = 100)
    public int getAutoSeqNumber() {
        return autoSeqNumber;
    }

    public void setAutoSeqNumber(int autoSeqNumber) {
        this.autoSeqNumber = autoSeqNumber;
    }

    @Column(name = "auto_fin_year", length = 100)
    public String getAutoFinYear() {
        return autoFinYear;
    }

    public void setAutoFinYear(String autoFinYear) {
        this.autoFinYear = autoFinYear;
    }

    @Column(name = "auto_cmp_code", length = 100)
    public String getAutoCmpCode() {
        return autoCmpCode;
    }

    public void setAutoCmpCode(String autoCmpCode) {
        this.autoCmpCode = autoCmpCode;
    }

    @Column(name = "auto_category", length = 100)
    public String getAutoCategory() {
        return autoCategory;
    }

    public void setAutoCategory(String autoCategory) {
        this.autoCategory = autoCategory;
    }
}
