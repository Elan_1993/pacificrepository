package com.pacific.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class ProductId implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private String prodCode;
	private String prodCmpCode;

	public ProductId() {
	}

	public ProductId(String prodCode, String prodCmpCode) {
		this.prodCode = prodCode;
		this.prodCmpCode = prodCmpCode;
	}

	@Column(name = "prod_code", nullable = false, length = 50)
	public String getProdCode() {
		return this.prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Column(name = "prod_cmp_code", nullable = false, length = 100)
	public String getProdCmpCode() {
		return this.prodCmpCode;
	}

	public void setProdCmpCode(String prodCmpCode) {
		this.prodCmpCode = prodCmpCode;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProductId))
			return false;
		ProductId castOther = (ProductId) other;

		return ((this.getProdCode() == castOther.getProdCode()) || (this.getProdCode() != null
				&& castOther.getProdCode() != null && this.getProdCode().equals(castOther.getProdCode())))
				&& ((this.getProdCmpCode() == castOther.getProdCmpCode())
						|| (this.getProdCmpCode() != null && castOther.getProdCmpCode() != null
								&& this.getProdCmpCode().equals(castOther.getProdCmpCode())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getProdCode() == null ? 0 : this.getProdCode().hashCode());
		result = 37 * result + (getProdCmpCode() == null ? 0 : this.getProdCmpCode().hashCode());
		return result;
	}
}
