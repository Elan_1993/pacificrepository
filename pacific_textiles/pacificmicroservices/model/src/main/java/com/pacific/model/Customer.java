package com.pacific.model;
// Generated Dec 23, 2020, 11:40:14 PM by Hibernate Tools 3.2.2.GA

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;

/**
 * CustomerM generated by hbm2java.
 */
@Entity
@Table(name = "customer_m")
public class Customer implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private String cusCode;
	private Date cusDob;
	private String cusGender;
	private String cusName;
	private String cusCountry;
	private String cusMobile;
	private String cusEmailId;
	private Boolean cusIsPaid;
	private Integer cusType;
	private String cusPaidRefNo;
	private Date dealershipExpiryDate;
	private Date createdDatetime;
	private Date modifiedDatetime;
//	private Set<CustomerAddress> customerAddresses = new HashSet<CustomerAddress>();

	public Customer() {
	}

	public Customer(String cusCode) {
		this.cusCode = cusCode;
	}

	public Customer(String cusCode, Date cusDob, String cusGender, String cusName, String cusCountry, String cusMobile,
			String cusEmailId, Boolean cusIsPaid, Integer cusType,String cusPaidRefNo, Date dealershipExpiryDate,Date createdDatetime , Date modifiedDatetime,
			Set<CustomerAddress> customerAddresses) {
		this.cusCode = cusCode;
		this.cusDob = cusDob;
		this.cusGender = cusGender;
		this.cusName = cusName;
		this.cusCountry = cusCountry;
		this.cusMobile = cusMobile;
		this.cusEmailId = cusEmailId;
		this.cusIsPaid = cusIsPaid;
		this.cusType = cusType;
		this.cusPaidRefNo = cusPaidRefNo;
		this.dealershipExpiryDate = dealershipExpiryDate;
		this.createdDatetime = createdDatetime;
		this.modifiedDatetime = modifiedDatetime;
//		this.customerAddresses = customerAddresses;
	}

	@Id

	@Column(name = "cus_code", unique = true, nullable = false, length = 50)
	public String getCusCode() {
		return this.cusCode;
	}

	public void setCusCode(String cusCode) {
		this.cusCode = cusCode;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "cus_dob", length = 10)
	public Date getCusDob() {
		return this.cusDob;
	}

	public void setCusDob(Date cusDob) {
		this.cusDob = cusDob;
	}

	@Column(name = "cus_gender", length = 10)
	public String getCusGender() {
		return this.cusGender;
	}

	public void setCusGender(String cusGender) {
		this.cusGender = cusGender;
	}

	@Column(name = "cus_name", length = 100)
	public String getCusName() {
		return this.cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	@Column(name = "cus_country", length = 50)
	public String getCusCountry() {
		return this.cusCountry;
	}

	public void setCusCountry(String cusCountry) {
		this.cusCountry = cusCountry;
	}

	@Column(name = "cus_mobile", length = 50)
	public String getCusMobile() {
		return this.cusMobile;
	}

	public void setCusMobile(String cusMobile) {
		this.cusMobile = cusMobile;
	}

	@Column(name = "cus_email_id", length = 100)
	public String getCusEmailId() {
		return this.cusEmailId;
	}

	public void setCusEmailId(String cusEmailId) {
		this.cusEmailId = cusEmailId;
	}

	@Column(name = "cus_is_paid")
	public Boolean getCusIsPaid() {
		return this.cusIsPaid;
	}

	public void setCusIsPaid(Boolean cusIsPaid) {
		this.cusIsPaid = cusIsPaid;
	}

	@Column(name = "cus_paid_ref_no", length = 100)
	public String getCusPaidRefNo() {
		return this.cusPaidRefNo;
	}

	public void setCusPaidRefNo(String cusPaidRefNo) {
		this.cusPaidRefNo = cusPaidRefNo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dealership_expiry_date", length = 10)
	public Date getDealershipExpiryDate() {
		return this.dealershipExpiryDate;
	}

	public void setDealershipExpiryDate(Date dealershipExpiryDate) {
		this.dealershipExpiryDate = dealershipExpiryDate;
	}

	@Column(name = "cus_type")
	public Integer getCusType() {return cusType;}

	public void setCusType(Integer cusType) {this.cusType = cusType;}

	@Column(name = "created_datetime" , updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Column(name = "modified_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifiedDatetime() {
		return this.modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customerM")
//	public Set<CustomerAddress> getCustomerAddresses() {
//		return this.customerAddresses;
//	}
//
//	public void setCustomerAddresses(Set<CustomerAddress> customerAddresses) {
//		this.customerAddresses = customerAddresses;
//	}

}
