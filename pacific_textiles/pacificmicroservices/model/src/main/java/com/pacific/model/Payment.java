package com.pacific.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "payment")
public class Payment {
    private String orderNo;
    private Double orderAmount;
    private String rpayOrderNo;
    private String rpayPaymentId;
    private String rpaySignature;

    public Payment (){

    }

    public Payment(String orderNo, Double orderAmount, String rpayOrderNo, String rpayPaymentId, String rpaySignature) {
        this.orderNo = orderNo;
        this.orderAmount = orderAmount;
        this.rpayOrderNo = rpayOrderNo;
        this.rpayPaymentId = rpayPaymentId;
        this.rpaySignature = rpaySignature;
    }
    @Id
    @Column(name="order_no", unique=true, nullable=false, length=50)
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    @Column(name="order_amount", precision=22, scale=6)
    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    @Column(name="rpay_order_no", length=50)
    public String getRpayOrderNo() {
        return rpayOrderNo;
    }

    public void setRpayOrderNo(String rpayOrderNo) {
        this.rpayOrderNo = rpayOrderNo;
    }

    @Column(name="rpay_payment_id", length=50)
    public String getRpayPaymentId() {
        return rpayPaymentId;
    }

    public void setRpayPaymentId(String rpayPaymentId) {
        this.rpayPaymentId = rpayPaymentId;
    }

    @Column(name="rpay_signature", length=50)
    public String getRpaySignature() {
        return rpaySignature;
    }

    public void setRpaySignature(String rpaySignature) {
        this.rpaySignature = rpaySignature;
    }
}
