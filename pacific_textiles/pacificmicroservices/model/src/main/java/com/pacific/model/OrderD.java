package com.pacific.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "order_d")
public class OrderD implements Serializable {
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private OrderDId id;
	private OrderH orderH;
	private String orderDCusCode;
	private Double orderQty;
	private Double orderRate;
	private Double orderGrossAmt;
	private Double orderTaxAmt;
	private Double orderDisc;
	private Double orderNetAmt;
	private String imgSrc;

	public OrderD() {
	}


	public OrderD(OrderDId id, OrderH orderH, String orderDCusCode) {
		this.id = id;
		this.orderH = orderH;
		this.orderDCusCode = orderDCusCode;
	}
	public OrderD(OrderDId id, OrderH orderH, String orderDCusCode, Double orderQty, Double orderRate, Double orderGrossAmt, Double orderTaxAmt, Double orderDisc, Double orderNetAmt) {
		this.id = id;
		this.orderH = orderH;
		this.orderDCusCode = orderDCusCode;
		this.orderQty = orderQty;
		this.orderRate = orderRate;
		this.orderGrossAmt = orderGrossAmt;
		this.orderTaxAmt = orderTaxAmt;
		this.orderDisc = orderDisc;
		this.orderNetAmt = orderNetAmt;
	}

	@EmbeddedId

	@AttributeOverrides( {
			@AttributeOverride(name="orderNo", column=@Column(name="order_no", nullable=false, length=50) ),
			@AttributeOverride(name="orderCmpCode", column=@Column(name="order_cmp_code", nullable=false, length=50) ),
			@AttributeOverride(name="orderProdCode", column=@Column(name="order_prod_code", nullable=false, length=50) ) } )
	public OrderDId getId() {
		return this.id;
	}

	public void setId(OrderDId id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="order_no", nullable=false, insertable=false, updatable=false)
	public OrderH getOrderH() {
		return this.orderH;
	}

	public void setOrderH(OrderH orderH) {
		this.orderH = orderH;
	}

	@Column(name="order_d_cus_code", nullable=false, length=50)
	public String getOrderDCusCode() {
		return this.orderDCusCode;
	}

	public void setOrderDCusCode(String orderDCusCode) {
		this.orderDCusCode = orderDCusCode;
	}

	@Column(name="order_qty", precision=22, scale=6)
	public Double getOrderQty() {
		return this.orderQty;
	}

	public void setOrderQty(Double orderQty) {
		this.orderQty = orderQty;
	}

	@Column(name="order_rate", precision=22, scale=6)
	public Double getOrderRate() {
		return this.orderRate;
	}

	public void setOrderRate(Double orderRate) {
		this.orderRate = orderRate;
	}

	@Column(name="order_gross_amt", precision=22, scale=6)
	public Double getOrderGrossAmt() {
		return this.orderGrossAmt;
	}

	public void setOrderGrossAmt(Double orderGrossAmt) {
		this.orderGrossAmt = orderGrossAmt;
	}

	@Column(name="order_tax_amt", precision=22, scale=6)
	public Double getOrderTaxAmt() {
		return this.orderTaxAmt;
	}

	public void setOrderTaxAmt(Double orderTaxAmt) {
		this.orderTaxAmt = orderTaxAmt;
	}

	@Column(name="order_disc", precision=22, scale=6)
	public Double getOrderDisc() {
		return this.orderDisc;
	}

	public void setOrderDisc(Double orderDisc) {
		this.orderDisc = orderDisc;
	}

	@Column(name="order_net_amt", precision=22, scale=6)
	public Double getOrderNetAmt() {
		return this.orderNetAmt;
	}

	public void setOrderNetAmt(Double orderNetAmt) {
		this.orderNetAmt = orderNetAmt;
	}

	@Transient
	public String getImgSrc() {
		return imgSrc;
	}

	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}
}
