package com.pacific.model;
// Generated 18-Dec-2020, 2:23:10 PM by Hibernate Tools 3.2.2.GA


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * CompanyM generated by hbm2java.
 */
@Entity
@Table(name = "company_m")
public class Company implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private String cmpCode;
    private String cmpName;
    private String cmpGstNo;
    private String cmpAddress;
    private String cmpCity;
    private String cmpState;
    private String cmpCountry;
    private Integer createdby;
    private Integer modifiedby;
    private Date createdDatetime;
    private Date modifiedDatetime;

    public Company() {
    }


    public Company(String cmpCode) {
        this.cmpCode = cmpCode;
    }

    public Company(String cmpCode, String cmpName, String cmpGstNo, String cmpAddress, String cmpCity, String cmpState, String cmpCountry, Integer createdby, Integer modifiedby, Date createdDatetime, Date modifiedDatetime) {
        this.cmpCode = cmpCode;
        this.cmpName = cmpName;
        this.cmpGstNo = cmpGstNo;
        this.cmpAddress = cmpAddress;
        this.cmpCity = cmpCity;
        this.cmpState = cmpState;
        this.cmpCountry = cmpCountry;
        this.createdby = createdby;
        this.modifiedby = modifiedby;
        this.createdDatetime = createdDatetime;
        this.modifiedDatetime = modifiedDatetime;
    }

    @Id

    @Column(name = "cmp_code", unique = true, nullable = false, length = 20)
    public String getCmpCode() {
        return this.cmpCode;
    }

    public void setCmpCode(String cmpCode) {
        this.cmpCode = cmpCode;
    }

    @Column(name = "cmp_name", length = 100)
    public String getCmpName() {
        return this.cmpName;
    }

    public void setCmpName(String cmpName) {
        this.cmpName = cmpName;
    }

    @Column(name = "cmp_gst_no", length = 50)
    public String getCmpGstNo() {
        return this.cmpGstNo;
    }

    public void setCmpGstNo(String cmpGstNo) {
        this.cmpGstNo = cmpGstNo;
    }

    @Column(name = "cmp_address", length = 100)
    public String getCmpAddress() {
        return this.cmpAddress;
    }

    public void setCmpAddress(String cmpAddress) {
        this.cmpAddress = cmpAddress;
    }

    @Column(name = "cmp_city", length = 100)
    public String getCmpCity() {
        return this.cmpCity;
    }

    public void setCmpCity(String cmpCity) {
        this.cmpCity = cmpCity;
    }

    @Column(name = "cmp_state", length = 50)
    public String getCmpState() {
        return this.cmpState;
    }

    public void setCmpState(String cmpState) {
        this.cmpState = cmpState;
    }

    @Column(name = "cmp_country", length = 50)
    public String getCmpCountry() {
        return this.cmpCountry;
    }

    public void setCmpCountry(String cmpCountry) {
        this.cmpCountry = cmpCountry;
    }

    @Column(name = "createdby")
    public Integer getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    @Column(name = "modifiedby")
    public Integer getModifiedby() {
        return this.modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_datetime", length = 19)
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_datetime", length = 19)
    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }


}


