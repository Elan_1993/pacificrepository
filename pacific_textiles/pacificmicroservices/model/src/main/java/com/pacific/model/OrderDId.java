package com.pacific.model;

import java.io.Serializable;

import javax.persistence.Column;

public class OrderDId implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private String orderNo;
	private String orderCmpCode;
	private String orderProdCode;

	public OrderDId() {
	}

	public OrderDId(String orderNo, String orderCmpCode, String orderProdCode) {
		this.orderNo = orderNo;
		this.orderCmpCode = orderCmpCode;
		this.orderProdCode = orderProdCode;
	}


	@Column(name="order_no", nullable=false, length=50)
	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="order_cmp_code", nullable=false, length=50)
	public String getOrderCmpCode() {
		return this.orderCmpCode;
	}

	public void setOrderCmpCode(String orderCmpCode) {
		this.orderCmpCode = orderCmpCode;
	}

	@Column(name="order_prod_code", nullable=false, length=50)
	public String getOrderProdCode() {
		return this.orderProdCode;
	}

	public void setOrderProdCode(String orderProdCode) {
		this.orderProdCode = orderProdCode;
	}


	public boolean equals(Object other) {
		if ( (this == other ) ) return true;
		if ( (other == null ) ) return false;
		if ( !(other instanceof OrderDId) ) return false;
		OrderDId castOther = ( OrderDId ) other;

		return ( (this.getOrderNo()==castOther.getOrderNo()) || ( this.getOrderNo()!=null && castOther.getOrderNo()!=null && this.getOrderNo().equals(castOther.getOrderNo()) ) )
				&& ( (this.getOrderCmpCode()==castOther.getOrderCmpCode()) || ( this.getOrderCmpCode()!=null && castOther.getOrderCmpCode()!=null && this.getOrderCmpCode().equals(castOther.getOrderCmpCode()) ) )
				&& ( (this.getOrderProdCode()==castOther.getOrderProdCode()) || ( this.getOrderProdCode()!=null && castOther.getOrderProdCode()!=null && this.getOrderProdCode().equals(castOther.getOrderProdCode()) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getOrderNo() == null ? 0 : this.getOrderNo().hashCode() );
		result = 37 * result + ( getOrderCmpCode() == null ? 0 : this.getOrderCmpCode().hashCode() );
		result = 37 * result + ( getOrderProdCode() == null ? 0 : this.getOrderProdCode().hashCode() );
		return result;
	}

}
