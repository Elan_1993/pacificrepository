package com.pacific.model;
// Generated 20-Jan-2021, 11:29:54 PM by Hibernate Tools 3.2.2.GA


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * Category generated by hbm2java.
 */
@Entity
@Table(name = "category")
public class Category implements Serializable {


    private int catId;
    private String catName;
    private Integer catParentRid;

    public Category() {
    }


    public Category(int catId) {
        this.catId = catId;
    }

    public Category(int catId, String catName, Integer catParentRid) {
        this.catId = catId;
        this.catName = catName;
        this.catParentRid = catParentRid;
    }

    @Id
    @Column(name = "cat_id", unique = true, nullable = false)
    public int getCatId() {
        return this.catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    @Column(name = "cat_name", length = 100)
    public String getCatName() {
        return this.catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    @Column(name = "cat_parent_rid")
    public Integer getCatParentRid() {
        return this.catParentRid;
    }

    public void setCatParentRid(Integer catParentRid) {
        this.catParentRid = catParentRid;
    }


}


