package com.pacific.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
public class ProductImagesId implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;


    private String prodCmpCode;
    private String prodCode;

    public ProductImagesId() {
    }

    public ProductImagesId(String prodCmpCode, String prodCode) {
        this.prodCmpCode = prodCmpCode;
        this.prodCode = prodCode;
    }


    @Column(name = "prod_cmp_code", nullable = false, length = 50)
    public String getProdCmpCode() {
        return this.prodCmpCode;
    }

    public void setProdCmpCode(String prodCmpCode) {
        this.prodCmpCode = prodCmpCode;
    }

    @Column(name = "prod_code", nullable = false, length = 50)
    public String getProdCode() {
        return this.prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }


    public boolean equals(Object other) {
        if ((this == other)) return true;
        if ((other == null)) return false;
        if (!(other instanceof ProductImagesId)) return false;
        ProductImagesId castOther = (ProductImagesId) other;

        return ((this.getProdCmpCode() == castOther.getProdCmpCode()) || (this.getProdCmpCode() != null && castOther.getProdCmpCode() != null && this.getProdCmpCode().equals(castOther.getProdCmpCode())))
                && ((this.getProdCode() == castOther.getProdCode()) || (this.getProdCode() != null && castOther.getProdCode() != null && this.getProdCode().equals(castOther.getProdCode())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getProdCmpCode() == null ? 0 : this.getProdCmpCode().hashCode());
        result = 37 * result + (getProdCode() == null ? 0 : this.getProdCode().hashCode());
        return result;
    }


}


