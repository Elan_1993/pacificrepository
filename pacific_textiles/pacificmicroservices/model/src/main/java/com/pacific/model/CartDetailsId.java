package com.pacific.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable

public class CartDetailsId implements Serializable {


    private Integer cartId;
    private String cartCmpCode;
    private String cartProdCode;
    private String cartUserCode;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="cart_id")
    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    @Column(name="cart_cmp_code",length=50)
    public String getCartCmpCode() {
        return cartCmpCode;
    }

    public void setCartCmpCode(String cartCmpCode) {
        this.cartCmpCode = cartCmpCode;
    }

    @Column(name="cart_prod_code",length=100)
    public String getCartProdCode() {
        return cartProdCode;
    }

    public void setCartProdCode(String cartProdCode) {
        this.cartProdCode = cartProdCode;
    }

    @Column(name="cart_user_code",length=50)
    public String getCartUserCode() {
        return cartUserCode;
    }

    public void setCartUserCode(String cartUserCode) {
        this.cartUserCode = cartUserCode;
    }
}
