package com.pacific.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "material_m")
public class Material {

	private static final long serialVersionUID = 1L;
	private int material_id;
	private String material_name;

	@Id
    @Column(name = "material_id", unique = true, nullable = false, length = 11)
	public int getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(int material_id) {
		this.material_id = material_id;
	}

	 @Column(name = "material_name", length = 50)
	public String getMaterial_name() {
		return material_name;
	}

	public void setMaterial_name(String material_name) {
		this.material_name = material_name;
	}	

}
