package com.pacific.model;
// Generated Dec 23, 2020, 11:40:14 PM by Hibernate Tools 3.2.2.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;


/**
 * CustomerAddress generated by hbm2java.
 */
@Entity
@Table(name = "customer_address")
public class CustomerAddress implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    private CustomerAddressId id;
    private Customer customerM;
    private String cusAdress1;
    private String cusAdress2;
    private String cusPincode;
    private String cusCity;
    private String cusState;
    private String cusCountry;
    private Byte cusIsDefaultAddress;
    private String cusPhoneNumber;
    private String cusAlternatePhoneNo;

    public CustomerAddress() {
    }


    public CustomerAddress(CustomerAddressId id, Customer customerM) {
        this.id = id;
        this.customerM = customerM;
    }

    public CustomerAddress(CustomerAddressId id, Customer customerM, String cusAdress1, String cusAdress2, String cusPincode, String cusCity, String cusState, String cusCountry, Byte cusIsDefaultAddress, String cusPhoneNumber, String cusAlternatePhoneNo) {
        this.id = id;
        this.customerM = customerM;
        this.cusAdress1 = cusAdress1;
        this.cusAdress2 = cusAdress2;
        this.cusPincode = cusPincode;
        this.cusCity = cusCity;
        this.cusState = cusState;
        this.cusCountry = cusCountry;
        this.cusIsDefaultAddress = cusIsDefaultAddress;
        this.cusPhoneNumber = cusPhoneNumber;
        this.cusAlternatePhoneNo = cusAlternatePhoneNo;
    }

    @EmbeddedId

    @AttributeOverrides({
            @AttributeOverride(name = "cusAddressId", column = @Column(name = "cus_address_id", nullable = false)),
            @AttributeOverride(name = "cusCode", column = @Column(name = "cus_code", nullable = false, length = 50))})
    public CustomerAddressId getId() {
        return this.id;
    }

    public void setId(CustomerAddressId id) {
        this.id = id;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cus_code", nullable = false, insertable = false, updatable = false)
    public Customer getCustomerM() {
        return this.customerM;
    }

    public void setCustomerM(Customer customerM) {
        this.customerM = customerM;
    }

    @Column(name = "cus_adress1", length = 100)
    public String getCusAdress1() {
        return this.cusAdress1;
    }

    public void setCusAdress1(String cusAdress1) {
        this.cusAdress1 = cusAdress1;
    }

    @Column(name = "cus_adress2", length = 100)
    public String getCusAdress2() {
        return this.cusAdress2;
    }

    public void setCusAdress2(String cusAdress2) {
        this.cusAdress2 = cusAdress2;
    }

    @Column(name = "cus_pincode", length = 20)
    public String getCusPincode() {
        return this.cusPincode;
    }

    public void setCusPincode(String cusPincode) {
        this.cusPincode = cusPincode;
    }

    @Column(name = "cus_city", length = 100)
    public String getCusCity() {
        return this.cusCity;
    }

    public void setCusCity(String cusCity) {
        this.cusCity = cusCity;
    }

    @Column(name = "cus_state", length = 100)
    public String getCusState() {
        return this.cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    @Column(name = "cus_country", length = 100)
    public String getCusCountry() {
        return this.cusCountry;
    }

    public void setCusCountry(String cusCountry) {
        this.cusCountry = cusCountry;
    }

    @Column(name = "cus_is_default_address")
    public Byte getCusIsDefaultAddress() {
        return this.cusIsDefaultAddress;
    }

    public void setCusIsDefaultAddress(Byte cusIsDefaultAddress) {
        this.cusIsDefaultAddress = cusIsDefaultAddress;
    }

    @Column(name = "cus_phone_number", length = 100)
    public String getCusPhoneNumber() {
        return this.cusPhoneNumber;
    }

    public void setCusPhoneNumber(String cusPhoneNumber) {
        this.cusPhoneNumber = cusPhoneNumber;
    }

    @Column(name = "cus_alternate_phone_no", length = 100)
    public String getCusAlternatePhoneNo() {
        return this.cusAlternatePhoneNo;
    }

    public void setCusAlternatePhoneNo(String cusAlternatePhoneNo) {
        this.cusAlternatePhoneNo = cusAlternatePhoneNo;
    }


}


