package com.pacific.model;
// Generated 25-Nov-2020, 11:55:17 AM by Hibernate Tools 3.2.2.GA

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * User generated by hbm2java.
 */
@Entity
@Table(name = "user_m")
public class User implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private UserGroup userGroup;
	private String userCode;
	private String groupCode;
	private String userName;
	private String userPassword;
	private String userPhone;
	private String userMail;
	private String userGender;
	private Date userDob;
	private String userAddress;
	private String userCity;
	private String userPincode;
	private String userState;
	private String userCountry;
	private Boolean isUserActive;
	private Date passwordExpiryDate;
	private Boolean isCompanyUser;
	private String userCmpCode;
	private Boolean isSuperAdmin;
	private Boolean isLocked;
	private Integer incorrectPwdCount;
	private Date accountLockedTillDate;
	private String createdBy;
	private String modifiedBy;
	private Date createdDatetime;
	private Date modifiedDatetime;

	public User() {
	}

	public User(String userCode, UserGroup userGroup) {
		this.userCode = userCode;
		this.userGroup = userGroup;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "group_code", nullable = false, insertable = false, updatable = false)
	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	@Id
	@Column(name = "user_code", length = 50)
	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "user_name", length = 50)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonIgnore
	@Column(name = "user_password", length = 50)
	public String getUserPassword() {
		return this.userPassword;
	}

	@JsonProperty
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	@Column(name = "user_phone", length = 50)
	public String getUserPhone() {
		return this.userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	@Column(name = "user_mail", length = 50)
	public String getUserMail() {
		return this.userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	@Column(name = "user_gender", length = 50)
	public String getUserGender() {
		return this.userGender;
	}

	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "user_dob", length = 10)
	public Date getUserDob() {
		return this.userDob;
	}

	public void setUserDob(Date userDob) {
		this.userDob = userDob;
	}

	@Column(name = "user_address", length = 100)
	public String getUserAddress() {
		return this.userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	@Column(name = "user_city", length = 50)
	public String getUserCity() {
		return this.userCity;
	}

	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}

	@Column(name = "user_pincode", length = 50)
	public String getUserPincode() {
		return this.userPincode;
	}

	public void setUserPincode(String userPincode) {
		this.userPincode = userPincode;
	}

	@Column(name = "user_state", length = 50)
	public String getUserState() {
		return this.userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	@Column(name = "user_country", length = 50)
	public String getUserCountry() {
		return this.userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	@Column(name = "is_user_active")
	public Boolean getIsUserActive() {
		return this.isUserActive;
	}

	public void setIsUserActive(Boolean isUserActive) {
		this.isUserActive = isUserActive;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "password_expiry_date", length = 10)
	public Date getPasswordExpiryDate() {
		return this.passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

	@Column(name = "is_company_user")
	public Boolean getIsCompanyUser() {
		return this.isCompanyUser;
	}

	public void setIsCompanyUser(Boolean isCompanyUser) {
		this.isCompanyUser = isCompanyUser;
	}

	@Column(name = "user_cmp_code", length = 50)
	public String getUserCmpCode() {
		return this.userCmpCode;
	}

	public void setUserCmpCode(String userCmpCode) {
		this.userCmpCode = userCmpCode;
	}
	
	@Column(name = "is_super_admin")
	public Boolean getIsSuperAdmin() {
		return this.isSuperAdmin;
	}

	public void setIsSuperAdmin(Boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	@Column(name = "is_locked")
	public Boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	@Column(name = "incorrect_pwd_count")
	public Integer getIncorrectPwdCount() {
		return this.incorrectPwdCount;
	}

	public void setIncorrectPwdCount(Integer incorrectPwdCount) {
		this.incorrectPwdCount = incorrectPwdCount;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "account_locked_till_date", length = 19)
	public Date getAccountLockedTillDate() {
		return this.accountLockedTillDate;
	}

	public void setAccountLockedTillDate(Date accountLockedTillDate) {
		this.accountLockedTillDate = accountLockedTillDate;
	}

	@Column(name = "createdby", updatable = false)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "modifiedby")
	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_datetime", length = 19, updatable = false)
	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_datetime", length = 19)
	public Date getModifiedDatetime() {
		return this.modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	@Column(name = "group_code")
	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

}
