package com.pacific.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "brand_m")
public class Brand {

	private int brand_id;
	private String brand_name;

	@Id
	@Column(name = "brand_id", unique = true, nullable = false, length = 11)
	public int getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}

	@Column(name = "brand_name", length = 50)
	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

}
