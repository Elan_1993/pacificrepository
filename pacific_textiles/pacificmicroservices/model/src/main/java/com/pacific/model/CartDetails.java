package com.pacific.model;

import javax.persistence.*;

@Entity
@Table(name = "cart_details")
public class CartDetails {
    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private CartDetailsId id;
    private int cartProdQty;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "cartId", column =  @Column(name="cart_id")),
            @AttributeOverride(name = "cartCmpCode", column =  @Column(name="cart_cmp_code",length=50)),
            @AttributeOverride(name = "cartProdCode", column = @Column(name="cart_prod_code",length=100)),
            @AttributeOverride(name = "cartUserCode", column = @Column(name="cart_user_code",length=50)) })


    public CartDetailsId getId() {
        return id;
    }

    public void setId(CartDetailsId id) {
        this.id = id;
    }

    @Column(name="cart_prod_qty",precision=22,scale=6)
    public int getCartProdQty() {
        return cartProdQty;
    }

    public void setCartProdQty(int cartProdQty) {
        this.cartProdQty = cartProdQty;
    }
}
