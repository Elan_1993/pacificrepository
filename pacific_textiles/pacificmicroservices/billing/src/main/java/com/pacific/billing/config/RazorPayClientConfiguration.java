package com.pacific.billing.config;

import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

public class RazorPayClientConfiguration {

    private static RazorpayClient razorpayClient = null;

    private RazorPayClientConfiguration() {
    }

    public static RazorpayClient getInstance() throws RazorpayException {
        if (razorpayClient == null) {
            razorpayClient = new RazorpayClient(RazorPayConfig.key, RazorPayConfig.secret);
        }
        return razorpayClient;
    }
}
