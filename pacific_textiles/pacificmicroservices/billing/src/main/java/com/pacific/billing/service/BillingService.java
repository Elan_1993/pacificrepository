package com.pacific.billing.service;

import com.pacific.common.responsedto.ResponseBeanDTO;
import com.pacific.model.CartDetails;
import com.pacific.model.OrderH;
import com.razorpay.RazorpayException;

import java.util.Date;
import java.util.List;

public interface BillingService {

    /**
     * @param order
     * @return
     */
    ResponseBeanDTO<OrderH> saveOrderH(OrderH order) throws Exception;

    /**
     * @return
     */
    List<OrderH> fetchAllOrders();

    /**
     * @return
     */
    List<OrderH> fetchOrdersByUserCode(String userId, Date fromDate, Date toDate);

    /**
     * @param cartdetails
     */
    void saveCart(CartDetails cartdetails);

    /**
     * @param state
     * @return OrderList
     */
    List<OrderH> fetchOrdersByState(Integer state, Date fromDate, Date toDate);

    OrderH getOrderByID(String orderId);

    OrderH checkSignature(OrderH orderh) throws Exception;

    /**
     * @param fromDate , toDate
     * @return OrderList
     */
    List<OrderH> fetchOrdersByDate(Date fromDate, Date toDate);

    /**
     *
     * @param orderHList
     */
    void saveAll(List<OrderH> orderHList);

    void stockReversal(OrderH orderH) throws Exception;
}
