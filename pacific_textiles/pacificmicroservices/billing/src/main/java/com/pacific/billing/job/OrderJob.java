package com.pacific.billing.job;

import com.pacific.billing.service.BillingService;
import com.pacific.common.util.CommonUtil;
import com.pacific.common.util.DateUtil;
import com.pacific.inventory.service.IInventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component
public class OrderJob {


    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(OrderJob.class);

    /**
     * service.
     */
    private final BillingService service;

    /**
     * inventoryService.
     */
    private final IInventoryService inventoryService;

    /**
     * Constructor to inject dependencies.
     *
     * @param serviceIn {@link BillingService}
     */
    public OrderJob(final BillingService serviceIn, final IInventoryService inventoryServiceIn) {
        this.service = serviceIn;
        this.inventoryService = inventoryServiceIn;
    }

    @Scheduled(cron = "${order.cron}")
    public final void execute() throws ParseException {
        try {
            LOG.info("Job started");
            var orderList = service.fetchOrdersByState(1, DateUtil.convertStringToDate(DateUtil.getDateStringAsGivenDays(30)), DateUtil.convertStringToDate(DateUtil.getYesterdayDateString()));
            if (!CommonUtil.isEmpty(orderList)) {
                orderList.forEach(a -> {
                    a.setOrderState(4);
                    a.setModifiedDatetime(new Date());
                    a.setOrderStatus("Rejected");
                    try {
                        inventoryService.reduceStockByOrder(a, "ORDER_REVERSAL");
                    } catch (Exception e) {
                        LOG.info(e.getMessage());

                    }
                });
                service.saveAll(orderList);
            }
            LOG.info("Job finished");
        }catch (Exception e){
            LOG.info(e.getMessage());
        }
    }

}

