package com.pacific.billing.controller;

import com.pacific.billing.service.BillingService;
import com.pacific.common.dto.CustomInputDTO;
import com.pacific.common.responsedto.ResponseBeanDTO;
import com.pacific.model.CartDetails;
import com.pacific.model.OrderH;
import com.razorpay.RazorpayException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping(value = "/billing/controller")
public class BillingController {


    /** billingService*/
    private final BillingService billingService;

    /**
     *
     * @param billingServiceIn
     */
    public BillingController(BillingService billingServiceIn) {
        this.billingService = billingServiceIn;
    }

    /**
     * @param orderH
     * @return
     */

    @PostMapping(value = "/save-order", produces = "application/json", consumes = "application/json")
    public ResponseBeanDTO<OrderH> saveOrderH(@RequestBody final OrderH orderH) throws Exception {
        return billingService.saveOrderH(orderH);
    }

    /**
     * @param orderH
     * @return
     */

    @PostMapping(value = "/stock-reversal", produces = "application/json", consumes = "application/json")
    public void stockReversal(@RequestBody final OrderH orderH) throws Exception {
        billingService.stockReversal(orderH);
    }

    /**
     * @param orderH
     * @return
     */
    @PostMapping(value = "/check-signature", produces = "application/json", consumes = "application/json")
    public OrderH checkSignature(@RequestBody final OrderH orderH) throws Exception {
        return billingService.checkSignature(orderH);
    }

    @GetMapping("/fetch-All-orders")
    public List<OrderH> fetchAllOrders() {
        return billingService.fetchAllOrders();
    }

    @PostMapping(value = "/fetch-orders-by-state", produces = "application/json", consumes = "application/json")
    public List<OrderH> fetchOrdersByState(final @RequestBody CustomInputDTO customInputDTO) {
        return billingService.fetchOrdersByState(customInputDTO.getStatus(), customInputDTO.getFromDate(),
                customInputDTO.getToDate());
    }

    @PostMapping(value = "/fetch-orders", produces = "application/json", consumes = "application/json")
    public List<OrderH> fetchOrdersByUserCode(final @RequestBody CustomInputDTO customInputDTO) {
        return billingService.fetchOrdersByUserCode(customInputDTO.getUserCode(),customInputDTO.getFromDate(),
                customInputDTO.getToDate());
    }

    @PostMapping(value = "/save-cart", produces = "application/json", consumes = "application/json")
    public void saveCart(@RequestBody final CartDetails cartDetails) {
        billingService.saveCart(cartDetails);
    }

    @GetMapping("/get-order-by-id/{orderId}")
    public OrderH getOrderByID(@PathVariable final String orderId) {
        return billingService.getOrderByID(orderId);
    }

}
