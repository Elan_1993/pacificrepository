package com.pacific.billing.service.impl;

import com.pacific.billing.config.RazorPayClientConfiguration;
import com.pacific.billing.config.RazorPayConfig;
import com.pacific.billing.service.BillingService;
import com.pacific.common.constants.StringConstants;
import com.pacific.common.responsedto.ResponseBeanDTO;
import com.pacific.common.exception.CommonExceptionHandler;
import com.pacific.common.exception.DulabException;
import com.pacific.common.service.CodeGeneratorService;
import com.pacific.common.util.CommonUtil;
import com.pacific.inventory.service.IInventoryService;
import com.pacific.model.*;
import com.pacific.repository.repo.*;
import com.razorpay.Order;
import com.razorpay.RazorpayException;
import com.razorpay.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class BillingServiceImpl extends CodeGeneratorService implements BillingService {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(BillingServiceImpl.class);

    /**
     * orderRepository.
     */
    private final OrderRepository orderRepository;

    /**
     * stateRepository.
     */
    private final StateRepository stateRepository;

    private final PaymentRepository paymentRepository;

    /**
     * cartDetailsRepository.
     */
    private final CartDetailsRepository cartDetailsRepository;

    /**
     * productImagesRepository.
     */
    private final ProductImagesRepository productImagesRepository;

    private final IInventoryService iInventoryService;

    private final CustomerRepository customerRepository;

    /**
     * @param orderRepositoryIn
     * @param cartDetailsRepositoryIn
     * @param stateRepositoryIn
     * @param paymentRepository
     * @param productImagesRepositoryIn
     * @param inventoryServiceIn
     * @param customerRepository
     */
    public BillingServiceImpl(final OrderRepository orderRepositoryIn,
                              final CartDetailsRepository cartDetailsRepositoryIn,
                              final StateRepository stateRepositoryIn,
                              PaymentRepository paymentRepository, final ProductImagesRepository productImagesRepositoryIn,
                              final IInventoryService inventoryServiceIn, final CustomerRepository customerRepository) {
        this.orderRepository = orderRepositoryIn;
        this.stateRepository = stateRepositoryIn;
        this.cartDetailsRepository = cartDetailsRepositoryIn;
        this.paymentRepository = paymentRepository;
        this.productImagesRepository = productImagesRepositoryIn;
        this.iInventoryService = inventoryServiceIn;
        this.customerRepository = customerRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void stockReversal(OrderH orderH) throws Exception {
        setOrderStateAndStatus(orderH , false);
        var orderResponse = orderRepository.save(orderH);
        iInventoryService.reduceStockByOrder(orderH, "ORDER_REVERSAL");
    }

    /**
     * @param fromDate
     * @param toDate
     * @return OrderList
     */
    @Override
    public List<OrderH> fetchOrdersByDate(Date fromDate, Date toDate) {
        return orderRepository.getAllBetweenDates(fromDate, toDate);
    }

    /**
     * @param orderHList
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveAll(List<OrderH> orderHList) {
        orderRepository.saveAll(orderHList);
    }

    /**
     * @return List<OrderH>
     */
    @Override
    public List<OrderH> fetchAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<OrderH> fetchOrdersByUserCode(String UserCode, Date fromDate, Date toDate) {
        var orderHlist= orderRepository.getAllBetweenDatesAndOrderOrderCusCode(fromDate, toDate, UserCode);
        var responseList = new ArrayList<OrderH>();
        ArrayList<OrderH> finalResponseList = responseList;
        orderHlist.forEach(orderH -> {
            if(orderH.getOrderState() == 2||orderH.getOrderState() == 3) {
                setImageSrcInOrderD(orderH);
                finalResponseList.add(orderH);
            }
        });
        responseList = (ArrayList<OrderH>) finalResponseList.stream().sorted(Comparator.comparing(OrderH::getCreatedDatetime).reversed()).collect(Collectors.toList());
        return responseList;
    }

    @Override
    public List<OrderH> fetchOrdersByState(Integer state, Date fromDate, Date toDate) {
        return orderRepository.getAllBetweenDatesAndOrderStatus(fromDate, toDate, state);
    }

    @Override
    public void saveCart(CartDetails cartdetails) {
        cartDetailsRepository.save(cartdetails);
    }

    @Override
    public OrderH getOrderByID(String orderId) {
        var orderH = orderRepository.findById(orderId).orElse(new OrderH());
        var customer= customerRepository.findById(orderH.getOrderCusCode());
        if(customer !=  null) {
            orderH.setCustomerName(customer.get().getCusName());
            orderH.setCustomerMobile(customer.get().getCusMobile());
        }
        setImageSrcInOrderD(orderH);
        return orderH;
    }

    /**
     * @param order
     * @return OrderNo
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    synchronized public ResponseBeanDTO<OrderH> saveOrderH(OrderH order) throws Exception {
        var orderHResponse = new ResponseBeanDTO<OrderH>();
        try {
            if (!CommonUtil.isEmpty(order) && CommonUtil.isEmpty(order.getOrderNo())) {
                Thread.sleep(400);
                var prodCodeList = findStockIsNotAvailableProductCode(order);
                if (CommonUtil.isEmpty(prodCodeList)) {
                    order.setOrderNo(generateCode(StringConstants.ORDER));
                    iInventoryService.reduceStockByOrder(order, "ORDER_DRAFT");
                    order.setCreatedDatetime(new Date());
                    for (OrderD orderD : order.getOrderDs()) {
                        orderD.getId().setOrderNo(order.getOrderNo());
                    }
                    var rPayOrderId = saveOrderInRpay(order);
                    if (rPayOrderId == null) {
                        throw new DulabException("Network seems to be low");
                    }
                    order.setOrderRpayNo(rPayOrderId);
                    saveOrUpdatePayment(order);
                } else {
                    var productCode =prodCodeList.stream().collect(Collectors.joining("-", "{", "}"));
                    throw new DulabException("Stock not available for the products".concat(productCode));
                }
            } else {
                setOrderStateAndStatus(order, true);
            }
            order.setModifiedDatetime(new Date());
            var orderH = orderRepository.save(order);
            orderHResponse.setObject(orderH);
        } catch (Exception e) {
            CommonExceptionHandler.ExceptionHandler(e, orderHResponse);
        }
        return orderHResponse;
    }

    private List<String>findStockIsNotAvailableProductCode(OrderH order){
        List<String> prodCodeList = new ArrayList<String>();
        for(OrderD orderD :order.getOrderDs()){
            Stock stock = iInventoryService.getStock(orderD.getId().getOrderProdCode() , orderD.getId().getOrderCmpCode());
            if(null!= stock){
                Double stkQty = stock.getStkQty();
                Double orderQty = orderD.getOrderQty();
                if(stkQty<orderQty){
                    prodCodeList.add(orderD.getId().getOrderProdCode());
                }
            }
        }
        return prodCodeList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public OrderH checkSignature(OrderH orderH) throws Exception {
        com.razorpay.Payment rPayment = null;
        rPayment = RazorPayClientConfiguration.getInstance().Payments.fetch(orderH.getOrderRpayPaymentNo());
        if (rPayment == null) {
            return null;
        }
        if (verifyPaymentSiganture(orderH)) {
            setOrderStateAndStatus(orderH, true);
            var orderResponse = orderRepository.save(orderH);
            saveOrUpdatePayment(orderResponse);
            iInventoryService.reduceStockByOrder(orderH, "Order");
            return orderResponse;
        } else {
            setOrderStateAndStatus(orderH, false);
            orderH.setModifiedDatetime(new Date());
            var orderResponse = orderRepository.save(orderH);
            iInventoryService.reduceStockByOrder(orderH, "ORDER_REVERSAL");
            return null;
        }
    }
    private OrderH setImageSrcInOrderD(OrderH orderH){
        if (!CommonUtil.isEmpty(orderH)) {
            orderH.getOrderDs().forEach(orderD -> {
                var productImage = productImagesRepository.
                        findById(new ProductImagesId(orderD.getId().getOrderCmpCode(),
                                orderD.getId().getOrderProdCode())).orElse(new ProductImages());
                if (!CommonUtil.isEmpty(productImage)) {
                    orderD.setImgSrc(productImage.getImagePath());
                } else {
                    orderD.setImgSrc("");
                }
            });
        }
        return orderH;
    }

    private boolean verifyPaymentSiganture(OrderH orderH) throws RazorpayException {
        JSONObject attributes = new JSONObject();
        attributes.put("razorpay_signature", orderH.getOrderRpaySignature());
        attributes.put("razorpay_order_id", orderH.getOrderRpayNo());
        attributes.put("razorpay_payment_id", orderH.getOrderRpayPaymentNo());
        return Utils.verifyPaymentSignature(attributes, RazorPayConfig.secret);
    }

    private String saveOrderInRpay(OrderH orderh) {
        Order order = null;
        try {
            JSONObject options = new JSONObject();
            options.put("amount", CommonUtil.convertRupeeToPaise(orderh.getOrderAmount().toString())); // Note: The amount should be in paise.
            options.put("currency", "INR");
            options.put("receipt", orderh.getOrderNo());
            order = RazorPayClientConfiguration.getInstance().Orders.create(options);
            String id = order.get("id");
            return id;
        } catch (RazorpayException e) {
            return null;
        }
    }
    private void saveOrUpdatePayment(OrderH orderH) {
        Optional<Payment> payment = paymentRepository.findById(orderH.getOrderNo());
        if (!payment.isEmpty()) {
            payment.get().setRpaySignature(orderH.getOrderRpaySignature());
            payment.get().setRpayPaymentId(orderH.getOrderRpayNo());
            paymentRepository.save(payment.get());
        } else {
            Payment p = new Payment();
            p.setOrderNo(orderH.getOrderNo());
            p.setOrderAmount(orderH.getOrderAmount());
            p.setRpayOrderNo(orderH.getOrderRpayNo());
            paymentRepository.save(p);
        }
    }

    private OrderH setOrderStateAndStatus(OrderH order, boolean isTransactionSuccess) {
        var stateList = stateRepository.findByModuleNameAndCurrentState(StringConstants.ORDER, order.getOrderState());
        if (stateList != null && !stateList.isEmpty()) {
            stateList.forEach(state -> {
                if (state.getIstransactionSuccess() == isTransactionSuccess) {
                    order.setOrderState(state.getNextState());
                    order.setOrderStatus(state.getNextStatus());
                }
            });
        }
        return order;
    }
}
