package com.pacific.security.service.impl;

import com.pacific.common.dao.IDAORepository;
import com.pacific.common.dto.UserInfoDTO;
import com.pacific.common.service.IQueryService;
import com.pacific.model.User;
import com.pacific.repository.repo.UserRepository;
import com.pacific.security.dto.CustomUserDetails;
import com.pacific.security.service.ISecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static com.pacific.common.constants.QueryNames.INSERT_USER_LOGIN_LOG;
import static com.pacific.common.constants.QueryNames.USER_LOGIN;

@Service
@Transactional
public class SecurityService implements ISecurityService , UserDetailsService {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SecurityService.class);

    /**
     * dao.
     */
    private final IDAORepository dao;

    /**
     * queryService.
     */
    private final IQueryService queryService;

    private final UserRepository userRepository;


    /**
     * Constructor to inject dependency.
     *
     * @param daoIn          DAORepository
     * @param queryServiceIn QueryService
     */
    public SecurityService(final IDAORepository daoIn, final IQueryService queryServiceIn ,
                           final UserRepository userRepositoryIn) {
        this.dao = daoIn;
        this.queryService = queryServiceIn;
        this.userRepository = userRepositoryIn;
    }


    /**
     * This method is used to validate user login.
     *
     * @param username        unique user name
     * @param password        user password
     * @param clientIpAddress user password
     */
    @Override
    public void login(String username, String password, String clientIpAddress) {
        var userInfo =  dao.queryForObjectWithRowMapper(queryService.get(USER_LOGIN), UserInfoDTO.class, username,username,username, password);
        dao.update(queryService.get(INSERT_USER_LOGIN_LOG), username, new Date(), clientIpAddress, true, null);
    }

    /**
     * Locates the user based on the username. In the actual implementation, the search
     * may possibly be case sensitive, or case insensitive depending on how the
     * implementation instance is configured. In this case, the <code>UserDetails</code>
     * object that comes back may have a username that is of a different case than what
     * was actually requested..
     *
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     *                                   GrantedAuthority
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        userRepository.findById(username);

        User user = userRepository.findByUserCodeOrUserPhoneOrUserMail(username,username,username);
        if(user ==null) {
            throw new UsernameNotFoundException("User Not Found");
        }
        return new CustomUserDetails(user);
    }
}
