package com.pacific.security.service;

public interface ISecurityService {


    /**
     * This method is used to validate user login.
     * @param username unique user name
     * @param password user password
     * @param clientIpAddress user password
     */
    void login(String username, String password, String clientIpAddress);
}
