package com.pacific.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;
import java.util.Objects;

@SpringBootApplication(scanBasePackages = "com.pacific.*")
@EnableAuthorizationServer
@EntityScan("com.pacific.model")
@EnableJpaRepositories("com.pacific.repository")
public class SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }


    /**
     * This method is used to create a JDBC token store.
     *
     * @param dataSourceInput JDBC data source
     * @return returns {@link JdbcTokenStore}
     */
    @Bean("jdbcTokenStore")
    public TokenStore tokenStore(final DataSource dataSourceInput) {
        return new JdbcTokenStore(Objects.requireNonNull(dataSourceInput));
    }

}
