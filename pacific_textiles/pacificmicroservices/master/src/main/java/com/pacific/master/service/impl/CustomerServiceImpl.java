package com.pacific.master.service.impl;

import com.pacific.common.responsedto.ApiResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pacific.common.service.CodeGeneratorService;
import com.pacific.master.service.ICustomerService;
import com.pacific.model.Customer;
import com.pacific.repository.repo.CustomerRepository;

@Service
@Transactional
public class CustomerServiceImpl extends CodeGeneratorService implements ICustomerService {
	/** LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CustomerServiceImpl.class);

	/** userRepository. */
	private final CustomerRepository customerRepository;

	/**
	 * This method is used the dependency injection.
	 * 
	 * @param customerRepository {@link CustomerRepository}
	 */
	public CustomerServiceImpl(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ApiResponseDTO saveCustomer(Customer customer) {
		var customerResponse = customerRepository.save(customer);
		return new ApiResponseDTO("Customer updated successfully", customerResponse.getCusCode(), Boolean.TRUE);
	}

	@Override
	public Customer getCustomerByPhoneOrEmail(String phoneOrEmail) {
		return customerRepository.findByCusMobileOrCusEmailId(phoneOrEmail, phoneOrEmail);
	}
}
