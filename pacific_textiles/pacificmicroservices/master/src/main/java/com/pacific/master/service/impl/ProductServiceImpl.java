package com.pacific.master.service.impl;

import com.pacific.common.constants.StringConstants;
import com.pacific.common.dto.ProductDTO;
import com.pacific.common.exception.DulabException;
import com.pacific.common.responsedto.ApiResponseDTO;
import com.pacific.common.service.CodeGeneratorService;
import com.pacific.common.util.CommonUtil;
import com.pacific.master.service.IProductService;
import com.pacific.model.Product;
import com.pacific.model.ProductId;
import com.pacific.model.StockId;
import com.pacific.repository.repo.ProductRepository;
import com.pacific.repository.repo.StockRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductServiceImpl extends CodeGeneratorService implements IProductService {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

    /**
     * productRepository.
     */
    private final ProductRepository productRepository;

    /**
     * stockRepository.
     */
    private final StockRepository stockRepository;

    /**
     * This method is used the dependency injection.
     *
     * @param productRepositoryIn ProductRepository
     * @param stockRepositoryIn   StockRepository
     */
    public ProductServiceImpl(ProductRepository productRepositoryIn, StockRepository stockRepositoryIn) {
        this.productRepository = productRepositoryIn;
        this.stockRepository = stockRepositoryIn;
    }

    @Override
    public ApiResponseDTO saveProductList(final List<Product> productList) {
        if (CommonUtil.isEmpty(productList)) {
            return new ApiResponseDTO("Invalid product detail", null, Boolean.FALSE);
        }
        if (!CommonUtil.isEmpty(checkingExistingProduct(productList))) {
            var existingList = checkingExistingProduct(productList);
            StringBuilder response = new StringBuilder();
            existingList.forEach(product -> {
                response.append(product.getId().getProdCode());
                response.append(",");
            });
            return new ApiResponseDTO("Duplicate product id", response.toString(), Boolean.FALSE);
        }
        if (CommonUtil.isEmpty(productList.get(0).getProdCatalogId())) {
            int catalogSequence = generateSequence(StringConstants.CATALOG);
            productList.forEach(product ->
                    product.setProdCatalogId(catalogSequence));
        }
        productList.forEach(product -> {
            product.setCreatedDatetime(new Date());
            product.setModifiedDatetime(new Date());
        });
        productRepository.saveAll(productList);
        return new ApiResponseDTO("Catalog is successfully saved", productList.get(0).getProdCatalogId().toString(), Boolean.TRUE);
    }

    private List<Product> checkingExistingProduct(List<Product> productList) {
        var prodCodeList = new ArrayList<String>();
        productList.forEach(product -> {
            if (product.isUpdateCheck()) {
                prodCodeList.add(product.getId().getProdCode());
            }
        });
        return productRepository.searchByMultipeProductCode(prodCodeList);
    }

    @Override
    public List<Product> getAllProduct() {
        return this.productRepository.findAll();
    }

    @Override
    public Product getProductById(ProductId productId) {
        var productDb = this.productRepository.findById(productId);
        if (productDb.isPresent()) {
            return productDb.get();
        } else {
            return null;
        }
    }

    @Override
    public ApiResponseDTO deleteProduct(String prodCode, String prodCmpCode) {
        var productDb = this.productRepository.findById(new ProductId(prodCode, prodCmpCode));
        if (productDb.isPresent()) {
            productDb.get().setProdIsActive(false);
            productDb.get().setModifiedDatetime(new Date());
            var product = this.productRepository.save(productDb.get());
            return new ApiResponseDTO("Product is successfully deleted", product.getId().getProdCode(), Boolean.TRUE);
        }
        return new ApiResponseDTO("Contact Administrator", prodCode, Boolean.FALSE);
    }

    @Override
    public List<Product> findProductsByCatalogId(int catalogId) {
        return productRepository.findByProdCatalogIdAndProdIsActive(catalogId, true);
    }

    @Override
    public List<Product> getAllCatalogs() {
        return productRepository.findAllCatalog();
    }

    @Override
    public List<ProductDTO> search(final String keywords, final boolean withStock) {
        List<Product> productList = new ArrayList<>();
        if (withStock) {
            productList = productRepository.searchByProductCodeWithStock(keywords);
        } else {
            productList = productRepository.searchByIdProdCodeLike(keywords);
        }
        var productDTOList = new ArrayList<ProductDTO>();

        productList.forEach(x -> {
            var productDTO = new ProductDTO();
            productDTO.setProdCode(x.getId().getProdCode());
            productDTO.setProdCmpCode(x.getId().getProdCmpCode());
            productDTO.setProdName(x.getProdName());
            productDTO.setProdPrice(x.getProdPrice());
            productDTO.setProdCatalogId(x.getProdCatalogId());
            if (withStock) {
                productDTO.setStkQty(stockRepository
                        .findById(new StockId(x.getId().getProdCode(), x.getId().getProdCmpCode())).get().getStkQty());
            }
            productDTOList.add(productDTO);
        });
        return productDTOList;
    }

    @Override
    public List<ProductDTO> searchForAutocomplete(String keywords) {
        var productList = productRepository.searchByProdCodeLike(keywords);
        return convertProductTOProductDTO(productList);
    }

    @Override
    public List<ProductDTO> searchByProductCatalogNameAutocomplete(String keywords) {
        var list = productRepository.searchByProdNameLike(keywords);
        return convertProductTOProductDTO(list);
    }

    private List<ProductDTO> convertProductTOProductDTO(List<Product> products) {
        var productDTOList = new ArrayList<ProductDTO>();
        if (CommonUtil.isEmpty(products)) {
            return productDTOList;
        } else {
            products.forEach(x -> {
                var productDTO = new ProductDTO();
                productDTO.setProdCode(x.getId().getProdCode());
                productDTO.setProdCmpCode(x.getId().getProdCmpCode());
                productDTO.setProdName(x.getProdName());
                productDTO.setProdPrice(x.getProdPrice());
                productDTO.setProdCatalogId(x.getProdCatalogId());
                productDTOList.add(productDTO);
            });
        }
        return productDTOList;
    }
}
