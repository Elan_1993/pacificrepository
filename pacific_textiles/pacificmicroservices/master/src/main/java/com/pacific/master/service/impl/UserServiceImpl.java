package com.pacific.master.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.pacific.common.responsedto.ApiResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pacific.common.constants.StringConstants;
import com.pacific.common.exception.DulabException;
import com.pacific.common.service.CodeGeneratorService;
import com.pacific.common.util.CommonUtil;
import com.pacific.common.util.PasswordEncryptor;
import com.pacific.master.service.ICustomerService;
import com.pacific.master.service.IUserService;
import com.pacific.model.Customer;
import com.pacific.model.CustomerAddress;
import com.pacific.model.CustomerAddressId;
import com.pacific.model.User;
import com.pacific.repository.repo.UserRepository;

@Service
@Transactional
public class UserServiceImpl extends CodeGeneratorService implements IUserService {

	/** LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	/** userRepository. */
	private final UserRepository userRepository;

	/** userRepository. */
	private final ICustomerService customerService;

	/**
	 * This method is used the dependency injection.
	 * 
	 * @param userRepositoryIn  UserRepository
	 * @param customerServiceIn ICustomerService
	 */
	public UserServiceImpl(final UserRepository userRepositoryIn, final ICustomerService customerServiceIn) {
		this.userRepository = userRepositoryIn;
		this.customerService = customerServiceIn;
	}

	/**
	 *
	 * @param userIn user
	 * @return String
	 */
	@Override
	public ApiResponseDTO createUser(final User userIn) {
		if (CommonUtil.isEmpty(userIn.getUserCode())) {
			if (checkExistingUser(userIn)) {
				return new ApiResponseDTO("User Already Exist","",Boolean.FALSE);
			}
			userIn.setUserCode(generateCode(StringConstants.USER));
			userIn.setUserPassword(PasswordEncryptor.digest(userIn.getUserPassword(), userIn.getUserCode()));
		} else {
			var userDb = userRepository.findById(userIn.getUserCode());
			userIn.setUserPassword(userDb.get().getUserPassword());
		}
		userIn.setCreatedDatetime(new Date());
		userIn.setModifiedDatetime(new Date());
		var user = userRepository.save(userIn);
		return new ApiResponseDTO("User is successfully Created",user.getUserCode(),Boolean.TRUE);
	}

	/**
	 * Checking the existing user by mail or phone
	 * 
	 * @param userIn
	 * @return boolean
	 */
	private boolean checkExistingUser(final User userIn) {
		return userRepository.existsByUserPhoneOrUserMail(userIn.getUserPhone(), userIn.getUserMail());
	}

	/**
	 * User Registration from order screen
	 * 
	 * @param userIn
	 * @return userCode
	 */
	@Override
	public ApiResponseDTO registerUser(final User userIn) {
		var customer = setCustomerFromUser(userIn);
		if(!CommonUtil.isEmpty(customer)){
			customerService.saveCustomer(customer);
			userIn.setUserPassword(PasswordEncryptor.digest(userIn.getUserPassword(), userIn.getUserCode()));
			userIn.setGroupCode("Dummy");
			userIn.setIsUserActive(true);
			userIn.setIsCompanyUser(false);
			var userResponse = userRepository.save(userIn);
			return new ApiResponseDTO("User is successfully Created",userResponse.getUserCode(),Boolean.TRUE);
		}else{
			return new ApiResponseDTO("Given Mobile Number Or Email Already Exist","",Boolean.FALSE);
		}
	}

	/**
	 * @param user
	 * @return customer
	 */
	private Customer setCustomerFromUser(final User user) {
		var customer = new Customer();
		if (user != null) {
			if (checkExistingUser(user)) {
				return null;
			}
			if (CommonUtil.isEmpty(customer.getCusCode())) {
				customer.setCusCode(generateCode(StringConstants.CUSTOMER));
			}
			customer.setCusDob(user.getUserDob());
			customer.setCusEmailId(user.getUserMail());
			customer.setCusGender(user.getUserGender());
			customer.setCusMobile(user.getUserPhone());
			customer.setCusName(user.getUserName());
			user.setUserCode(customer.getCusCode());
			customer.setCusIsPaid(false);
			customer.setCusType(StringConstants.RETAILER);
			customer.setCreatedDatetime(new Date());
			customer.setModifiedDatetime(new Date());
		}
		return customer;
	}

	/**
	 * @param user
	 * @return customerAddressList
	 */
	private Set<CustomerAddress> setCustomerAddress(final User user) {
		var customerAddress = new CustomerAddress();
		var addressId = new CustomerAddressId();
		addressId.setCusCode(user.getUserCode());
		customerAddress.setId(addressId);
		customerAddress.setCusAdress1(user.getUserAddress());
		customerAddress.setCusCity(user.getUserCity());
		customerAddress.setCusPincode(user.getUserPincode());
		customerAddress.setCusState(user.getUserState());
		customerAddress.setCusCountry(user.getUserCountry());
		var customerAddressList = new HashSet<CustomerAddress>();
		customerAddressList.add(customerAddress);
		return customerAddressList;
	}

	/**
	 * Get the user by Id.
	 *
	 * @param userId userId.
	 * @return user user.
	 */
	@Override
	public User getUserById(final String userId) {
		var userDb = userRepository.findById(userId);
		if (userDb.isPresent()) {
			return userDb.get();
		} else {
			throw new DulabException("Record not found with id : " + userId);
		}
	}

	/**
	 * Fetch all user.
	 *
	 * @return list
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> fetchAllUser() {
		return userRepository.findAll();
	}

	/**
	 * Fetch all user by cmp code.
	 *
	 * @return User list
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> fetchUserByCmpCode(final String cmpCode) {
		return userRepository.findByUserCmpCodeAndIsUserActive(cmpCode, true);
	}

	/**
	 *
	 * @param userIn
	 * @return
	 */
	@Override
	public User resetPassword(User userIn) {
		var userDb =userRepository.findById(userIn.getUserCode());
		userDb.get().setUserPassword(PasswordEncryptor.digest(userIn.getUserPassword(), userDb.get().getUserCode()));
		return  userRepository.save(userDb.get());
	}
}
