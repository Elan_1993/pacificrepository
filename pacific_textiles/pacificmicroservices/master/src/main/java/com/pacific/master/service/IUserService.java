package com.pacific.master.service;

import com.pacific.common.responsedto.ApiResponseDTO;
import com.pacific.model.User;

import java.util.List;

public interface IUserService {

	/**
	 * This method is used to save the User.
	 * 
	 * @param user
	 * @return MasterResponse
	 */
	ApiResponseDTO createUser(User user);

	/**
	 * This method is used to save the User and customer.	 
	 * @param user
	 * @return String
	 */
	ApiResponseDTO registerUser(User user);

	/**
	 * Get user by Id.
	 * 
	 * @param userId
	 * @return user
	 */
	User getUserById(String userId);

	/**
	 * Fetch all user.
	 * 
	 * @return list
	 */
	List<User> fetchAllUser();
	
	/**
	 * @param cmpCode
	 * @return UserList
	 */
	List<User> fetchUserByCmpCode(String cmpCode);

	/**
	 * @param user
	 * @return user
	 */
	User resetPassword(User user);


}
