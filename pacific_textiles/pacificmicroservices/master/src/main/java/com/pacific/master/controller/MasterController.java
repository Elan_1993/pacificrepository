package com.pacific.master.controller;

import com.pacific.common.dto.ProductDTO;
import com.pacific.common.responsedto.ApiResponseDTO;
import com.pacific.master.service.IProductService;
import com.pacific.master.service.ICustomerService;
import com.pacific.master.service.IUserService;
import com.pacific.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * MasterController to get all the request for master data.
 * 
 * @author elanthiraiyan T
 */
@RestController
@RequestMapping(value = "/master/controller")
public class MasterController {

	/** userService. */
	private final IUserService userService;

	/** productService. */
	private final IProductService productService;

	/** customerService. */
	private final ICustomerService customerService;


	public MasterController(IUserService userService, IProductService productService, ICustomerService customerService) {
		this.userService = userService;
		this.productService = productService;
		this.customerService = customerService;
	}

	/**
	 * Method to save the New User info for the new Registration.
	 * 
	 * @param user user
	 * @return user user
	 */
	@PostMapping(value = "/save-user", produces = "application/json", consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ApiResponseDTO> createUser(final @RequestBody User user) {
		return new ResponseEntity<>(userService.createUser(user), HttpStatus.OK);
	}

	/**
	 * Method to save the New Ussave-customerer info for the new Registration.
	 *
	 * @param user user
	 * @return user user
	 */
	@PostMapping(value = "/reset-password", produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> resetPassword(final @RequestBody User user) {
		return new ResponseEntity<>(userService.resetPassword(user), HttpStatus.OK);
	}

	/**
	 * Method to get the User for the given id.
	 * 
	 * @param userId userId
	 * @return user user
	 */
	@GetMapping("/get-user/{userId}")
	public User getUserById(@PathVariable  final String userId) {
		return userService.getUserById(userId);
	}

	/**
	 * Method to get all the User.
	 * 
	 * @return list<User>
	 */
	@GetMapping("/fetch-all-user")
	public List<User> fetchAllUser() {
		return userService.fetchAllUser();
	}

	
	@GetMapping("/fetch-user-by-company/{cmpCode}")
	public List<User> fetchUserByCmpCode(final @PathVariable String cmpCode) {
		return userService.fetchUserByCmpCode(cmpCode);
	}

	/**
	 *
	 * @param productList productList.
	 * @return productList productList.
	 */
	@PostMapping(value = "/save-product", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponseDTO> saveProducts(@RequestBody final List<Product> productList) {
			return new ResponseEntity<>(productService.saveProductList(productList), HttpStatus.OK);
	}

	/**
	 * Method to get all the Catalog.
	 * 
	 * @return list<Priduct>
	 */
	@GetMapping("/fetch-all-catalogs")
	public List<Product> fetchAllCatalogs() {
		return productService.getAllCatalogs();
	}

	/**
	 * Method to get products by the CatalogId.
	 * 
	 * @return list<Product>
	 */
	@GetMapping("/get-product-by-catalog-id/{catalogId}")
	public List<Product> fetchProductsByCatalogId(final @PathVariable int catalogId) {
		return productService.findProductsByCatalogId(catalogId);
	}

	/**
	 * @param phoneOrEmail
	 * @return Customer
	 */
	@GetMapping(value = "/get-customer-by-id/{phoneOrEmail}", produces = "application/json")
	public Customer fetchCustomerById(final @PathVariable String phoneOrEmail) {
		return customerService.getCustomerByPhoneOrEmail(phoneOrEmail);
	}

	/**
	 * @param customer
	 * @return String
	 */
	@PostMapping("/save-customer")
	public ResponseEntity<ApiResponseDTO> saveCustomer(@RequestBody final Customer customer) {
        return new ResponseEntity<>(customerService.saveCustomer(customer), HttpStatus.OK);
	}

	/**
	 * Method to get all the Catalog.
	 * 
	 * @return list<Priduct>
	 */
	@GetMapping("/fetch-all-products")
	public List<Product> fetchAllProducts() {
		return productService.getAllProduct();
	}

	/**
	 * Method to get products by the CatalogId.
	 *
	 * @return list<Product>
	 */
	@GetMapping("/product-search/{productCode}/{withStock}")
	public List<ProductDTO> fetchProductsByCatalogId(final @PathVariable String productCode,
													 final @PathVariable Boolean withStock) {
		return productService.search(productCode, withStock);
	}

	/**
	 * Method to get products by the CatalogId.
	 *
	 * @return list<Product>
	 */
	@GetMapping("/product-search/{productCode}")
	public Product fetchProductsByProductId(final @PathVariable String productCode) {
		var productId = new ProductId(productCode,"pac");
		return productService.getProductById(productId);
	}

	/**
	 * Method to get products by the CatalogId.
	 *
	 * @return list<Product>
	 */
	@GetMapping("/product-autocomplete-search-product-name/{keyWord}")
	public List<ProductDTO> fetchProductAutocompleteSearchBYProductName(final @PathVariable String keyWord) {
		return productService.searchByProductCatalogNameAutocomplete(keyWord);
	}

	@PostMapping("/delete-product/{prodCode}/{prodCmpCode}")
	public ResponseEntity<ApiResponseDTO> deleteProduct(@PathVariable final String prodCode , @PathVariable final String prodCmpCode) {
		return new ResponseEntity<>(productService.deleteProduct(prodCode , prodCmpCode), HttpStatus.OK);
	}
}
