package com.pacific.master.service;

import com.pacific.common.dto.ProductDTO;
import com.pacific.common.responsedto.ApiResponseDTO;
import com.pacific.model.Product;
import com.pacific.model.ProductId;
import com.pacific.model.Brand; 
import com.pacific.model.Material;

import java.util.List;
import java.util.Map;

public interface IProductService {

	ApiResponseDTO saveProductList(List<Product> productList);

	List<Product> getAllProduct();

	Product getProductById(ProductId productId);

	ApiResponseDTO deleteProduct(String	 prodCode , String prodCmpCode);

	List<Product> getAllCatalogs();

	List<Product> findProductsByCatalogId(int catalogId);

	List<ProductDTO> search(String keywords,boolean withStock);

	List<ProductDTO> searchForAutocomplete(String keywords);

	List<ProductDTO> searchByProductCatalogNameAutocomplete(String keywords);

}
