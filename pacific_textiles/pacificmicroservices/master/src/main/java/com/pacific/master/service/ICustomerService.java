package com.pacific.master.service;

import com.pacific.common.responsedto.ApiResponseDTO;
import com.pacific.model.Customer;

public interface ICustomerService {
	/**
	 * @param customer
	 * @return
	 */
	ApiResponseDTO saveCustomer(Customer customer);

	/**
	 * @param Id
	 * @return
	 */
	Customer getCustomerByPhoneOrEmail(String Id);
}
