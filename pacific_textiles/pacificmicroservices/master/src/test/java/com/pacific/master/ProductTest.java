package com.pacific.master;

import com.pacific.master.service.IProductService;
import com.pacific.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductTest {


    @Autowired
    IProductService productService;

    /**
     * Test for login success.
     */
    @Test
    public void testProductSave() {
//        var product = new Product();
//        product.setPro
//        product.setDescription("123456789");
//        product.setCreatedDateTime(new Date());
//        product.setModifiedDateTime(new Date());
//        product.setPrice(80000d);
//		product.setCatalogeName("new Cataloge");
//		product.setCatalogeId(15);
//		product.setColor("blue");
//		product.setGender("f");
//		product.setCategory("Dress");
//		product.setSubCategory1("saree");
//		product.setSubCategory2("silk saree");
//		
//		var product1 = new Product();
//		product1.setName("1");
//        product1.setDescription("123456");
//        product1.setCreatedDateTime(new Date());
//        product1.setModifiedDateTime(new Date());
//        product1.setPrice(800d);
//		product1.setCatalogeName("new Cataloge1");
//		product1.setCatalogeId(16);
//		product1.setColor("red");
//		product1.setGender("m");
//		product1.setCategory("Accesories");
//		product1.setSubCategory1("bags");
//		product1.setSubCategory2("leather bag");
//        IProductService.saveProductList(List.of(product,product1));
    }

    /**
     * Test for login success.
     */
    @Test
    public void testProductCodeByLikeWithOutStocks() {
        var products = productService.search("1",false);
        System.out.println(products.size());
    }

    @Test
    public void testProductCodeByLikeWithStocks() {
        var products = productService.search("1",true);
        System.out.println(products.size());
    }


}
