package com.pacific.master;

import com.pacific.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserTest {

    /** restTemplate. */
    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Method to test get-user by given ID.
     */
//    @Test
//    public void testGetUserByID() {
//        var entity = createHttpEntity();
//        var response = restTemplate
//                .exchange("/master/controller/get-user/1", HttpMethod.GET, entity, User.class);
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//
//    }

    /**
     * Method to test fetchAllUser.
     */
    @Test
    public void testFetchALLUser() {
        var entity = createHttpEntity();
        var response = restTemplate
                .exchange("/master/controller/fetch-all-user", HttpMethod.POST, entity, List.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(Objects.requireNonNull(response.getBody())==null);

    }

    /**
     * Method to test fetchAllUser.
     */
    @Test
    public void testSaveUser() {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var user = new User();
        user.setUserName("Abdul1");
        user.setUserPhone("9999999999");
        user.setUserPassword("Admin@123");
        user.setUserMail("abdul@gmail.com");
        user.setIsCompanyUser(true);
        user.setIsUserActive(true);
        user.setUserGender("M");
        user.setUserDob(new Date());
        user.setIsLocked(false);
        user.setGroupCode("1");
        var entity = new HttpEntity<>(user, headers);
        var response = restTemplate
                .exchange("/master/controller/save-user", HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(Objects.requireNonNull(response.getBody())==null);

    }


    /**
     * This method is used create HttpEntity.
     * @return HttpEntity<Object>
     */
    private HttpEntity<Object> createHttpEntity() {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(headers);
    }
}
