package com.pacific.externalapi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ComponentScan("com.pacific.*")
public class ExternalReportTest {

    /** restTemplate. */
    @Autowired
    private TestRestTemplate restTemplate;

    /** accessToken. */
    private String accessToken;

    @BeforeEach
    public void init() {
        var url = "http://localhost:7001/oauth/token?grant_type=password&scope=user_info&username=9999999999"
                + "&password=Admin123";
        var response = restTemplate.postForEntity(url, null, Map.class);
        accessToken = Objects.requireNonNull(response.getBody()).get("access_token").toString();
    }

    /**
     * Test method for logout.
     */
    @Test
    public void testStockReportTable() {
        var entity = createHttpEntity();
        var response = restTemplate.exchange("/external.api/report/fetch-stock-report", HttpMethod.GET, entity, List.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(Objects.requireNonNull(response.getBody()).isEmpty());
    }

    /**
     * This method is used create HttpEntity.
     *
     * @return HttpEntity<Object>
     */
    private HttpEntity<Object> createHttpEntity() {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(accessToken);
        return new HttpEntity<>(headers);
    }
}
