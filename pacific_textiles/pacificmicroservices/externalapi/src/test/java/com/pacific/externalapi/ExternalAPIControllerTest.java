package com.pacific.externalapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.pacific.common.dto.LoginDTO;
import com.pacific.common.dto.UserInfoDTO;
import com.pacific.model.User;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ComponentScan("com.pacific.*")
public class ExternalAPIControllerTest {

	/** restTemplate. */
	@Autowired
	private TestRestTemplate restTemplate;

	/** accessToken. */
	private String accessToken;

	@BeforeEach
	public void init() {
		var url = "/oauth/token?grant_type=password&scope=user_info&username=user8"
				+ "&password=Admin@123";
		var response = restTemplate.postForEntity(url, null, Map.class);
		accessToken = Objects.requireNonNull(response.getBody()).get("access_token").toString();
	}

	/**
	 * Test for login success.
	 */
	@Test
	public void testLoginSuccess() {
		var url = "/oauth/token?grant_type=password&scope=user_info&username=user8"
				+ "&password=Admin@123";
		var response = restTemplate.postForEntity(url, null, Map.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNotNull(Objects.requireNonNull(response.getBody()).get("access_token"));
	}

	/**
	 * Test for login fail.
	 */
	@Test
	public void testLoginFail() {
		var url = "/oauth/token?grant_type=password&scope=user_info&username=fgfgfg" + "&password=hghghghhh";
		var response = restTemplate.postForEntity(url, null, Map.class);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	/**
	 * Method to test login
	 */
	@Test
	public void testLogin() {
		var headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		var dto = new LoginDTO();
		dto.setUserCode("User8");
		dto.setPassword("Admin@123");
		dto.setCompanyUser(Boolean.TRUE);
		var entity = new HttpEntity<>(dto, headers);

		var response = restTemplate.exchange("/external.api/login", HttpMethod.POST, entity, UserInfoDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertFalse(Objects.requireNonNull(response.getBody()) == null);

	}

	/**
	 * Method to test save-user-info.
	 */
	@Test
	public void testSaveUserInfo() {
		var headers = new HttpHeaders();
		var user = new User();
		user.setUserName("Elan");
		user.setUserPhone("9999999999");
		user.setUserPassword("Admin@123");
		user.setUserMail("elan@gmail.com");
		user.setIsCompanyUser(true);
		user.setIsSuperAdmin(true);
		user.setIsUserActive(true);
		user.setUserGender("M");
		user.setUserDob(new Date());
		user.setIsLocked(false);
		user.setGroupCode("Test");
		var entity = new HttpEntity<>(user, headers);

		var response = restTemplate.exchange("/external.api/user-register", HttpMethod.POST, entity, String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	/**
	 * Test method for logout.
	 */
	@Test
	public void testLogout() {
		var entity = createHttpEntity();
		var response = restTemplate.exchange("/external.api/logout", HttpMethod.POST, entity, Void.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	/**
	 * This method is used create HttpEntity.
	 * 
	 * @return HttpEntity<Object>
	 */
	private HttpEntity<Object> createHttpEntity() {
		var headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		return new HttpEntity<>(headers);
	}

	/**
	 * Test method for logout.
	 */
	@Test
	public void testFetchCodeTable() {
		var entity = createHttpEntity();
		var response = restTemplate.exchange("/external.api/fetch-code-table", HttpMethod.GET, entity, List.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertFalse(Objects.requireNonNull(response.getBody()).isEmpty());
	}

	/**
	 * Test method for Group Code.
	 */
	@Test
	public void testGroupCodeFetch() {
		var entity = createHttpEntity();
		var response = restTemplate.exchange("/external.api/load-user-groups", HttpMethod.GET, entity, List.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertFalse(Objects.requireNonNull(response.getBody()).isEmpty());
	}

	/**
	 * Test method for Product category.
	 */
	@Test
	public void testProductCategoryFetch() {
		var entity = createHttpEntity();
		var response = restTemplate.exchange("/external.api/fetch-product-category", HttpMethod.GET, entity, List.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertFalse(Objects.requireNonNull(response.getBody()).isEmpty());
	}

}
