package com.pacific.externalapi;

import com.pacific.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserTest {

    /** restTemplate. */
    @Autowired
    private TestRestTemplate restTemplate;

    /** accessToken. */
    private String accessToken;

    @BeforeEach
    public void init() {
        var url = "/oauth/token?grant_type=password&scope=user_info&username=SuperAdmin"
                + "&password=Admin@123";
        var response = restTemplate.postForEntity(url, null, Map.class);
        accessToken = Objects.requireNonNull(response.getBody()).get("access_token").toString();
    }

    /**
     * Method to test get-user by given ID.
     */
    @Test
    public void testGetUserByID() {
        var entity = createHttpEntity();
        var response = restTemplate
                .exchange("/external.api/get-user/1", HttpMethod.GET, entity, User.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    /**
     * Method to test fetchAllUser.
     */
    @Test
    public void testFetchALLUser() {
        var entity = createHttpEntity();
        var response = restTemplate
                .exchange("/external.api/fetch-all-user", HttpMethod.POST, entity, List.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(Objects.requireNonNull(response.getBody())==null);

    }

    /**
     * This method is used create HttpEntity.
     * @return HttpEntity<Object>
     */
    private HttpEntity<Object> createHttpEntity() {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(accessToken);
        return new HttpEntity<>(headers);
    }
}
