package com.pacific.externalapi.storageaction;

import com.pacific.common.util.CommonUtil;
import com.pacific.externalapi.controller.ExternalWebController;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static com.pacific.common.constants.StringConstants.PRODUCT_IMAGES;


@Service("localStorage")
public class FilesStorageServiceImpl extends IFilesStorageService {

    @PostConstruct
    public void init() {
        var file = new File(outputPath + File.separatorChar + PRODUCT_IMAGES + File.separatorChar);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    @Override
    public void upload(final MultipartFile file, final String cmpCode, final Integer catalogId) {
        try {
            if (!CommonUtil.isEmpty(saveProductImage(file, cmpCode, catalogId , MvcUriComponentsBuilder
                    .fromMethodName(ExternalWebController.class, "getFile", cmpCode, catalogId, file.getOriginalFilename()).build().toString()))) {
                Files.copy(file.getInputStream(), generatePath(cmpCode, catalogId).
                        toPath().resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public Resource load(String filename, String cmpCode, Integer catalogId) {
        try {
            Path file = generatePath(cmpCode, catalogId).toPath().resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    private File generatePath(String cmpCode, Integer catalogId) {
        var file = new File(outputPath + File.separatorChar + PRODUCT_IMAGES + File.separatorChar + cmpCode + File.separatorChar);
        if (!file.exists()) {
            file.mkdir();
        }
        var file2 = new File(outputPath + File.separatorChar + PRODUCT_IMAGES +
                File.separatorChar + cmpCode + File.separatorChar + catalogId + File.separatorChar);

        if (!file2.exists()) {
            file2.mkdir();
        }
        return file2;
    }

}
