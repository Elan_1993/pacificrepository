package com.pacific.externalapi.controller;

import com.pacific.common.dto.DashBoardDTO;
import com.pacific.common.dto.ProductSearchCountDTO;
import com.pacific.common.dto.StockReportDTO;
import com.pacific.externalapi.service.IReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/external.api/report")
public class ReportController {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ReportController.class);

    /**
     * service.
     */
    private final IReportService service;

    /**
     *
     * @param serviceIn
     */
    public ReportController(final IReportService serviceIn) {
        this.service = serviceIn;
    }

    /**
     * This method is used to fetch stock report details.
     *
     */
    @GetMapping(value = "/fetch-stock-report")
    public final List<StockReportDTO> fetchStockReportData() {
        return service.generateStockReport();
    }

    /**
     * This method is used to fetch product search count details.
     *
     */
    @GetMapping(value = "/fetch-product-search-count")
    public final List<ProductSearchCountDTO> fetchProductSearchCountDTO() {
        return service.generateProductSearchCountDTO();
    }

    /**
     * This method is used to fetch product search count details.
     *
     */
    @GetMapping(value = "/fetch-dashboard-data")
    public final Map<String,List<DashBoardDTO>> fetchDashBoardData() {
        return service.generateDashBoard();
    }


}
