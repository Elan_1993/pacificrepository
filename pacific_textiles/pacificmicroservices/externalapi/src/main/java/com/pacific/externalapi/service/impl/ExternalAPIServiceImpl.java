package com.pacific.externalapi.service.impl;

import com.pacific.common.constants.StringConstants;
import com.pacific.common.dao.IDAORepository;
import com.pacific.common.dto.*;
import com.pacific.common.service.IQueryService;
import com.pacific.externalapi.service.IExternalAPIService;
import com.pacific.model.Category;
import com.pacific.model.CodeTable;
import com.pacific.repository.repo.CategoryRepository;
import com.pacific.repository.repo.CodeTableRepository;
import com.pacific.repository.repo.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.pacific.common.constants.QueryNames.*;
import static com.pacific.common.constants.StringConstants.*;

@Service
@Transactional
public class ExternalAPIServiceImpl implements IExternalAPIService {

	/**
	 * LOG.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ExternalAPIServiceImpl.class);

	/**
	 * dao.
	 */
	private final IDAORepository dao;

	/**
	 * queryService.
	 */
	private final IQueryService queryService;

	/** codeTableRepository. */
	private final CodeTableRepository codeTableRepository;


	@Autowired private CategoryRepository categoryRepository;

	@Autowired private CustomerRepository customerRepository;


	/**
	 * Constructor to inject dependency.
	 *
	 * @param daoIn          DAORepository
	 * @param queryServiceIn QueryService
	 */
	public ExternalAPIServiceImpl(final IDAORepository daoIn, final IQueryService queryServiceIn,
                                  final CodeTableRepository codeTableRepositoryIn) {
		this.dao = daoIn;
		this.queryService = queryServiceIn;
		this.codeTableRepository = codeTableRepositoryIn;
	}

	/**
	 * This method is used to validate user login.
	 *
	 * @param username        unique user name
	 * @param password        user password
	 * @param clientIpAddress user password
	 */
	@Override
	public void login(String username, String password, String clientIpAddress) {
		var userInfo = dao.queryForObjectWithRowMapper(queryService.get(USER_LOGIN), UserInfoDTO.class, username,
				username, username, password);
		dao.update(queryService.get(INSERT_USER_LOGIN_LOG), username, new Date(), clientIpAddress, true, null);
	}

	/**
	 * This method is used to get user details.
	 *
	 * @param loginDTO Login
	 * @return User information
	 */
	@Override
	public UserInfoDTO getUserInfo(LoginDTO loginDTO) {
		try {
			// Load user info
			var userInfoDTO = dao.queryForObjectWithRowMapper(queryService.get(USER_INFO), UserInfoDTO.class,
					loginDTO.getUserCode(), loginDTO.getUserCode(), loginDTO.getUserCode());
			userInfoDTO.setLoginCode(loginDTO.getUserCode());
			userInfoDTO.setUserType(Integer.valueOf(DEALER));
			if (loginDTO.getCompanyUserMenu()) {
				// Load Screens
				userInfoDTO.setScreenList(loadScreenDTO(userInfoDTO.getUserCode()));
				// Load User Roles (Screen access)
				userInfoDTO.setUserRoleAccess(dao.query(queryService.get(SELECT_ROLE_ACCESS), RoleAccessDTO.class,
						userInfoDTO.getGroupCode()));
			}else{
				if(!userInfoDTO.isCompanyUser()){
				  var customer = customerRepository.findById(userInfoDTO.getUserCode());
				  if(customer.isPresent()){
				  	  userInfoDTO.setPaidUser(customer.get().getCusIsPaid());
					  userInfoDTO.setUserType(customer.get().getCusType());
				  }
				}
			}
			userInfoDTO.setLoginStatus(true);
			return userInfoDTO;
		} catch (Exception e) {
			LOG.error("Login failed", e);
			return new UserInfoDTO("", "", false, "Username/Password incorrect!");
		}
	}

	/**
	 * This method is used to load screens.
	 *
	 * @param userCode User Code
	 * @return List of screens
	 */
	private List<ScreenDTO> loadScreenDTO(final String userCode) {
		var screenMap = new TreeMap<Integer, ScreenDTO>();
		var subMenuMap = new TreeMap<String, ScreenDTO>();

		var screenMapList = dao.queryForList(queryService.get(SELECT_SCREENS), userCode);
		for (var screen : screenMapList) {
			var moduleNo = Integer.valueOf(screen.get(MODULENO).toString());
			if ((boolean) screen.get(SUB_MENU) && screenMap.containsKey(moduleNo)) {
				loadScreensForNewlyAddedScreenDTO(screenMap, subMenuMap, screen);
			} else if ((boolean) screen.get(SUB_MENU) && !screenMap.containsKey(moduleNo)) {
				loadScreensForExistingScreenDTO(screenMap, subMenuMap, screen);
			} else {
				var item = getScreenDTO(screen, moduleNo, MODULE_NAME, MODULE_ICON);
				screenMap.put(moduleNo, item);
			}
		}
		return new ArrayList<>(screenMap.values());
	}

	/**
	 * This method is used to load screens.
	 *
	 * @param screenMap  User Code
	 * @param subMenuMap User Code
	 * @param screen     User Code
	 */
	private void loadScreensForNewlyAddedScreenDTO(final TreeMap<Integer, ScreenDTO> screenMap,
			final TreeMap<String, ScreenDTO> subMenuMap, final Map<String, Object> screen) {
		var moduleNo = Integer.valueOf(screen.get(MODULENO).toString());
		var subMenuName = screen.get(SUB_MENU_NAME);

		var screenDTO = screenMap.get(moduleNo);
		var item = getScreenDTO(screen, moduleNo, SCREEN_NAME, SCREEN_ICON);
		if (subMenuName != null && subMenuMap.containsKey(moduleNo + "_" + subMenuName)) {
			var subItem = subMenuMap.get(moduleNo + "_" + subMenuName);
			subItem.getItems().add(item);
		} else if (subMenuName != null && !subMenuMap.containsKey(moduleNo + "_" + subMenuName)) {
			var subItem = getSubScreenDTO(screen);
			subItem.getItems().add(item);
			screenDTO.getItems().add(subItem);
			subMenuMap.put(moduleNo + "_" + subMenuName.toString(), subItem);
		} else {
			screenDTO.getItems().add(item);
		}

	}

	/**
	 * This method is used to load screens.
	 *
	 * @param screenMap  User Code
	 * @param subMenuMap User Code
	 * @param screen     User Code
	 */
	private void loadScreensForExistingScreenDTO(final TreeMap<Integer, ScreenDTO> screenMap,
			final TreeMap<String, ScreenDTO> subMenuMap, final Map<String, Object> screen) {
		var moduleNo = Integer.valueOf(screen.get(MODULENO).toString());
		var subMenuName = screen.get(SUB_MENU_NAME);
		var screenDTO = new ScreenDTO();
		screenDTO.setLabel((String) screen.get(MODULE_NAME));
		screenDTO.setIcon((String) screen.get(MODULE_ICON));
		var item = getScreenDTO(screen, moduleNo, SCREEN_NAME, SCREEN_ICON);

		if (subMenuName != null && subMenuMap.containsKey(moduleNo + "_" + subMenuName)) {
			var subItem = subMenuMap.get(moduleNo + "_" + subMenuName);
			subItem.getItems().add(item);
		} else if (subMenuName != null && !subMenuMap.containsKey(moduleNo + "_" + subMenuName)) {
			var subItem = getSubScreenDTO(screen);
			subItem.getItems().add(item);
			var subItems = new ArrayList<ScreenDTO>();
			subItems.add(subItem);
			screenDTO.setItems(subItems);
			screenMap.put(moduleNo, screenDTO);
			subMenuMap.put(moduleNo + "_" + subMenuName.toString(), subItem);
		} else {
			var items = new ArrayList<ScreenDTO>();
			items.add(item);
			screenDTO.setItems(items);
			screenMap.put(moduleNo, screenDTO);
		}
	}

	/**
	 * This method is used to create new screen object.
	 *
	 * @param screen     Screen details
	 * @param moduleNo   Module number
	 * @param screenName Screen name
	 * @param screenIcon Screen Icon
	 * @return New ScreenDTO
	 */
	private ScreenDTO getScreenDTO(final Map<String, Object> screen, final Integer moduleNo, final String screenName,
			final String screenIcon) {
		var item = new ScreenDTO();
		item.setModuleNo(moduleNo);
		item.setScreenNo(Integer.parseInt(screen.get(SCREENNO).toString()));
		item.setLabel((String) screen.get(screenName));
		item.setIcon((String) screen.get(screenIcon));
		item.setRouterLink((String) screen.get(ROUTER_LINK));
		item.setSubMenuName((String) screen.get(SUB_MENU_NAME));
		return item;
	}

	/**
	 * This method is used to create new screen object.
	 *
	 * @param screen Screen details
	 * @return New SubScreenDTO
	 */
	private ScreenDTO getSubScreenDTO(final Map<String, Object> screen) {
		var screenItem = new ScreenDTO();
		screenItem.setModuleNo(0);
		screenItem.setScreenNo(0);
		screenItem.setLabel((String) screen.get(SUB_MENU_NAME));
		screenItem.setIcon((String) screen.get(SCREEN_ICON));
		screenItem.setRouterLink(null);
		screenItem.setItems(new ArrayList<>());
		return screenItem;
	}

	/**
	 * This method is used for logout the users.
	 * 
	 * @param userCode userCode
	 */
	@Override
	public void logout(final String userCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("user_code", userCode);
		params.put("is_logged_in", false);
		params.put("logout_time", new Date());
		dao.updateWithParams(queryService.get(UPDATE_USER_LOGOUT_LOG), params);
	}

	/**
	 * @return codeTableList
	 */
	@Override
	public List<CodeTable> fetchAllCodeTableList() {
		return codeTableRepository.findAll();
	}

	/**
	 * This method is used to load user group's.
	 * 
	 * @return User group's
	 */
	@Override
	public List<UserGroupDTO> getUserGroups() {
		return dao.query(queryService.get(SELECT_USER_GROUP), UserGroupDTO.class);
	}


	@Override
	public DropDownValuesDTO getAllDropDowns() {
		var dropDownValuesDTO =  new DropDownValuesDTO();
		dropDownValuesDTO.setBrandList(dao.query(queryService.get(SELECT_BRAND), DropDownDTO.class));
		dropDownValuesDTO.setMaterialList(dao.query(queryService.get(SELECT_MATERIAL), DropDownDTO.class));
		return dropDownValuesDTO;
	}
   

    /**
     * This method is used to load Product Category.
     *
     * @return Category's
     */
    @Override
    public List<Category> fetchAllLeastCategory() {
        return categoryRepository.fetchAllLeastCategory();
    }

	/**
	 * @param tableName
	 * @return codeTableList
	 */
	@Override
	public List<CodeTable> fetchAllCodeTableListByTableName(String tableName) {
		return codeTableRepository.findByIdTableName(tableName);
	}

	@Override
	public DropDownValuesDTO getSateCourierDropDowns(Integer countryCode) {
		var dropDownValuesDTO =  new DropDownValuesDTO();
		dropDownValuesDTO.setCourierList(dao.query(queryService.get("SELECT_COURIER"), DropDownDTO.class));
		dropDownValuesDTO.setStateList(dao.query(queryService.get("SELECT_STATE"),Map.of("countryId",countryCode),DropDownDTO.class));
		Map<String, Object> params = new HashMap<>();
		params.put("stateList", dropDownValuesDTO.getStateList().stream().map(DropDownDTO::getValue).collect(Collectors.toList()));
		params.put("courierList", dropDownValuesDTO.getCourierList().stream().map(DropDownDTO::getValue).collect(Collectors.toList()));
		var courier_price_List = dao.queryWithRSExtractor(params,queryService.get("SELECT_COURIER_PRICE"));
		dropDownValuesDTO.setCourierPriceList(courier_price_List.stream()
				.collect(Collectors.toMap(e -> (String) e[0], e -> (BigDecimal) e[1])));
		return dropDownValuesDTO;
	}
}
