package com.pacific.externalapi.service;

import com.pacific.common.dto.LoginDTO;
import com.pacific.common.dto.UserGroupDTO;
import com.pacific.common.dto.UserInfoDTO;
import com.pacific.common.dto.DropDownValuesDTO;
import com.pacific.model.Category;
import com.pacific.model.CodeTable;
import java.util.List;

public interface IExternalAPIService {

	/**
	 * This method is used to validate user login.
	 * 
	 * @param username        unique user name
	 * @param password        user password
	 * @param clientIpAddress user password
	 */
	void login(String username, String password, String clientIpAddress);

	/**
	 * This method is used to get user details.
	 * 
	 * @param loginDTO Login
	 * @return User information
	 */
	UserInfoDTO getUserInfo(LoginDTO loginDTO);

	/**
	 * This method is used for Logout.
	 * 
	 * @param userCode userCode
	 */
	void logout(String userCode);

	/**
	 *
	 * @return codeTableList
	 */
	List<CodeTable> fetchAllCodeTableList();

	/**
	 * This method is used to load user group's.
	 * 
	 * @return User group's
	 */
	List<UserGroupDTO> getUserGroups();

	/**
	 * This method is used to get drop downs
	 */
	DropDownValuesDTO getAllDropDowns();

	/**
	 * This method is used to load Product Category.
	 * 
	 * @return Category's
	 */
	List<Category> fetchAllLeastCategory();

	/**
	 *
	 * @return codeTableList
	 */
	List<CodeTable> fetchAllCodeTableListByTableName(String tableName);

	/**
	 * This method is used to get drop downs
	 */
	DropDownValuesDTO getSateCourierDropDowns(Integer countryCode);


}
