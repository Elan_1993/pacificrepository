package com.pacific.externalapi.service;

import com.pacific.common.dto.CustomInputDTO;
import com.pacific.common.dto.ProductDTO;

import java.util.List;
import java.util.Map;

public interface IExternalProductService {

    /**
     *
     * @param customInputDTO
     * @return
     */
    List<ProductDTO> getProductInfo(CustomInputDTO customInputDTO);

    /**
     * This method is used to build WHERE clause for custom report.
     * @param inputDTO Input details
     * @param params   Query parameters
     * @return WHERE clause for query
     */
    String buildWhereClause(CustomInputDTO inputDTO, Map<String, Object> params);

    /**
     *
     * @param prodCode
     * @param cmpCode
     * @param customerType
     * @return
     */
    ProductDTO getProductInfoByProductCode(String prodCode,String cmpCode,Integer customerType);

    /**
     *
     * @return List<ProductDTO>
     */
    Map<String ,List<ProductDTO>> fetchProductsForDashboard(CustomInputDTO customInputDTO);



    /**
     *
     * @param customInputDTO Input Details
     * @return Total Count
     */
    long getTotalRecords(CustomInputDTO customInputDTO);
}
