package com.pacific.externalapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;
import java.util.Objects;

@SpringBootApplication(scanBasePackages = "com.pacific.*")
@EnableResourceServer
@EntityScan("com.pacific.model")
@EnableJpaRepositories("com.pacific.repository")
@EnableScheduling
public class ExternalApiApplication extends WebSecurityConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(ExternalApiApplication.class, args);
    }


    /**
     * This method is used to create a JDBC token store.
     *
     * @param dataSourceInput JDBC data source
     * @return returns {@link JdbcTokenStore}
     */
    @Bean("jdbcTokenStore")
    public TokenStore tokenStore(final DataSource dataSourceInput) {
        return new JdbcTokenStore(Objects.requireNonNull(dataSourceInput));
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/external.api/controller/**").antMatchers("/actuator/**");
    }
}
