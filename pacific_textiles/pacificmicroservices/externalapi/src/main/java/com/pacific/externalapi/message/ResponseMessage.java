package com.pacific.externalapi.message;

/**
 * @author elanthiraiyan T
 */
public class ResponseMessage {

    private String message;

    /**
     *
     * @param message
     */
    public ResponseMessage(String message) {
        this.message = message;
    }

    /**
     * @return the message
     */
    public final String getMessage() {
        return message;
    }

    /**
     * @param messageIn the message to set
     */
    public final void setMessage(final String messageIn) {
        message = messageIn;
    }
}
