package com.pacific.externalapi.controller;

import com.pacific.common.dto.*;
import com.pacific.externalapi.message.ResponseMessage;
import com.pacific.externalapi.service.IExternalAPIService;
import com.pacific.externalapi.storageaction.IFilesStorageService;
import com.pacific.model.Category;
import com.pacific.model.CodeTable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/external.api")
public class ExternalAPIController {

    /**
     * context.
     */
    private final ApplicationContext context;

    /**
     * service.
     */
    private final IExternalAPIService service;

    /**
     * tokenStore.
     */
    private final TokenStore tokenStore;

    private final IFilesStorageService filesStorageService;

    /**
     * Constructor to inject dependency.
     *
     * @param serviceIn     {@link IExternalAPIService}
     * @param tokenStoreIn  {@link TokenStore}
     */
    public ExternalAPIController(final IExternalAPIService serviceIn,
                                 final @Qualifier("jdbcTokenStore") TokenStore tokenStoreIn,
                                 final ApplicationContext contextIn) {
        this.service = serviceIn;
        this.tokenStore = tokenStoreIn;
        this.context = contextIn;
        filesStorageService = context.getBean("s3Storage", IFilesStorageService.class);
    }


    /**
     * This method is used to get user details.
     *
     * @param loginDTO Login
     * @return User information
     */
    @PostMapping(value = "/get-user-info", produces = "application/json", consumes = "application/json")
    public final UserInfoDTO getUserInfo(@RequestBody final LoginDTO loginDTO) {
        return service.getUserInfo(loginDTO);
    }

    /**
     * This method is used to logout from oauth session.
     *
     * @param request Report input details
     */
    @PostMapping(value = "/logout")
    public final void logout(final HttpServletRequest request) {
        service.logout(request.getUserPrincipal().getName());
        var authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            var tokenValue = authHeader.replace("Bearer", "").trim();
            var accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
        }
    }

    /**
     * This method is used to fetch code table details.
     */
    @GetMapping(value = "/fetch-code-table")
    public final List<CodeTable> fetchCodeTable() {
        return service.fetchAllCodeTableList();
    }

    /**
     * This method is used to load user group's.
     *
     * @return User group's
     */
    @GetMapping("/load-user-groups")
    public final List<UserGroupDTO> getUserGroups() {
        return service.getUserGroups();
    }

    @GetMapping("/load-all-dropdowns")
    public final DropDownValuesDTO getAllDropDowns() { return service.getAllDropDowns();}

    /**
     * This method is used to fetch code table details.
     */
    @GetMapping(value = "/fetch-product-category")
    public final List<Category> fetchProductCategory() {
        return service.fetchAllLeastCategory();
    }

    /**
     * This method is used to fetch code table details.
     */
    @GetMapping(value = "/fetch-code-table/{tableName}")
    public final List<CodeTable> fetchCodeTableByTableName(final @PathVariable String tableName) {
        return service.fetchAllCodeTableListByTableName(tableName);
    }

    @GetMapping("/files/{cmpCode}/{catalogId}")
    public ResponseEntity<List<FileInfoDTO>> getListFiles(final @PathVariable String cmpCode,
                                                          final @PathVariable Integer catalogId) {
        List<FileInfoDTO> fileInfos = filesStorageService.loadAll(cmpCode, catalogId);
        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }

    @PutMapping("/upload/{cmpCode}/{catalogId}")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file,
                                                      final @PathVariable String cmpCode,
                                                      final @PathVariable Integer catalogId) {
        String message = "";
        try {

            filesStorageService.upload(file, cmpCode, catalogId);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }
}
