package com.pacific.externalapi.service;

import com.pacific.common.dto.DashBoardDTO;
import com.pacific.common.dto.ProductSearchCountDTO;
import com.pacific.common.dto.StockReportDTO;

import java.util.List;
import java.util.Map;

public interface IReportService {

    List<StockReportDTO> generateStockReport();

    List<ProductSearchCountDTO> generateProductSearchCountDTO();

    Map<String,List<DashBoardDTO>> generateDashBoard();
}
