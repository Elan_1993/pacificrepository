package com.pacific.externalapi.storageaction;

import com.pacific.common.constants.StringConstants;
import com.pacific.common.dto.FileInfoDTO;
import com.pacific.externalapi.config.S3Config;
import com.pacific.model.ProductImages;
import com.pacific.model.ProductImagesId;
import com.pacific.repository.repo.ProductImagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

public abstract class  IFilesStorageService {

    @Value("${output.path}")
    public  String outputPath;

    @Autowired
    public ProductImagesRepository productImagesRepository;

    @Autowired
    public S3Config amazonS3;

    @Value("${s3.bucket.name}")
    public String bucketName;

    @Value("${s3.common.path}")
    public String s3CommonPath;

    /**
     *
     * @param file
     * @param cmpCode
     * @param catalogId
     */
    public abstract void upload(MultipartFile file, String cmpCode, Integer catalogId);

    public abstract Resource load(String filename, String cmpCode, Integer catalogId);

    public List<FileInfoDTO> loadAll(String cmpCode, Integer catalogId){
        var productImagesList = productImagesRepository.findByIdProdCmpCodeAndProdCatalogId(cmpCode,catalogId);
        return productImagesList.parallelStream().map(path -> {
            return new FileInfoDTO(path.getOriginalFileName(), path.getImagePath());
        }).collect(Collectors.toList());
    }

    public String saveProductImage(final MultipartFile file, final String cmpCode, final Integer catalogId , String imagePath) {
        String originalFileName = file.getOriginalFilename().toString();
        var fileName = originalFileName.split("[.]");
        var productImageId = new ProductImagesId();
        var productImage = new ProductImages();
        productImage.setImagePath(imagePath);
        productImage.setProdCatalogId(catalogId);
        productImage.setOriginalFileName(originalFileName);
        productImage.setImageType(file.getContentType());
        productImageId.setProdCmpCode(cmpCode);
        productImageId.setProdCode(fileName[0]);
        productImage.setId(productImageId);
        var save = productImagesRepository.save(productImage);
        return save.getId().getProdCode();
    }

    public String generatePath(String fileName, String cmpCode, Integer catalogId) {
        StringBuilder path = new StringBuilder();
        path.append(StringConstants.PRODUCT_IMAGES.toLowerCase() + "/");
        path.append(cmpCode + "/");
        path.append(catalogId + "/");
        path.append(fileName);
        return path.toString();
    }
}
