package com.pacific.externalapi.service.impl;

import com.pacific.common.constants.NumericConstants;
import com.pacific.common.constants.StringConstants;
import com.pacific.common.dao.IDAORepository;
import com.pacific.common.dto.CustomInputDTO;
import com.pacific.common.dto.ProductDTO;
import com.pacific.common.exception.DulabException;
import com.pacific.common.service.IQueryService;
import com.pacific.common.util.CommonUtil;
import com.pacific.externalapi.service.IExternalProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

import static com.pacific.common.constants.QueryNames.SELECT_PRODUCT_INFO;
import static com.pacific.common.constants.QueryNames.INSERT_PRODUCT_SEARCH_COUNT;
import static com.pacific.common.constants.StringConstants.WHERE;
import static com.pacific.common.constants.StringConstants.ORDER_BY;
import static com.pacific.common.constants.StringConstants.QUERY_AND;
import static com.pacific.common.constants.StringConstants.NUMBER_FOUR;


/**
 * @author elan
 */
@Service
@Transactional
public class ExternalProductServiceImpl implements IExternalProductService {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ExternalProductServiceImpl.class);

    /**
     * dao.
     */
    private final IDAORepository dao;

    /**
     * queryService.
     */
    private final IQueryService queryService;

    /**
     * @param dao
     * @param queryService
     */
    public ExternalProductServiceImpl(IDAORepository dao, IQueryService queryService) {
        this.dao = dao;
        this.queryService = queryService;
    }

    /**
     * @param customInputDTO
     * @return
     */
    @Override
    public List<ProductDTO> getProductInfo(CustomInputDTO customInputDTO) {
        try {
            var params = new HashMap<String, Object>();
            var query = queryService.get(SELECT_PRODUCT_INFO);
            // build where conditions
            var whereClause = buildWhereClause(customInputDTO, params);
            if (!whereClause.trim().isEmpty()) {
                query += "\n" + WHERE + whereClause;
            }
            // order by
            query += "\n" + ORDER_BY + "products.prod_code";
            // add limit
            if (customInputDTO.getRows() != 0) {
                query += "\n LIMIT " + customInputDTO.getFirst() + " , " + customInputDTO.getRows();
            }
            // execute query
            return dao.query(query, params, ProductDTO.class);

        } catch (Exception e) {
            throw new DulabException(e);
        }
    }

    /**
     * This method is used to build WHERE clause for custom report.
     *
     * @param inputDTO Input details
     * @param params   Query parameters
     * @return WHERE clause for query
     */
    @Override
    public String buildWhereClause(final CustomInputDTO inputDTO, final Map<String, Object> params) {
        var whereClause = new StringBuilder();
        // add user filter
        params.put("cusType", CommonUtil.isEmpty(inputDTO.getCusType()) ? Integer.valueOf(StringConstants.DEALER) : inputDTO.getCusType());
        if (inputDTO.getConditions() != null) {

            if (inputDTO.getConditions().getCategory() != null) {
                if (whereClause.length() != NumericConstants.ZERO) {
                    whereClause.append(QUERY_AND);
                }
                params.put(inputDTO.getConditions().getCategory(), inputDTO.getConditions().getCategory());
                whereClause.append("cat_name").append(" = :").append(inputDTO.getConditions().getCategory());
            }

            if (inputDTO.getConditions().getProdCatalogId() != null) {
                if (whereClause.length() != NumericConstants.ZERO) {
                    whereClause.append(QUERY_AND);
                }
                params.put(inputDTO.getConditions().getProdCatalogId().toString(),
                        inputDTO.getConditions().getProdCatalogId());
                whereClause.append("products.prod_catalog_id").append(" = :").append(inputDTO.getConditions().getProdCatalogId().toString());
            }

            if (inputDTO.getConditions().getProdExcludeCode() != null) {
                if (whereClause.length() != NumericConstants.ZERO) {
                    whereClause.append(QUERY_AND);
                }
                params.put(inputDTO.getConditions().getProdExcludeCode(), inputDTO.getConditions().getProdExcludeCode());
                whereClause.append("products.prod_code").append(" != :").append(inputDTO.getConditions().getProdExcludeCode());
            }


            if (inputDTO.getConditions().isStkQtyNonZeroEnable()) {
                if (whereClause.length() != NumericConstants.ZERO) {
                    whereClause.append(QUERY_AND);
                }
                whereClause.append("stk_qty").append(" != 0");
            }


        }

        if (inputDTO.getProductCode() != null) {
            // add user filter
            if (whereClause.length() != NumericConstants.ZERO) {
                whereClause.append(QUERY_AND);
            }
            params.put(inputDTO.getProductCode(), inputDTO.getProductCode());
            whereClause.append("products.prod_code").append(" = :").append(inputDTO.getProductCode());
        }
        return whereClause.toString();
    }

    /**
     * @param prodCode
     * @return
     */
    @Override
    public ProductDTO getProductInfoByProductCode(final String prodCode, final String cmpCode, final Integer customerType) {
        try {
            var params = new HashMap<String, Object>();
            var query = queryService.get(SELECT_PRODUCT_INFO);
            // build where conditions
            var whereClause = buildWhereClause(prodCode, cmpCode, params);

            if (!whereClause.trim().isEmpty()) {
                query += "\n" + WHERE + whereClause;
            }
            params.put("cusType", CommonUtil.isEmpty(customerType) ? Integer.valueOf(StringConstants.DEALER) : customerType);
            // execute query
            var productList = dao.query(query, params, ProductDTO.class);
            if (!CommonUtil.isEmpty(productList)) {
                var productDTO = productList.stream().filter(Objects::nonNull).findFirst().get();
                if (!CommonUtil.isEmpty(productDTO) && productDTO.getStock() == 0) {
                    var paramMap = new HashMap<String, Object>();
                    paramMap.put("cmpCode", productDTO.getCmpCode());
                    paramMap.put("prodCode", productDTO.getId());
                    dao.batchUpdate(queryService.get(INSERT_PRODUCT_SEARCH_COUNT), List.of(paramMap));
                }
                return productDTO;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new DulabException(e);
        }
    }

    @Override
    public Map<String, List<ProductDTO>> fetchProductsForDashboard(final CustomInputDTO customInputDTO) {
        var dashBoardMap = new LinkedHashMap<String, List<ProductDTO>>();
        var params = new HashMap<String, Object>();
        params.put("cusType", CommonUtil.isEmpty(customInputDTO.getCusType()) ? Integer.valueOf(StringConstants.DEALER) : customInputDTO.getCusType());
        var newCollectionList = dao.query(queryService.get("SELECT_NEW_COLLECTIONS"), params, ProductDTO.class);
        if (newCollectionList.isEmpty() || newCollectionList.size() < NUMBER_FOUR) {
            newCollectionList = dao.query(queryService.get("SELECT_DEFAULT_NEW_COLLECTIONS"), params, ProductDTO.class);
        }
        dashBoardMap.put(StringConstants.NEW_COLLECTIONS, newCollectionList);

        List<ProductDTO> trendingCollectionList = new ArrayList<ProductDTO>();
//        var trendingCollectionList = dao.query(queryService.get("SELECT_TRENDING_COLLECTIONS"),params,ProductDTO.class);
        if (trendingCollectionList.isEmpty() || trendingCollectionList.size() < NUMBER_FOUR) {
            trendingCollectionList = dao.query(queryService.get("SELECT_DEFAULT_TRENDING_COLLECTIONS"), params, ProductDTO.class);
        }
        var trenCollectionList = removeNewInTrending(newCollectionList, trendingCollectionList);
        dashBoardMap.put(StringConstants.TRENDING_COLLECTIONS, trenCollectionList);

        if (customInputDTO.getCusType() == 3) {
            dashBoardMap.put(StringConstants.SPECIAL_OFFERS,
                    dao.query(queryService.get("SELECT_SPECIAL_OFFERS"), params, ProductDTO.class));
        }
        return dashBoardMap;
    }

    private List<ProductDTO> removeNewInTrending(List<ProductDTO> newCollectionList, List<ProductDTO> trendingCollectionList) {
        for (ProductDTO n : newCollectionList) {
            for (ProductDTO t : trendingCollectionList) {
                if (t.getId().equals(n.getId())) {
                    trendingCollectionList.remove(t);
                    break;
                }
            }
        }
        return trendingCollectionList;
    }

    /**
     * @param prodCode
     * @param cmpCode
     * @return
     */
    private String buildWhereClause(final String prodCode, final String cmpCode, final Map<String, Object> params) {
        var whereClause = new StringBuilder();
        // add user filter
        if (whereClause.length() != NumericConstants.ZERO) {
            whereClause.append(QUERY_AND);
        }
        if (prodCode != null) {
            params.put(prodCode, prodCode);
            whereClause.append("products.prod_code").append(" = :").append(prodCode);
        }
        if (whereClause.length() != NumericConstants.ZERO) {
            whereClause.append(QUERY_AND);
        }
        if (cmpCode != null) {
            params.put(cmpCode, cmpCode);
            whereClause.append("products.prod_cmp_code").append(" = :").append(cmpCode);
        }

        return whereClause.toString();
    }


    /**
     * This method is used is used to get the total record count.
     *
     * @param customInputDTO Report input details
     * @return Total records count
     */
    @Override
    public long getTotalRecords(final CustomInputDTO customInputDTO) {
        try {
            var totalRecordParams = new HashMap<String, Object>();
            var query = queryService.get("SELECT_PRODUCT_INFO_COUNT");
            // build where conditions
            var whereClause = buildWhereClause(customInputDTO, totalRecordParams);
            if (!whereClause.trim().isEmpty()) {
                query += "\n" + WHERE + whereClause;
            }
            // order by
            query += "\n" + ORDER_BY + "products.prod_code";
            return dao.queryForObject(query, totalRecordParams, Long.class);
        } catch (Exception e) {
            throw new DulabException(e);
        }

    }
}
