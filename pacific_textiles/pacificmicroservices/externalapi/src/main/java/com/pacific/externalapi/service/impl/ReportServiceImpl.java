package com.pacific.externalapi.service.impl;

import com.pacific.common.dao.IDAORepository;
import com.pacific.common.dto.DashBoardDTO;
import com.pacific.common.dto.ProductSearchCountDTO;
import com.pacific.common.dto.StockReportDTO;
import com.pacific.common.service.IQueryService;
import com.pacific.externalapi.service.IReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
@Transactional
public class ReportServiceImpl implements IReportService {

    /**
     * LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ExternalAPIServiceImpl.class);

    /**
     * dao.
     */
    private final IDAORepository dao;

    /**
     * queryService.
     */
    private final IQueryService queryService;

    /**
     * Constructor to inject dependency.
     *
     * @param daoIn          DAORepository
     * @param queryServiceIn QueryService
     */
    public ReportServiceImpl(final IDAORepository daoIn, final IQueryService queryServiceIn) {
        this.dao = daoIn;
        this.queryService = queryServiceIn;
    }

    @Override
    public List<StockReportDTO> generateStockReport() {
        return dao.query(queryService.get("SELECT_STOCK_REPORT"), StockReportDTO.class);
    }

    @Override
    public List<ProductSearchCountDTO> generateProductSearchCountDTO() {
        return dao.query(queryService.get("SEARCH_PRODUCT_SEARCH_COUNT_T"), ProductSearchCountDTO.class);
    }

    @Override
    public Map<String,List<DashBoardDTO>> generateDashBoard() {
        var customerTypeList = dao.query(queryService.get("CUSTOMER_TYPE_COUNT"), DashBoardDTO.class);
        var currentDayOrderCount = dao.query(queryService.get("CURRENT_DAY_ORDER_COUNT"), DashBoardDTO.class);
        var totalPendingOrderCount = dao.query(queryService.get("TOTAL_PENDING_ORDER_COUNT"), DashBoardDTO.class);

        return Map.of("CUSTOMER_TYPE_COUNT", customerTypeList,"CURRENT_DAY_ORDER_COUNT",currentDayOrderCount,
                "TOTAL_PENDING_ORDER_COUNT",totalPendingOrderCount);


    }
}
