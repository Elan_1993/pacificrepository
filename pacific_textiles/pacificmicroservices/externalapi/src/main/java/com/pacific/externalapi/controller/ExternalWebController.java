package com.pacific.externalapi.controller;

import com.pacific.common.dto.CustomInputDTO;
import com.pacific.common.dto.DropDownValuesDTO;
import com.pacific.common.dto.ProductDTO;
import com.pacific.common.responsedto.ApiResponseDTO;
import com.pacific.externalapi.service.IExternalAPIService;
import com.pacific.externalapi.service.IExternalProductService;
import com.pacific.externalapi.storageaction.IFilesStorageService;
import com.pacific.master.service.IProductService;
import com.pacific.master.service.IUserService;
import com.pacific.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/external.api/controller")
public class ExternalWebController {

    /**
     * service.
     */
    private final IExternalProductService service;

    /**
     * userService.
     */
    private final IUserService userService;

    /**
     * productService.
     */
    private final IProductService productService;

    /**
     * filesStorageService.
     */

    private IFilesStorageService filesStorageService;

    /**
     * context.
     */
    private final ApplicationContext context;

    /**
     * service.
     */
    @Autowired
    private IExternalAPIService externalAPIService;


    /**
     * Constructor to inject dependency.
     *
     * @param serviceIn {@link IExternalAPIService}
     * @param userServiceIn {@link IUserService}
     */
    public ExternalWebController(final IExternalProductService serviceIn,final IUserService userServiceIn ,
                                 final IProductService productServiceIn
            , final ApplicationContext contextIn) {
        this.service = serviceIn;
        this.userService = userServiceIn;
        this.productService = productServiceIn;
        this.context = contextIn;
        filesStorageService = context.getBean("s3Storage", IFilesStorageService.class);

    }

    /**
     * This method is used to generate custom report.
     * @param inputDTO Custom InputDTO
     * @return Report output
     */
    @PostMapping(value = "/generate-product-info", produces = "application/json", consumes = "application/json")
    public final List<ProductDTO> generateProductInfo(final @RequestBody CustomInputDTO inputDTO) {
        return service.getProductInfo(inputDTO);
    }

    /**
     * @param user
     * @return
     */
    @PostMapping(value = "/register-user", produces = "application/json", consumes = "application/json")
    public final ResponseEntity<ApiResponseDTO>  registerUser(@RequestBody final User user) {
        return new ResponseEntity<>(userService.registerUser(user), HttpStatus.OK);
    }

    /**
     * This method is used to fetch code table details.
     *
     */
    @GetMapping(value = "/generate-product-by-id/{prodCode}/{cmpCode}/{customerType}")
    public final ProductDTO getProductInfoByProductCode(final @PathVariable String prodCode,
                                                        final @PathVariable String cmpCode,
                                                        final @PathVariable Integer customerType) {
        return service.getProductInfoByProductCode(prodCode, cmpCode, customerType);
    }

    /**
     * This method is used to generate custom report.
     * @param inputDTO Custom InputDTO
     * @return Report output
     */
    @PostMapping(value = "/get-total-record-count", produces = "application/json", consumes = "application/json")
    public final Long getTotalRecordCount(final @RequestBody CustomInputDTO inputDTO) {
        return service.getTotalRecords(inputDTO);
    }

    /**
     * Method to get products by the CatalogId.
     *
     * @return list<Product>
     */
    @GetMapping("/product-search/{productCode}")
    public List<ProductDTO> fetchProductsByCatalogId(final @PathVariable String productCode) {
        return productService.searchForAutocomplete(productCode);
    }

    @PostMapping("/dashboard-search")
    public Map<String ,List<ProductDTO>> fetchProductsForDashboard(final @RequestBody CustomInputDTO inputDTO) {
        return service.fetchProductsForDashboard(inputDTO);
    }

    @GetMapping("/files/{cmpCode}/{catalogId}/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(final @PathVariable String cmpCode,
                                            final @PathVariable Integer catalogId,
    @PathVariable String filename) {
        Resource file = filesStorageService.load(filename,cmpCode,catalogId);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }


    @GetMapping("/load-state-courier-dropdowns/{countryCode}")
    public final ResponseEntity<?> getAllDropDowns(final @PathVariable Integer countryCode) {
        return ResponseEntity.status(HttpStatus.OK).body(externalAPIService.getSateCourierDropDowns(countryCode));
    }

}
