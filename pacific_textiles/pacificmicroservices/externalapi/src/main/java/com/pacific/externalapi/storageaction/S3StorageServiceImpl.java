package com.pacific.externalapi.storageaction;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service("s3Storage")
public class S3StorageServiceImpl extends IFilesStorageService {


    @Override
    public void upload(MultipartFile file, String cmpCode, Integer catalogId) {
        var path = generatePath(file.getOriginalFilename(), cmpCode, catalogId);
        var s3Client = amazonS3.getAmazonS3Client();
        try {
            var absolutePath = s3CommonPath.concat(path);
            saveProductImage(file, cmpCode, catalogId, absolutePath);
            PutObjectRequest put = new PutObjectRequest(bucketName, path, file.getInputStream(), new ObjectMetadata()).withCannedAcl(CannedAccessControlList.Private);
            s3Client.putObject(put);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Resource load(String filename, String cmpCode, Integer catalogId) {
        return null;
    }
}
