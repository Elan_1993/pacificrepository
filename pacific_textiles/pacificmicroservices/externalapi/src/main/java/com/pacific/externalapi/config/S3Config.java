package com.pacific.externalapi.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class S3Config {
    private static AmazonS3 amazonS3Client;

    @Value("${aws.iam.accessKey}")
    private String accessKey;

    @Value("${aws.iam.secretKey}")
    private String secretKey;


    private  AWSCredentialsProvider amazonAWSCredentialsProvider() {
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(accessKey, secretKey));
    }

    @Bean
    public AmazonS3 getAmazonS3Client() {
        if (null == amazonS3Client) {
            amazonS3Client = AmazonS3ClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
                    .withRegion(Regions.AP_SOUTH_1).build();
        }
        return amazonS3Client;
    }
}
